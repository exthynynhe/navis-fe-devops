import { Injectable } from "@angular/core";
import { AppSettingDTO } from 'src/app/model/AppSettingDTO';
import { RestFulAPIService } from '../shared/services/restfulapi.service';
import { UserPreferenceDTO } from "../model/UserPreferenceDTO";
import { NGXLogger } from "ngx-logger";

@Injectable({
    providedIn: 'root'
  })

export class Globals {
    
    public settingDTO: AppSettingDTO = undefined;    
    public autoUpdateStarted: boolean = false;
    public preferencesDTO: UserPreferenceDTO = undefined;
    
    constructor(private restFulAPI: RestFulAPIService, private logger: NGXLogger) {
        this.loadApplicationSettings();        
        this.autoUpdateStarted = false;
    }

    public loadApplicationSettings(){        
        this.restFulAPI.getApplicationSettings().subscribe(res => {         
            this.settingDTO = res;             
        },
        err => {
            this.logger.debug(`getApplicationSettings| ${err}`);            
        }); 
    }
    public loadUserPreferences(username: string){
        this.restFulAPI.getUserPreferences(username).subscribe(res => {
            this.preferencesDTO = res;
        },
        err => {
            this.logger.debug(`getUserPreferences() | ${err}`);
        });
    }
    public updateUserPreferences(preferencesDTO: UserPreferenceDTO){
        this.preferencesDTO = preferencesDTO;
    }
    public updateApplicationSettings(settingDTO: AppSettingDTO) {
        this.settingDTO = settingDTO;               
    }

    public setAutoUpdateStarted(val: boolean){
        this.autoUpdateStarted = val;
      }
  
    public getAutoUpdateStarted(): boolean {
        return this.autoUpdateStarted;
    }
}