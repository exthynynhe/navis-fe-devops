import * as jwt_decode from 'jwt-decode';
import { VesselVisitDTO } from '../model/VesselVisitDTO';

export enum LOGIN_TYPE {LOGGED, E_CREDENTIALS, E_CONNECTION}

export const version = "v1.086"

// ################### REMOVE ALL THIS WHEN CONECTED TO API

// Available columns
export const colDescs = [
    { "columnDescId":"0","columnDescName":"Vessel Visit Phase","columnDescShow":true, "columnDescOrder": "0" },
    { "columnDescId":"1","columnDescName":"Vessel Visit","columnDescShow":true, "columnDescOrder": "1" },
    { "columnDescId":"2","columnDescName":"ETA/ATA Berth","columnDescShow":true, "columnDescOrder": "2"},
    { "columnDescId":"3","columnDescName":"ETD Berth","columnDescShow":true, "columnDescOrder": "3"},
    { "columnDescId":"4","columnDescName":"ETC","columnDescShow":true, "columnDescOrder": "4"},
    { "columnDescId":"5","columnDescName":"Service","columnDescShow":true, "columnDescOrder": "5"},
    { "columnDescId":"6","columnDescName":"Alarms","columnDescShow":true, "columnDescOrder": "6"},
    { "columnDescId":"7","columnDescName":"Discharge Count","columnDescShow":true, "columnDescOrder": "7"},
    { "columnDescId":"8","columnDescName":"Load Count","columnDescShow":true, "columnDescOrder": "8"},
    { "columnDescId":"9","columnDescName":"Restow","columnDescShow":true, "columnDescOrder": "9"},
    { "columnDescId":"10","columnDescName":"Crane Intensity","columnDescShow":true, "columnDescOrder": "10"},
    { "columnDescId":"11","columnDescName":"Vessel Notes","columnDescShow":true, "columnDescOrder": "11"},
    { "columnDescId":"12","columnDescName":"Workflow Progress","columnDescShow":true, "columnDescOrder": "12"},      
    { "columnDescId":"13","columnDescName":"Dsch Count Completion","columnDescShow":false, "columnDescOrder": "13"},
    { "columnDescId":"14","columnDescName":"Load Count Completion","columnDescShow":false, "columnDescOrder": "14"},
    { "columnDescId":"15","columnDescName":"Count Completion","columnDescShow":true, "columnDescOrder": "15"},
    { "columnDescId":"16","columnDescName":"Berthing","columnDescShow":false, "columnDescOrder": "16"},
    { "columnDescId":"17","columnDescName":"Facility","columnDescShow":false, "columnDescOrder": "17"},
    { "columnDescId":"18","columnDescName":"Workflow","columnDescShow":false, "columnDescOrder": "18"},
    { "columnDescId":"19","columnDescName":"SLA","columnDescShow":false, "columnDescOrder": "19"}
];

//Workflow defaults
export const carriers = [
    {
        value: 1,
        text: 'Carrier #1'
    },
    {
        value: 2,
        text: 'Carrier #2'
    },
    {
        value: 3,
        text: 'Carrier #3'
    },
    {
        value: 4,
        text: 'Carrier #4'
    },
  ];

  export const facilities = [
    {
        value: 1,
        text: 'Facility #1'
    },
    {
        value: 2,
        text: 'Facility #2'
    },
    {
        value: 3,
        text: 'Facility #3'
    },
    {
        value: 4,
        text: 'Facility #4'
    },
  ];

  export const operators = [
    {
        value: 1,
        text: 'Operator #1'
    },
    {
        value: 2,
        text: 'Operator #2'
    },
    {
        value: 3,
        text: 'Operator #3'
    },
    {
        value: 4,
        text: 'Operator #4'
    },
  ];

  // ################ REMOVE UNTIL HERE

export const userRoles = [
    {
      value: 2,
      text: 'ADMIN'
    }, 
    {
      value: 3,
      text: 'PLANNER'
    }, 
    { 
      value: 4,
      text: 'MANAGER'
    }
  ];

export const notificationProfiles = [
    {
      value: 1,
      text: 'ALL'
    }, 
    {
      value: 2,
      text: 'NONE'
    },     
    {
      value: 3,
      text: 'TERMINAL'
    }, 
    {
      value: 4,
      text: 'EDI'
    }
  ];



export const workflowStages : { id: number, code: string, name: string }[] = [
    { "id": 0, "code": "PRE", "name": "Preparations" },
    { "id": 1, "code": "DISCHARGE", "name": "Discharge" },
    { "id": 2, "code": "LOAD", "name": "Load" },
    { "id": 3, "code": "DEPARTURE", "name": "Departure" },
    { "id": 4, "code": "OVERALL", "name": "Overall" }
  ];

export const weekDays = [
    { id: 0, value: "Sunday"},
    { id: 1, value: "Monday"},
    { id: 2, value: "Tuesday"},
    { id: 3, value: "Wednesday"},
    { id: 4, value: "Thursday"},
    { id: 5, value: "Friday"},
    { id: 6, value: "Saturday"}    
]

export const phases = [
    {value: 'PRE'},
    {value: 'DISCHARGE'},
    {value: 'LOAD'},
    {value: 'DEPARTURE'},
    {value: 'OVERALL'}
]

export function getOperatorPicture(operator) {        
    switch (operator) {
        case 'APL':
        return 'assets/images/lines/APL.png';        
        case 'CMA':
        case 'CGM':
        return 'assets/images/lines/CMA.png';
        case 'COS':
        case 'COSCO':
        return 'assets/images/lines/COS.png';
        case 'EMC':
        case 'EMS':
        case 'EVG':
        return 'assets/images/lines/EMC.png';
        case 'HDAS':
        case 'HDS':
        return 'assets/images/lines/HDS.png';
        case 'HLC':
        case 'HPG':
        return 'assets/images/lines/HLC.png';
        case 'HMM':
        case 'HYU':
        return 'assets/images/lines/HMM.png';
        case 'HSD':
        case 'HSL':
        return 'assets/images/lines/HSL.png';
        case 'MSC': 
        return 'assets/images/lines/MSC.png';
        case 'MSK': 
        return 'assets/images/lines/MSK.png';
        case 'ONE':
        return 'assets/images/lines/ONE.png';
        case 'OOC':
        case 'OOCL':
        return 'assets/images/lines/OOC.png';
        case 'PIL':
        return 'assets/images/lines/PIL.png';
        case 'UNI':
        case 'UFE':
        return 'assets/images/lines/UNI.png';
        case 'VIA':
        return 'assets/images/lines/VIA.png';
        case 'WHL':
        return 'assets/images/lines/WHL.png';
        case 'YML':
        return 'assets/images/lines/YML.png';
        case 'ZIM':
        return 'assets/images/lines/ZIM.png';
        //default: 
        //return 'assets/images/lines/XPC_X-Press.png';
        default: 
        return '';
    }
}    

export function getColorPhase(phase) {
    switch (phase) {
        case 'WORKING':
            return '#FFD700';
        case 'ARCHIVED':
            return '#696969';
        case 'ARRIVED':
            return '#1AA79C';
        case 'CREATED':
            return '#FFF8DC';
        case 'INBOUND':
            return '#F57B20';
        case 'CANCELLED':
            return '#DC143C';
        case 'COMPLETE':
            return '#E9967A';
        case 'CLOSED':
            return '#A9A9A9';
    }
}  

export function getStageIcon(stage) : string {
    var result = 'assets/images/details/Incomplete.png';
    if(stage != undefined) {
        switch(stage.status) {
            case "MAPPED":
            case "WAITING": 
                result = 'assets/images/details/Incomplete.png';
                break;
            case "EXPIRED":             
            case "ERROR":
                result = 'assets/images/details/Error.png';
                break;
            case "WARNING":
            case "WARNINGS":
            case "COMPLETE": 
                result = 'assets/images/details/Done.png';
                break;
            default:      
                result = 'assets/images/details/Incomplete.png';
                break;
        }
    }    
    return result;
}

export function getSelectedStageIcon(stage) : string {
    var result = 'assets/images/details/IncompleteSel.png';
    if(stage != undefined) {
        switch(stage.status) {
            case "MAPPED":
            case "WAITING": 
                result = 'assets/images/details/IncompleteSel.png';
                break;
            case "EXPIRED":             
            case "ERROR":
                result = 'assets/images/details/ErrorSel.png';
                break;
            case "WARNING":
            case "WARNINGS":
            case "COMPLETE": 
                result = 'assets/images/details/DoneSel.png';
                break;
            default:      
                result = 'assets/images/details/IncompleteSel.png';
                break;
        }
    }    
    return result;
}

export function checkParamsInSession() : Boolean {
    return !(localStorage.getItem("paramsInSession") === null);
}

export function saveParamInSession(paramName: string, paramValue: Boolean, paramNumberValue: Number) {
    if(paramValue === undefined) {
        localStorage.setItem(paramName, String(paramNumberValue));
    } else {        
        localStorage.setItem(paramName, String(paramValue));
    }    
    localStorage.setItem('paramsInSession', "true");
}

export function readParamFromSession(paramName: string) : Boolean {    
    return localStorage.getItem(paramName) === "true";    
}

export function readNumberParamFromSession(paramName: string) : Number {    
    return Number(localStorage.getItem(paramName));
}

export function getCurrentUser() : String {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser) {
        var token = currentUser.token; // your token
        const decoded = jwt_decode(token);                        
        return decoded.sub;
    }    
}

export function disableAllButtons() {    
    var buttons = document.getElementsByTagName("button");
    var notDisableButtons = [
        "e2e-alert-dialog-yes-button",
        "e2e-alert-dialog-no-button",
        "e2e-alert-dialog-ok-button",
        "e2e-navigation-dropdown-item-one-button-sla",
        "e2e-navigation-dropdown-item-two-button-workflow",
        "e2e-navigation-dropdown-item-three-button-users",
        "e2e-navigation-dropdown-item-four-button-appsettings",
        "e2e-navigation-n4-sync-button",
        "e2e-navigation-navbar-dropdown-menu-one-help",        
        "e2e-navigation-navbar-dropdown-menu-two-logout",
        "workflow-settings-workflow-list-tab-btn",
        "workflow-settings-workflow-stages-library-tab-btn",
        "workflow-settings-workflow-configuration-tab-btn"]
    for (var i = 0; i < buttons.length; i++) {
        if (!notDisableButtons.includes(buttons[i].id) && !buttons[i].id.includes("closebutton")) {
          buttons[i].disabled = true;
        }              
    }
  }

export function enableAllButtons() {
    var buttons = document.getElementsByTagName("button");
    for (var i = 0; i < buttons.length; i++) {        
      if (!buttons[i].id.includes("closebutton")) {      
        buttons[i].disabled = false;        
      }
    }
  }

export function checkInputValue(input, minValue, maxValue) {
    if (input.value.length > input.maxLength) {
      input.value = input.value.slice(0, input.maxLength);
    }
    if (input.value > maxValue) { 
      input.value = maxValue;
    }
    if (input.value < minValue) { 
      input.value = 0;
    }
  }

export function getWeekdayNumber(weekday: String) {
    var weekDays = [
        { id: 0, value: "Sunday"},
        { id: 1, value: "Monday"},
        { id: 2, value: "Tuesday"},
        { id: 3, value: "Wednesday"},
        { id: 4, value: "Thursday"},
        { id: 5, value: "Friday"},
        { id: 6, value: "Saturday"}    
      ]    
    for (var i = 0; i < weekDays.length; i++) {
      if (weekDays[i].value === weekday) {
          return weekDays[i].id;
      }
    }
    return 0;
  }

  export function getVesselBackgroundClass(inputVVDto: VesselVisitDTO) {
    var returnClass = 'vesselContainer';
    switch(inputVVDto.shipSideTo) {
        case 'STARBOARD':
            if(inputVVDto.classification === 'BARGE') {
                returnClass = 'vesselContainerBarge';
            } else if (inputVVDto.classification === 'DEEPSEA') {
                returnClass = 'vesselContainerDeep';
            }
            break;
        case 'PORTSIDE':
            if(inputVVDto.classification === 'BARGE') {
                returnClass = 'vesselContainerBargeMirrored';
            } else if (inputVVDto.classification === 'DEEPSEA') {
                returnClass = 'vesselContainerDeepMirrored';
            }
            break; 
        default:
            return 'vesselContainer';                   
    }
    return returnClass;
    }

    // Completed Moves Chart
    export function calculateTitleMoves(vesselVisitDTO: VesselVisitDTO) : Number {
        if(vesselVisitDTO.completedMoves === undefined || vesselVisitDTO.completedMoves === null) {
            return 0;
        }
        return vesselVisitDTO.completedMoves;
    }

    export function calculateSubtitleMoves(vesselVisitDTO: VesselVisitDTO) : Number {
        var loadMoves = 0;
        var dischMoves = 0;
        if (vesselVisitDTO.loadMoves !== null && vesselVisitDTO.loadMoves !== undefined) {
            loadMoves = vesselVisitDTO.loadMoves;
        }
        if (vesselVisitDTO.dischMoves !== null && vesselVisitDTO.dischMoves !== undefined) {
            dischMoves = vesselVisitDTO.dischMoves;
        }
        return loadMoves + dischMoves;                                                            
    }

    export function calculatePercentMoves(vesselVisitDTO: VesselVisitDTO) : Number {
        var loadMoves = 0;
        var dischMoves = 0;
        var completedMoves = 0;
        if (vesselVisitDTO.loadMoves !== null && vesselVisitDTO.loadMoves !== undefined) {
            loadMoves = vesselVisitDTO.loadMoves;
        }
        if (vesselVisitDTO.dischMoves !== null && vesselVisitDTO.dischMoves !== undefined) {
            dischMoves = vesselVisitDTO.dischMoves;
        }
        if(vesselVisitDTO.completedMoves !== null && vesselVisitDTO.completedMoves !== undefined) {
            completedMoves = vesselVisitDTO.completedMoves;
        }
        // Cannot divide by 0
        if(loadMoves+dischMoves === 0) {
            return 0;
        }
        return (completedMoves*100)/(loadMoves + dischMoves);
    }

    // Completed Load Moves Chart
    export function calculateLoadTitleMoves(vesselVisitDTO: VesselVisitDTO) : Number {
        if(vesselVisitDTO.completedLoadMoves === undefined || vesselVisitDTO.completedLoadMoves === null) {
            return 0;
        }
        return vesselVisitDTO.completedLoadMoves;
    }

    export function calculateLoadSubtitleMoves(vesselVisitDTO: VesselVisitDTO) : Number {
        var loadMoves = 0;
        if (vesselVisitDTO.loadMoves !== null && vesselVisitDTO.loadMoves !== undefined) {
            loadMoves = vesselVisitDTO.loadMoves;
        }
        return loadMoves;                                                            
    }

    export function calculateLoadPercentMoves(vesselVisitDTO: VesselVisitDTO) : Number {
        var loadMoves = 0;
        var completedLoadMoves = 0;
        if (vesselVisitDTO.loadMoves !== null && vesselVisitDTO.loadMoves !== undefined) {
            loadMoves = vesselVisitDTO.loadMoves;
        }
        if(vesselVisitDTO.completedLoadMoves !== null && vesselVisitDTO.completedLoadMoves !== undefined) {
            completedLoadMoves = vesselVisitDTO.completedLoadMoves;
        }
        // Cannot divide by 0
        if(loadMoves === 0) {
            return 0;
        }
        return (completedLoadMoves*100)/(loadMoves);
    }

    // Completed Discharge Moves Chart
    export function calculateDschTitleMoves(vesselVisitDTO: VesselVisitDTO) : Number {
        if(vesselVisitDTO.completedDschMoves === undefined || vesselVisitDTO.completedDschMoves === null) {
            return 0;
        }
        return vesselVisitDTO.completedDschMoves;
    }

    export function calculateDschSubtitleMoves(vesselVisitDTO: VesselVisitDTO) : Number {
        var dischMoves = 0;
        if (vesselVisitDTO.dischMoves !== null && vesselVisitDTO.dischMoves !== undefined) {
            dischMoves = vesselVisitDTO.dischMoves;
        }
        return dischMoves;                                                            
    }

    export function calculateDschPercentMoves(vesselVisitDTO: VesselVisitDTO) : Number {
        var dischMoves = 0;
        var completedDschMoves = 0;
        if (vesselVisitDTO.dischMoves !== null && vesselVisitDTO.dischMoves !== undefined) {
            dischMoves = vesselVisitDTO.dischMoves;
        }
        if(vesselVisitDTO.completedDschMoves !== null && vesselVisitDTO.completedDschMoves !== undefined) {
            completedDschMoves = vesselVisitDTO.completedDschMoves;
        }
        // Cannot divide by 0
        if(dischMoves === 0) {
            return 0;
        }
        return (completedDschMoves*100)/(dischMoves);
    }

    export function cleanCharacters(inputText: String) : String {
        var returnedValue = inputText;
        returnedValue = returnedValue.trim();
        returnedValue = returnedValue.replace('%','');
        returnedValue = returnedValue.replace('/','');
        returnedValue = returnedValue.replace(':','');
        return returnedValue
    }