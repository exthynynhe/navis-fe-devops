export class NoteDTO {    

    constructor(noteDTO?: NoteDTO) {
        if(noteDTO != undefined) {
            this.noteId = noteDTO.noteId;            
            this.textMessage = noteDTO.textMessage;            
            this.username = noteDTO.username;            
            this.date = noteDTO.date;            
            this.active = noteDTO.active;            
            this.vesselVisitName = noteDTO.vesselVisitName;   
        }
    }

    public noteId: String;
    public textMessage: String;
    public username: String;    
    public date: Date;
    public active: Boolean;
    public vesselVisitName: String;
}