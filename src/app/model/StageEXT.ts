import { StageDTO } from "./StageDTO";

export class StageEXT extends StageDTO {

    constructor(stageDTO?: StageDTO, editable?: boolean) { 
        super(stageDTO);     
        this.editable = editable;
    }
   
    public editable: boolean;
}