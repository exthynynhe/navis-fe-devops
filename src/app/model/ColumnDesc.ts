export class ColumnDesc {    

    constructor(columnDesc?: ColumnDesc) {
        if(columnDesc != undefined) { 
            this.name = columnDesc.name;            
            this.visible = columnDesc.visible;
            this.class = columnDesc.class;
            this.imageSrc = columnDesc.imageSrc;
            this.id = columnDesc.id;           
            this.order = columnDesc.order;
        }
    }
    public id: number;
    public name: String;
    public visible: Boolean;
    public class: String;
    public imageSrc: String;
    public order: number;
}
