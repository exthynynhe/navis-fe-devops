export class UnitDTO {    

    constructor(unitDTO?: UnitDTO) {
        if(unitDTO != undefined) {
            this.unitId = unitDTO.unitId;            
            this.id = unitDTO.id;            
            this.iso = unitDTO.iso;            
            this.length = unitDTO.length;            
            this.reefer = unitDTO.reefer;
            this.hazardous = unitDTO.hazardous;
            this.oog = unitDTO.oog;            
            this.freightKind = unitDTO.freightKind;   
            this.moveKind = unitDTO.moveKind;         
        }
    }

    public unitId: String;
    public id: String;
    public iso: String;    
    public length: String;
    public reefer: boolean;
    public hazardous: boolean;
    public oog: boolean;
    public freightKind: String;
    public moveKind: String;
}