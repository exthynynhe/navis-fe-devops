export class CarrierServiceDTO {    

    constructor(carrierServiceDTO?: CarrierServiceDTO) {
        if(carrierServiceDTO != undefined) {
            this.carrierServiceId = carrierServiceDTO.carrierServiceId;
            this.carrierServiceIdName = carrierServiceDTO.carrierServiceIdName;
            this.carrierServiceName = carrierServiceDTO.carrierServiceName;
            this.carrierServiceMode = carrierServiceDTO.carrierServiceMode;  
            this.active = carrierServiceDTO.active;            
        }
    }

    public carrierServiceId: String;
    public carrierServiceIdName: String;
    public carrierServiceName: String;
    public carrierServiceMode: String;
    public active: Boolean;
}