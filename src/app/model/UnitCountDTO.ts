
export class UnitCountDTO {    

    constructor(unitCountDTO?: UnitCountDTO) {
        if(unitCountDTO != undefined) {           
            this.total = unitCountDTO.total;            
            this.nom20 = unitCountDTO.nom20;            
            this.nom40 = unitCountDTO.nom40;            
            this.nom45 = unitCountDTO.nom45;               
            this.full = unitCountDTO.full;
            this.empty = unitCountDTO.empty;
            this.standard = unitCountDTO.standard;
            this.reefer = unitCountDTO.reefer;
            this.oog = unitCountDTO.oog;
            this.dg = unitCountDTO.dg;   
        }
    }

    public total: Number;
    public nom20: Number;    
    public nom40: Number;
    public nom45: Number;
    
    public full: UnitCountDTO;
    public empty: UnitCountDTO;
    public standard: UnitCountDTO;
    public reefer: UnitCountDTO;
    public oog: UnitCountDTO;
    public dg: UnitCountDTO;
}