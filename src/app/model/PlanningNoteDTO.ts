export class PlanningNoteDTO {    

    constructor(planningNoteDTO?: PlanningNoteDTO) {
        if(planningNoteDTO != undefined) {
            this.noteId = planningNoteDTO.noteId;            
            this.textMessage = planningNoteDTO.textMessage;            
            this.username = planningNoteDTO.username;            
            this.date = planningNoteDTO.date;           
            this.doneUsername = planningNoteDTO.doneUsername;            
            this.doneDate = planningNoteDTO.doneDate;  
            this.doneStatus = planningNoteDTO.doneStatus;          
            this.active = planningNoteDTO.active;            
            this.vesselVisitName = planningNoteDTO.vesselVisitName;  
             
        }
    }

    public noteId: String;
    public textMessage: String;
    public username: String;    
    public date: Date;
    public doneUsername: String;    
    public doneDate: Date;
    public doneStatus: Boolean;
    public active: Boolean;
    public vesselVisitName: String;
}