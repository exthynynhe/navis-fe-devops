export class VesselNoteDTO {    

    constructor(noteDTO?: VesselNoteDTO) {
        if(noteDTO != undefined) {
            this.noteId = noteDTO.noteId;            
            this.textMessage = noteDTO.textMessage;            
            this.username = noteDTO.username;            
            this.date = noteDTO.date;                       
            this.vesselName = noteDTO.vesselName;   
            this.active = noteDTO.active;
        }
    }

    public noteId: String;
    public textMessage: String;
    public username: String;    
    public date: Date;
    public vesselName: String;
    public active: Boolean;
}