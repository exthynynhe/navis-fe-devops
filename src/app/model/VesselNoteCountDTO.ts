export class VesselNoteCountDTO {    

    constructor(vesselNoteCountDTO?: VesselNoteCountDTO) {
        if(vesselNoteCountDTO != undefined) {
            this.vesselName = vesselNoteCountDTO.vesselName;            
            this.notesCount = vesselNoteCountDTO.notesCount;                        
        }
    }
    
    public vesselName: String;
    public notesCount: number;        
}