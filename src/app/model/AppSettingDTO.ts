export class AppSettingDTO {    

    constructor(settingDTO?: AppSettingDTO) {
        if(settingDTO != undefined) {
            this.settingId = settingDTO.settingId;            
            this.autoUpdateFrequency = settingDTO.autoUpdateFrequency;
            this.autoUpdateWithFilters = settingDTO.autoUpdateWithFilters;
            this.resetAllDsch = settingDTO.resetAllDsch;            
            this.multipleStepsComplete = settingDTO.multipleStepsComplete;    
            this.amountItemsPagination = settingDTO.amountItemsPagination;      
        }
    }

    public settingId: String;
    public autoUpdateFrequency: number;
    public autoUpdateWithFilters: boolean;
    public resetAllDsch: boolean;
    public multipleStepsComplete: boolean;    
    public amountItemsPagination: number;
}
