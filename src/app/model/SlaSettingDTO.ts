import { FacilityDTO } from './FacilityDTO';
import { LineOperatorDTO } from './LineOperatorDTO';
import { CarrierServiceDTO } from './CarrierServiceDTO';

export class SlaSettingDTO {    

    constructor(slaSettingDTO?: SlaSettingDTO) {
        if(slaSettingDTO != undefined) {
            this.slaId = slaSettingDTO.slaId;  
            this.slaName = slaSettingDTO.slaName; 
            this.facilities = slaSettingDTO.facilities;
            this.lineOperators = slaSettingDTO.lineOperators;
            this.carrierServices = slaSettingDTO.carrierServices;       
            this.service = slaSettingDTO.service;
            this.coprarLoadDeadline = slaSettingDTO.coprarLoadDeadline;
            this.coprarDischDeadline = slaSettingDTO.coprarDischDeadline;            
            this.movinsDeadline = slaSettingDTO.movinsDeadline;
            this.baplieDeadline = slaSettingDTO.baplieDeadline;
            this.forecastDeadline = slaSettingDTO.forecastDeadline;
            this.berthProdMin = slaSettingDTO.berthProdMin;
            this.berthProdMax = slaSettingDTO.berthProdMax;
            this.dischMoves = slaSettingDTO.dischMoves;
            this.loadMoves = slaSettingDTO.loadMoves;
            this.craneIntensity = slaSettingDTO.craneIntensity;
            this.eta = slaSettingDTO.eta;
            this.etd = slaSettingDTO.etd;           
        }
    }

    public slaId: String;
    public slaName: String;
    public facilities: Array<FacilityDTO>;
    public lineOperators: Array<LineOperatorDTO>;
    public carrierServices: Array<CarrierServiceDTO>
    public service: String;
    public coprarLoadDeadline: number;
    public coprarDischDeadline: number;
    public movinsDeadline: number;
    public baplieDeadline: number;
    public forecastDeadline: number;
    public berthProdMin: number;
    public berthProdMax: number;
    public dischMoves: number;
    public loadMoves: number;
    public craneIntensity: number;
    public eta: Date;
    public etd: Date;    
}
