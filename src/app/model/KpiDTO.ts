export class KpiDTO {    

    constructor(kpiDTO?: KpiDTO) {
        if(kpiDTO != undefined) {
            this.kpiId = kpiDTO.kpiId;            
            this.name = kpiDTO.name;            
            this.agreedValue = kpiDTO.agreedValue;            
            this.actualValue = kpiDTO.actualValue;            
            this.agreedValueDate = kpiDTO.agreedValueDate;
            this.actualValueDate = kpiDTO.actualValueDate;
            this.comment = kpiDTO.comment;            
            this.username = kpiDTO.username;   
            this.date = kpiDTO.date;               
            this.editable = kpiDTO.editable;   
            this.dataType = kpiDTO.dataType;
            this.kpiType = kpiDTO.kpiType;
            this.position = kpiDTO.position;
            this.vesselVisitName = kpiDTO.vesselVisitName;
            this.active = kpiDTO.active;
        }
    }

    public kpiId: String;
    public name: String;
    public agreedValue: String;    
    public actualValue: String;
    public agreedValueDate: Date;
    public actualValueDate: Date;
    public comment: String;
    public username: String;
    public date: Date;
    public editable: Boolean;
    public dataType: string;
    public kpiType: string;
    public position: number;
    public vesselVisitName: String;
    private active: Boolean;
}
