export class FacilityDTO {    

    constructor(facilityDTO?: FacilityDTO) {
        if(facilityDTO != undefined) {
            this.facilityId = facilityDTO.facilityId;            
            this.facilityName = facilityDTO.facilityName;            
            this.timeZone = facilityDTO.timeZone;                       
            this.active = facilityDTO.active;            
        }
    }

    public facilityId: String;
    public facilityName: String;
    public timeZone: String;
    public active: boolean;
}