export class StageDTO {    

    constructor(stageDTO?: StageDTO) {
        if(stageDTO != undefined) {
            this.stageTemplateId = stageDTO.stageTemplateId;
            this.name = stageDTO.name;
            this.position = stageDTO.position;
            this.active = stageDTO.active;
            this.closed = stageDTO.closed;
            this.modifiedBy = stageDTO.modifiedBy;
            this.modifiedDate = stageDTO.modifiedDate;
            this.completed = stageDTO.completed;
            this.completedBy = stageDTO.completedBy;    
            this.status = stageDTO.status;
            this.category = stageDTO.category;
            this.processTime = stageDTO.processTime;
            this.lastComment = stageDTO.lastComment;
            this.lastCommentUsername = stageDTO.lastCommentUsername;
            this.children = stageDTO.children;
            this.role = stageDTO.role;    
            this.comment = stageDTO.comment;
            this.commentBy = stageDTO.commentBy;
            this.commentDate = stageDTO.commentDate;
            this.taken = stageDTO.taken;
            this.takenBy = stageDTO.takenBy;
            this.takenDate = stageDTO.takenDate;            
            this.builtInS = stageDTO.builtInS;
            this.username = stageDTO.username;
            this.stageType = stageDTO.stageType;    
            this.batchNumber = stageDTO.batchNumber;        
        }
    }

    public stageTemplateId: String;
    public name: String;
    public position: number;
    public active: boolean;
    public closed: boolean;
    public modifiedBy: String;
    public modifiedDate: Date;
    public completed: Boolean;
    public completedBy: String;    
    public status: String;
    public category: String;
    public processTime: Date;
    public lastComment: String;
    public lastCommentUsername: String;
    public children: Array<StageDTO>;
    public role: String;    
    public comment: String;
    public commentBy: String;
    public commentDate: Date;
    public taken: boolean;
    public takenBy: String;
    public takenDate: Date;
    public builtInS: boolean;
    public username: String;
    public stageType: String;
    public batchNumber: number;
}
