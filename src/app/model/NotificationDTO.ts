export class NotificationDTO {    

    constructor(notificationDTO?: NotificationDTO) {
        if(notificationDTO != undefined) {
            this.ntId = notificationDTO.ntId;            
            this.deadlineName = notificationDTO.deadlineName;   
            this.message = notificationDTO.message;         
            this.vesselVisitName = notificationDTO.vesselVisitName;  
            this.notificationType = this.notificationType;
            this.notificationProfiles = notificationDTO.notificationProfiles;          
        }
    }

    public ntId: String;
    public deadlineName: string;
    public message: string;
    public vesselVisitName: string; 
    public notificationType: string;
    public notificationProfiles: Array<string>;
}