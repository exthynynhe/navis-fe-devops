import { UnitCountDTO } from "./UnitCountDTO";

export class UnitsRecapDTO {    

    constructor(unitsRecapDTO?: UnitsRecapDTO) {
        if(unitsRecapDTO != undefined) {
            this.vesselVisitName = unitsRecapDTO.vesselVisitName;            
            this.loadRecap = unitsRecapDTO.loadRecap;
            this.dschRecap = unitsRecapDTO.dschRecap;
            this.restowRecap = unitsRecapDTO.restowRecap;
        }
    }

    public vesselVisitName: String;
    public loadRecap: UnitCountDTO;
    public dschRecap: UnitCountDTO;
    public restowRecap: UnitCountDTO;
}