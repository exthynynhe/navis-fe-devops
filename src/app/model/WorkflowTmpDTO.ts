import { FacilityDTO } from './FacilityDTO';
import { CarrierServiceDTO } from './CarrierServiceDTO';
import { LineOperatorDTO } from './LineOperatorDTO';
import { StageEXT } from './StageEXT';

export class WorkflowTmpDTO {    

    constructor(workflowTmpDTO ?: WorkflowTmpDTO) {
        if(workflowTmpDTO != undefined) {
            this.workflowTmpId = workflowTmpDTO.workflowTmpId;
            this.workflowTmpName = workflowTmpDTO.workflowTmpName;
            this.active = workflowTmpDTO.active;
            this.facilities = workflowTmpDTO.facilities;
            this.carrierServices = workflowTmpDTO.carrierServices;
            this.lineOperators = workflowTmpDTO.lineOperators;
            this.stageTemplates = workflowTmpDTO.stageTemplates;
            this.selected = false;
           }
    }

    public workflowTmpId: number;
    public workflowTmpName: String;
    public active: Boolean;    
    public facilities: Array<FacilityDTO>; 
    public carrierServices: Array<CarrierServiceDTO>;
    public lineOperators: Array<LineOperatorDTO>;
    public stageTemplates: Array<StageEXT>;
    public selected: boolean;
       
}