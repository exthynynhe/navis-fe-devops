export class UserPreferenceDTO{
    constructor(userPreferenceDTO ?: UserPreferenceDTO){
        if(userPreferenceDTO != null){
            this.userPreferenceId = userPreferenceDTO.userPreferenceId;
            this.preferences = userPreferenceDTO.preferences;
        }
    }

    public userPreferenceId: String;
    public username: String;
    public preferences: string;
}