export class LineOperatorDTO {    

    constructor(lineOperatorDTO?: LineOperatorDTO) {
        if(lineOperatorDTO != undefined) {
            this.lineOperatorId = lineOperatorDTO.lineOperatorId;            
            this.lineOperatorName = lineOperatorDTO.lineOperatorName;
            this.active = lineOperatorDTO.active;
        }
    }

    public lineOperatorId: String;
    public lineOperatorName: String;
    public active: Boolean;
}