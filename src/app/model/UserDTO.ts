export class UserDTO {    

    constructor(userDTO ?: UserDTO) {
        if(userDTO != undefined) {
            this.userId = userDTO.userId;
            this.username = userDTO.username;
            this.password = userDTO.password;
            this.repeatPassword = userDTO.repeatPassword;
            this.notificationProfileId = userDTO.notificationProfileId;
            this.roleId = userDTO.roleId;
            this.active = userDTO.active;
            this.editEnabled = false;
        }
    }

    public userId: String;
    public username: String;
    public password: String;    
    public repeatPassword: String; 
    public notificationProfileId: Number;
    public roleId: Number;    
    public active: Boolean;
    public editEnabled: Boolean;
}