export class ColumnDescDTO {
    constructor(columnDescDTO ?: ColumnDescDTO){
        this.columnDescId = columnDescDTO.columnDescId;
        this.columnDescName = columnDescDTO.columnDescName;
        this.columnDescShow = columnDescDTO.columnDescShow;
        this.columnDescOrder = columnDescDTO.columnDescOrder;
    }

    public columnDescId: String;
    public columnDescName: String;
    public columnDescShow: Boolean;
    public columnDescOrder: String;
}