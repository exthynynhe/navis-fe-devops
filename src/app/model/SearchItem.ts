export class SearchItem {    

    constructor(searchItem?: SearchItem) {
        if(searchItem != undefined) {
            this.id = searchItem.id;            
            this.name = searchItem.name;
            this.chipName = searchItem.chipName;
            this.attributeName = searchItem.attributeName;
            this.value = searchItem.value;
            this.comparisonChar = searchItem.comparisonChar;        
        }
    }

    public id: number;
    public name: string;
    public chipName: string;
    public attributeName: string;                    
    public value: string;
    public comparisonChar: string;
}
