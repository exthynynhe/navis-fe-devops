import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StagesWorkflowDetailsComponent } from './stages-workflow-details.component';

describe('StagesWorkflowDetailsComponent', () => {
  let component: StagesWorkflowDetailsComponent;
  let fixture: ComponentFixture<StagesWorkflowDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StagesWorkflowDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StagesWorkflowDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
