import { VesselVisitDTO } from '../../../model/VesselVisitDTO';
import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { StageDTO } from '../../../model/StageDTO';
import { getStageIcon, getSelectedStageIcon, workflowStages } from '../../../global/utils';

@Component({
  selector: 'e2e-stages-workflow-details',
  templateUrl: './stages-workflow-details.component.html',
  styleUrls: ['./stages-workflow-details.component.scss']
})

export class StagesWorkflowDetailsComponent implements OnInit {

  public workflowStages = workflowStages; 

  @Input() vesselVisitDTO: VesselVisitDTO;
  @Input() selectedStage: StageDTO;

  @Output() messageEvent = new EventEmitter<StageDTO>();

  @ViewChild('carrouselContent', { read: ElementRef }) public carrouselContent: ElementRef<any>;

  constructor() {
  }

  ngOnInit() {    
  }

  public updateStageClickStyles(phase: StageDTO) {
    if(this.selectedStage !== null && this.selectedStage.stageTemplateId !== null) {            
      document.getElementById(this.selectedStage.stageTemplateId.valueOf()).style.fontWeight = "normal";
      (<HTMLImageElement>document.getElementById('img ' + this.selectedStage.stageTemplateId)).src = getStageIcon(this.selectedStage);
      (<HTMLImageElement>document.getElementById('img ' + this.selectedStage.stageTemplateId)).className = 'box';
    }
    this.selectedStage = phase;    
    (<HTMLImageElement>document.getElementById('img ' + this.selectedStage.stageTemplateId)).src = getSelectedStageIcon(this.selectedStage);    
  }

  public stageClick(phase: StageDTO) {
    this.updateStageClickStyles(phase);
    this.messageEvent.emit(this.selectedStage);    
  }

  public getWorkflowStages() {
    var returnedStages = [];
    for(var i = 0; i < this.workflowStages.length; i++) {
      if(this.getFilteredStages(this.workflowStages[i].code).length > 0) {
        returnedStages.push(this.workflowStages[i]);
      }
    }
    return returnedStages;
  }

  public getFilteredStages(category): Array<StageDTO> {
    var returnStages: Array<StageDTO>  = this.vesselVisitDTO.stages.filter(
      stage => stage.category === category);
    returnStages.sort(function(a, b){
        return a.position-b.position;
    })   
    return returnStages;
  }
  
  public getStgIcon(stage): string {
    if(stage.stageTemplateId === this.selectedStage.stageTemplateId) {            
      return getSelectedStageIcon(stage);
    } else {
      return getStageIcon(stage);
    }
  }

  public getSelectedStgIcon(stage): string {
    return getSelectedStageIcon(stage);    
  }

  public isSelectedStage(stage) {    
    return stage.stageTemplateId === this.selectedStage.stageTemplateId;
  }

  getStagesBridgeClass(index, filteredStages) : String {
    var returnValue = 'greySquare';    
    if(index < filteredStages.length - 1 && filteredStages[index].status === "COMPLETE" && filteredStages[index + 1].status === "COMPLETE") {
      returnValue = 'blueSquare';
    }
    return returnValue;
  }

  public scrollRight(): void {
    this.carrouselContent.nativeElement.scrollTo({ left: (this.carrouselContent.nativeElement.scrollLeft + 150), behavior: 'smooth' }); 
  }

  public scrollLeft(): void {
    this.carrouselContent.nativeElement.scrollTo({ left: (this.carrouselContent.nativeElement.scrollLeft - 150), behavior: 'smooth' });
  }
}

