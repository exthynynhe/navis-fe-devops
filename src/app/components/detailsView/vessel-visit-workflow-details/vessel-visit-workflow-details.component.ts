import { Component, OnInit, Input } from '@angular/core';
import { VesselVisitDTO } from '../../../model/VesselVisitDTO';
import { WebSocketService } from '../../../shared/services/websocket.service';
import { colDescs, getColorPhase, getOperatorPicture, getVesselBackgroundClass, calculateTitleMoves, calculateSubtitleMoves, calculatePercentMoves, calculateLoadPercentMoves, calculateLoadTitleMoves, calculateLoadSubtitleMoves, calculateDschPercentMoves, calculateDschTitleMoves, calculateDschSubtitleMoves } from '../../../global/utils';
import { VesselNoteCountDTO } from 'src/app/model/VesselNoteCountDTO';
import { ColumnDescDTO } from 'src/app/model/ColumnDescDTO';
import { RestFulAPIService } from 'src/app/shared/services/restfulapi.service';
import { Globals } from 'src/app/global/globals';
import { UserPreferenceDTO } from 'src/app/model/UserPreferenceDTO';

@Component({
  selector: 'e2e-vessel-visit-workflow-details',
  templateUrl: './vessel-visit-workflow-details.component.html',
  styleUrls: ['./vessel-visit-workflow-details.component.scss']
})

export class VesselVisitWorkflowDetailsComponent implements OnInit {

  public vesselNotesCount: VesselNoteCountDTO;      
  public columns: Array<ColumnDescDTO> = [];    
  public preferencesDTO: UserPreferenceDTO;

  @Input() vvDTO: VesselVisitDTO;

  constructor(private restFulAPI: RestFulAPIService, private globals: Globals) {
    this.fillColumnsVisibility();
  }
  
  ngOnInit() {
  }

  async fillColumnsVisibility() {
    var currentUser = JSON.parse(localStorage.getItem('currentUser')); 
    if(currentUser) {
      var username = currentUser.username;
      await this.restFulAPI.getUserPreferences(username).subscribe(res => {
        if(!this.globals.preferencesDTO){
          this.globals.updateUserPreferences(res);
        }
        if(this.globals.preferencesDTO){
          this.preferencesDTO = this.globals.preferencesDTO;
          if(this.preferencesDTO.preferences !== undefined && this.preferencesDTO.preferences !== null){
            var preferences = JSON.parse(this.preferencesDTO.preferences);
            this.columns = preferences.colDescs;                  
          }
      } else {
          this.columns = colDescs;
        }      
      });
      this.columns.sort(this.getSortOrder("columnDescOrder"));
    }
  }

  getSortOrder(prop){
    return function(a,b){
      if(Number(a[prop]) > Number(b[prop])){
        return 1;
      }
      else if(Number(a[prop]) < Number(b[prop])){
        return -1;
      }
      return 0;
    }
  }
  
  getVesselBackgroundClass(inputVVDto: VesselVisitDTO) {
    return getVesselBackgroundClass(inputVVDto);
  }

  getColorPhase(phase) {
    return getColorPhase(phase);    
  }

  getOperatorPicture(operator) {
    return getOperatorPicture(operator);    
  }

  calculateTitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateTitleMoves(vesselVisitDTO);
  }

  calculateSubtitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateSubtitleMoves(vesselVisitDTO);
  }

  calculatePercentMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculatePercentMoves(vesselVisitDTO);
  }
  
  calculateLoadPercentMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateLoadPercentMoves(vesselVisitDTO);
  }
  calculateLoadTitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateLoadTitleMoves(vesselVisitDTO);
  }
  
  calculateLoadSubtitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateLoadSubtitleMoves(vesselVisitDTO);
  }
  calculateDschPercentMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateDschPercentMoves(vesselVisitDTO);
  }
  calculateDschTitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateDschTitleMoves(vesselVisitDTO);
  }
  calculateDschSubtitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateDschSubtitleMoves(vesselVisitDTO);
  }

}