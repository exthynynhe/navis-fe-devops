import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselVisitWorkflowDetailsComponent } from './vessel-visit-workflow-details.component';

describe('VesselVisitWorkflowDetailsComponent', () => {
  let component: VesselVisitWorkflowDetailsComponent;
  let fixture: ComponentFixture<VesselVisitWorkflowDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VesselVisitWorkflowDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VesselVisitWorkflowDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
