import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusMovesWorkflowDetailsComponent } from './status-moves-workflow-details.component';

describe('StatusMovesWorkflowDetailsComponent', () => {
  let component: StatusMovesWorkflowDetailsComponent;
  let fixture: ComponentFixture<StatusMovesWorkflowDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusMovesWorkflowDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusMovesWorkflowDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
