import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { VesselVisitDTO } from '../../../model/VesselVisitDTO';
import { PlanningNoteDTO } from '../../../model/PlanningNoteDTO';
import { RestFulAPIService } from '../../../shared/services/restfulapi.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { getCurrentUser } from '../../../global/utils';
import { WebSocketService } from '../../../shared/services/websocket.service';
import { enableAllButtons, disableAllButtons } from '../../../global/utils';

@Component({
  selector: 'e2e-status-moves-workflow-details',
  templateUrl: './status-moves-workflow-details.component.html',
  styleUrls: ['./status-moves-workflow-details.component.scss']
})

export class StatusMovesWorkflowDetailsComponent implements OnInit {

  @Input() vesselVisitDTO: VesselVisitDTO;
  @Output() tabEvent = new EventEmitter<any>();
  showAlert: string = "none";
  alertMessage: string = "";
  public vvNotes: Array<PlanningNoteDTO> = [];

  constructor(private restFulAPI: RestFulAPIService, private webSocketService: WebSocketService) {         
    this.webSocketService.vvnDTOListObvbl$.subscribe(data => {            
      this.addVesselVisitNoteDTOList(data);
      this.sortVesselVisitNotes(this.vvNotes);
    })
  }

  ngOnInit() {
    this.getVesselVisitNotes();
  }

  switchToMovesScreen() {    
    this.tabEvent.emit(2);
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.vvNotes, event.previousIndex, event.currentIndex);
  }
  
  addVesselVisitNoteDTOList(vvnDto: PlanningNoteDTO) {          
    // Check if exists
    var elementExists = false;
    this.vvNotes.forEach(element => {
      if(element.noteId === vvnDto.noteId) {
        elementExists = true;
        return;
      }
    });
    // Remove item
    if(elementExists) {
      this.vvNotes = this.vvNotes.filter(function( obj ) {
        return obj.noteId !== vvnDto.noteId;
      });  
    }
    // Add item
    this.vvNotes.push(vvnDto);    
  }

  getVesselVisitNotes() { 
    this.vvNotes = [];
    this.restFulAPI.getVesselVisitNotes(this.vesselVisitDTO.vesselVisitName).subscribe(res => { 
      this.vvNotes = res;
      this.sortVesselVisitNotes(this.vvNotes);         
    });
  }

  sortVesselVisitNotes(notes: PlanningNoteDTO[]) {
    notes.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());    
  }

  addVesselVisitNote() {
    // Check if already exists an empty row
    var allowAdd = true;
    this.vvNotes.forEach(function(element) {      
      if(element.noteId === "") {
        allowAdd = false;
      }
    });
    // Allow or not the edition
    if(allowAdd) {
      let vvNote = new PlanningNoteDTO();
      vvNote.noteId = "";      
      vvNote.textMessage = "";
      vvNote.date = new Date();
      vvNote.vesselVisitName = "";
      vvNote.username = getCurrentUser();
      vvNote.active = true;    
      vvNote.doneStatus = false;
      vvNote.doneDate = undefined;
      vvNote.doneUsername = "";
      this.vvNotes.push(vvNote);
      this.sortVesselVisitNotes(this.vvNotes);
    }
  }

  planningNoteChanged(vvNote: PlanningNoteDTO) {
    vvNote.doneDate = new Date();
    vvNote.doneUsername = getCurrentUser();
    vvNote.doneStatus = !vvNote.doneStatus; 
    this.modifyExistingVesselVisitNote(vvNote);
  }

  editVesselVisitNote(vvNote: PlanningNoteDTO) {
    this.enterEditMode(vvNote);
  }

  cancelVesselVisitNote(vvNote: PlanningNoteDTO) {
    this.enterReadMode(vvNote, false);
  }

  removeVesselVisitNoteArray(vvNoteId: String) {
    // Remove the initial empty one
    this.vvNotes = this.vvNotes.filter(function( obj ) {      
      return obj.noteId !== vvNoteId;
    });
  }

  alert(message: string){
    this.showAlert = 'block';
    this.alertMessage = message;
  }

  saveVesselVisitNote(vvNote: PlanningNoteDTO) {              
    this.getVesselVisitNoteData(vvNote.noteId, vvNote);
    if(vvNote.textMessage.trim() === "") {
      this.alert('Please enter text in the planning notes.');
      return;
    }
    if(vvNote.noteId === "") {       
      console.log(this.vvNotes);
      vvNote.noteId = null;
      vvNote.active = true;
      vvNote.doneStatus = false;
      vvNote.doneDate = undefined;
      vvNote.doneUsername = "";
      // Save the new one
      this.saveNewVesselVisitNote(vvNote);
    } else {      
      vvNote.active = vvNote.active;
      vvNote.doneStatus = vvNote.doneStatus;
      vvNote.doneDate = vvNote.doneDate;
      vvNote.doneUsername = vvNote.doneUsername;
      this.modifyExistingVesselVisitNote(vvNote);
    }    
  }
  
  getVesselVisitNoteData(currentId: String, newVvNote: PlanningNoteDTO) {    
    newVvNote.date = new Date();
    newVvNote.textMessage = (document.getElementById("vvnInput " + currentId) as HTMLInputElement).value;
    newVvNote.username = getCurrentUser();       
    newVvNote.vesselVisitName = this.vesselVisitDTO.vesselVisitName;    
  }

  saveNewVesselVisitNote(vvNote: PlanningNoteDTO) {
    this.restFulAPI.addVesselVisitNote(vvNote).subscribe(data => {             
      this.sortVesselVisitNotes(this.vvNotes);
      vvNote.noteId = "";
      this.vvNotes = this.vvNotes.filter(function( obj ) {
        return obj.noteId !== vvNote.noteId;
      }); 
      enableAllButtons();           
    });    
  }
  
  modifyExistingVesselVisitNote(vvNote: PlanningNoteDTO) {
    this.restFulAPI.modifyVesselVisitNote(vvNote).subscribe(data => {             
      this.sortVesselVisitNotes(this.vvNotes);
      this.enterReadMode(vvNote, false);           
    });     
  }

  enterReadMode(vvNote: PlanningNoteDTO, newVvNote: boolean) {
    enableAllButtons();
    if(vvNote.textMessage.trim() === "") {
      this.vvNotes.splice(this.vvNotes.indexOf(vvNote), 1);
      return;
    }
    var noteId: String = "";
    if(!newVvNote) {
      noteId = vvNote.noteId;
    } else {
      noteId = "";
    }    
    document.getElementById("vvnEditButtons " + noteId).style.display = "none";
    document.getElementById("vvnReadButtons " + noteId).style.display = "block";
    document.getElementById("vvnCommentsInput " + noteId).style.display = "none";
    document.getElementById("vvnCommentsValue " + noteId).style.display = "block";          
  }

  enterEditMode(terminalNote: PlanningNoteDTO) {
    disableAllButtons();
    document.getElementById("vvnEditButtons " + terminalNote.noteId).style.display = "block";
    document.getElementById("vvnReadButtons " + terminalNote.noteId).style.display = "none";
    document.getElementById("vvnCommentsInput " + terminalNote.noteId).style.display = "block";
    document.getElementById("vvnCommentsValue " + terminalNote.noteId).style.display = "none";    
    // Show editing buttons
    (document.getElementById("saveVesselVisitNote " + terminalNote.noteId) as HTMLButtonElement).disabled = false;
    (document.getElementById("cancelVesselVisitNote " + terminalNote.noteId) as HTMLButtonElement).disabled = false;
  }
}

