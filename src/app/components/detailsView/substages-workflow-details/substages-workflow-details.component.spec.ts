import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubstagesWorkflowDetailsComponent } from './substages-workflow-details.component';

describe('SubstagesWorkflowDetailsComponent', () => {
  let component: SubstagesWorkflowDetailsComponent;
  let fixture: ComponentFixture<SubstagesWorkflowDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubstagesWorkflowDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubstagesWorkflowDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
