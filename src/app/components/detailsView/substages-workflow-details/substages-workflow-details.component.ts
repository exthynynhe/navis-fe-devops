import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { VesselVisitDTO } from '../../../model/VesselVisitDTO';
import { StageDTO } from '../../../model/StageDTO';
import { getStageIcon } from '../../../global/utils';
import { getCurrentUser } from '../../../global/utils';
import { RestFulAPIService } from '../../../shared/services/restfulapi.service';
import { MatTabChangeEvent } from '@angular/material';
import { FormControl } from '@angular/forms';
import { UnitDTO } from '../../../model/UnitDTO';
import { UnitsRecapDTO } from '../../../model/UnitsRecapDTO';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { VesselNoteDTO } from '../../../model/VesselNoteDTO';
import { WebSocketService } from '../../../shared/services/websocket.service';
import { enableAllButtons, disableAllButtons } from '../../../global/utils';
import { Globals } from '../../../global/globals';
import { AppSettingDTO } from 'src/app/model/AppSettingDTO';
import { NGXLogger } from 'ngx-logger';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'e2e-substages-workflow-details',
  templateUrl: './substages-workflow-details.component.html',
  styleUrls: ['./substages-workflow-details.component.scss']
})

export class SubstagesWorkflowDetailsComponent implements OnInit {

  @Input() vesselVisitDTO: VesselVisitDTO;
  @Input() selectedStage: StageDTO;
  @Input() remainingStages: StageDTO[];
  @Input() selectedTab: FormControl;
  @Input() unitRecapDTO: UnitsRecapDTO;
  @Output() messageEvent = new EventEmitter<StageDTO>();    
  @Input() showAllTasksFromPage;
  @Input() showRemTasksFromPage;
  public showAlert = 'none';
  public showConfirm = 'none';
  public alertMessage = "";
  public confirmMessage = "";
  public confirmCallback;
  public confirmElement;

  public p1;
  public p2;
  public amountItemsPagination: number;
  public vesselNotes: Array<VesselNoteDTO> = [];
  public unitDTOLst: UnitDTO[] = new Array<UnitDTO>();
  public appSetting: AppSettingDTO;

  constructor(private spinner: NgxSpinnerService, private restFulAPI: RestFulAPIService, private webSocketService: WebSocketService, private globals: Globals, private logger: NGXLogger) {     
    if(this.globals.settingDTO === undefined) {
      this.globals.loadApplicationSettings();      
    }
    this.appSetting = this.globals.settingDTO;
    this.amountItemsPagination = this.appSetting.amountItemsPagination;
    this.webSocketService.vnDTOListObvbl$.subscribe(data => {            
      if(this.vesselVisitDTO.vesselName == data.vesselName){
        this.addVesselNoteDTOList(data);
        this.sortVesselNotes(this.vesselNotes);
      }
    })    
  }
  
  ngOnInit() { 
    this.getVesselNotes();   
    if(this.selectedStage !== undefined && this.selectedStage.children !== undefined) {  
      this.orderStages();
      this.orderSelectedStageSubstages(); 
    }
  }

  ngOnChanges() {
    this.p1 = this.showAllTasksFromPage;
    this.p2 = this.showRemTasksFromPage;
    if(this.selectedStage !== undefined && this.selectedStage.children !== undefined) {
      this.orderStages();
      this.orderSelectedStageSubstages(); 
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.vesselNotes, event.previousIndex, event.currentIndex);
  }

  public updateSelectedTab(event: MatTabChangeEvent) {     
    this.selectedTab.setValue(event); 
    if(this.selectedTab.value == 2) {
      this.getVesselVisitUnits();
    }     
  }

  public getSubstagesOrderedByStatus() : Array<StageDTO> {
    var completedArray = []
    var openedArray = []
    completedArray = this.selectedStage.children.filter(function( obj ) {
      return obj.completed === true;
    });
    openedArray = this.selectedStage.children.filter(function( obj ) {
      return obj.completed === false;
    });
    completedArray.sort((a, b) => {
      if(a.name < b.name) { return -1; }
      if(a.name > b.name) { return 1; }
      return 0;        
    });    
    openedArray.sort((a, b) => {
      if(a.name < b.name) { return -1; }
      if(a.name > b.name) { return 1; }
      return 0;        
    });      
    return openedArray.concat(completedArray);    
  }

  public orderStages() {    
      for(var i = 0; i < this.vesselVisitDTO.stages.length; i++) {
        this.vesselVisitDTO.stages[i].children.sort((a, b) => {
          return a.position - b.position;
        });        
      }
      this.vesselVisitDTO.stages.sort((a, b) => {
        return a.position - b.position;
      });            
  }

  public orderSelectedStageSubstages() {                
    this.selectedStage.children.sort((a, b) => {
      if(a.name < b.name) { return -1; }
      if(a.name > b.name) { return 1; }
      return 0;        
    });        
  }

  public findParentStage(substage) : StageDTO {
    var parentStage : StageDTO;
    for(var i=0; i < this.vesselVisitDTO.stages.length; i++) {
      // Check if it is same stage
      if (this.vesselVisitDTO.stages[i].stageTemplateId === substage.stageTemplateId) {
        parentStage = this.vesselVisitDTO.stages[i];
        break;
      }
      // Search in the children stages
      if (this.vesselVisitDTO.stages[i].children && this.vesselVisitDTO.stages[i].children.length > 0) {
        for(var j=0; j<this.vesselVisitDTO.stages[i].children.length; j++) {
          if(this.vesselVisitDTO.stages[i].children[j].stageTemplateId === substage.stageTemplateId) {
            parentStage = this.vesselVisitDTO.stages[i];
            break;
          }          
        }
      }
    }
    return parentStage;
  }

  public createTempStageDataComments(currentStage: StageDTO) : StageDTO {
    var newSubstage = Object.assign({}, currentStage);
    newSubstage.comment = (<HTMLInputElement>document.getElementById("input " + currentStage.stageTemplateId)).value;
    newSubstage.commentBy = getCurrentUser();
    newSubstage.commentDate = new Date();     
    return newSubstage;
  }

  public createTempStageDataStatus(currentStage: StageDTO) : StageDTO {
    var newSubstage: StageDTO = Object.assign({}, currentStage); 
    if(currentStage.completed) {
      newSubstage.status = "WAITING";
      newSubstage.batchNumber = null;
    } else {
      newSubstage.status = "COMPLETE";
    }  
    newSubstage.modifiedBy = getCurrentUser();
    newSubstage.modifiedDate = new Date();
    return newSubstage;
  }

  public createTempStageDataTaken(currentStage: StageDTO) : StageDTO {
    var newSubstage = Object.assign({}, currentStage);
    newSubstage.taken = !currentStage.taken;
    newSubstage.takenBy = getCurrentUser();
    newSubstage.takenDate = new Date();
    return newSubstage;
  }
  
  // ############# Methods to take or release the task
  public changeStageTake(currentStage: StageDTO) {
    this.restFulAPI.modifyVVStage(this.createTempStageDataTaken(currentStage), this.vesselVisitDTO).subscribe(data => {      
      this.selectedStage = data; 
      this.messageEvent.emit(this.selectedStage);       
    });
  }

  public changeSubstageTake(substage, selectedStage) {
    this.restFulAPI.modifyVVStageChild(this.createTempStageDataTaken(substage), selectedStage, this.vesselVisitDTO).subscribe(data => {
      for(var i=0; i< this.selectedStage.children.length; i++) {                
        if (this.selectedStage.children[i].stageTemplateId === data.stageTemplateId) {
          this.selectedStage.children[i] = data;
          break;
        }
      }      
      this.orderSelectedStageSubstages();
      this.messageEvent.emit(this.selectedStage); 
    });
  }

  public changeSubstageTakeRemaining(substage) {
    if(this.findParentStage(substage).stageTemplateId === substage.stageTemplateId) {
      this.changeStageTake(substage);
    } else {
      this.changeSubstageTake(substage, this.findParentStage(substage));
    }           
  }

  // ############# Methods to change STATUS
  public changeStageStatus(currentStage: StageDTO) {        
    this.restFulAPI.modifyVVStage(this.createTempStageDataStatus(currentStage), this.vesselVisitDTO).subscribe(data => {      
        // Do not re-asign StageDTO or selected stage, as Vessel Visit is propagated from backend selected stage is asigned in parent
        // this.selectedStage = data; 
        // this.messageEvent.emit(this.selectedStage);
    });          
  }  

  public changeSubstageStatus(substage, selectedStage) {
    this.restFulAPI.modifyVVStageChild(this.createTempStageDataStatus(substage), selectedStage, this.vesselVisitDTO).subscribe(data => {
      // Do not re-asign StageDTO or selected stage, as Vessel Visit is propagated from backend selected stage is asigned in parent
      /*
      for(var i=0; i< this.selectedStage.children.length; i++) {              
        if (this.selectedStage.children[i].stageTemplateId === data.stageTemplateId) {
          this.selectedStage.children[i] = data;
          break;
        }
      }            
      this.messageEvent.emit(this.selectedStage); 
      */
     this.orderSelectedStageSubstages();
    }); 
  }

  public changeSubstageStatusRemaining(substage) {
    if(this.findParentStage(substage).stageTemplateId === substage.stageTemplateId) {
      this.changeStageStatus(substage);
    } else {
      this.changeSubstageStatus(substage, this.findParentStage(substage));
    }           
  }

  public closeAllCoprarRemainingTasks(selStage: StageDTO) {    
      var remainingCoprarStages: StageDTO[] = [];
      for (var i = 0; i < selStage.children.length; i++) {
        if (!selStage.children[i].completed) {
          remainingCoprarStages.push(Object.assign({}, selStage.children[i]));
        }                  
      }
      if(remainingCoprarStages && remainingCoprarStages.length > 0) {
        // Close all in new array
        remainingCoprarStages.forEach( (element) => {
          element.status = "COMPLETE";
          element.modifiedBy = getCurrentUser();
          element.modifiedDate = new Date();
        });
        // Call to REST API
        this.restFulAPI.closeVVStages(remainingCoprarStages, selStage, this.vesselVisitDTO).subscribe(data => {                  
          // Do not re-asign StageDTO or selected stage, as Vessel Visit is propagated from backend selected stage is asigned in parent
          /*
          for(var j=0; j< this.selectedStage.children.length; j++) {   
            for(var k=0; k < data.length; k++) {
              if(this.selectedStage.children[j].stageTemplateId === data[k].stageTemplateId) {
                this.selectedStage.children[j] = data[k];
                break;
              }  
            }                 
          }          
          this.messageEvent.emit(this.selectedStage);                                 
          */
         this.orderSelectedStageSubstages();
        });  
      }      
  }

  // ############# Methods to edit comments
  public editComment(selectedStage) {    
    if(document.getElementById("comments label " + selectedStage.stageTemplateId) !== null) {
      document.getElementById("comments label " + selectedStage.stageTemplateId).style.display = "none";
    }    
    if(document.getElementById("comments input " + selectedStage.stageTemplateId) !== null) {
      document.getElementById("comments input " + selectedStage.stageTemplateId).style.display = "block";
    }    
    document.getElementById("edit button " + selectedStage.stageTemplateId).style.display = "none";
    document.getElementById("save button " + selectedStage.stageTemplateId).style.display = "block";
  }  

  public saveComment(selectedStage) {    
    this.restFulAPI.modifyVVStage(this.createTempStageDataComments(selectedStage), this.vesselVisitDTO).subscribe(data => {            
      this.selectedStage = data; 
      this.messageEvent.emit(this.selectedStage);       
    });  
    if(document.getElementById("comments input " + selectedStage.stageTemplateId) !== null) {
      document.getElementById("comments input " + selectedStage.stageTemplateId).style.display = "none";
    }
    if(document.getElementById("comments label " + selectedStage.stageTemplateId) !== null) {
      document.getElementById("comments label " + selectedStage.stageTemplateId).style.display = "block";
    }        
    document.getElementById("edit button " + selectedStage.stageTemplateId).style.display = "block";
    document.getElementById("save button " + selectedStage.stageTemplateId).style.display = "none";
  }

  public saveCommentSubstage(substage, selectedStage) {    
    this.restFulAPI.modifyVVStageChild(this.createTempStageDataComments(substage), selectedStage, this.vesselVisitDTO).subscribe(data => {            
      for(var i=0; i< this.selectedStage.children.length; i++) {        
        if (this.selectedStage.children[i].stageTemplateId === data.stageTemplateId) {
          this.selectedStage.children[i] = data;
          break;
        }
      }      
      this.orderSelectedStageSubstages();
      this.messageEvent.emit(this.selectedStage);       
    });  
    if(document.getElementById("comments input " + substage.stageTemplateId) !== null) {
      document.getElementById("comments input " + substage.stageTemplateId).style.display = "none";
    }
    if(document.getElementById("comments label " + substage.stageTemplateId) !== null) {
      document.getElementById("comments label " + substage.stageTemplateId).style.display = "block";
    }        
    document.getElementById("edit button " + substage.stageTemplateId).style.display = "block";
    document.getElementById("save button " + substage.stageTemplateId).style.display = "none";
  }

  public saveCommentSubstageRemaining(substage) {
    if(this.findParentStage(substage).stageTemplateId === substage.stageTemplateId) {
      this.saveComment(substage);
    } else {
      this.saveCommentSubstage(substage, this.findParentStage(substage));
    } 
  }
  
  public getStageIcon(stage): String {    
    return getStageIcon(stage);
  }

  // Moves Recap methods
  public getContainersTotal(moveKind: string, type: String) {
    return this.unitDTOLst.filter((obj) => obj.moveKind === moveKind && obj.freightKind === type).length;         
  }

  public getVesselVisitUnits() {
    if(this.vesselVisitDTO.active) {
      this.restFulAPI.getVesselVisitUnits(this.vesselVisitDTO).subscribe(res => {     
      },
        err => {
          this.logger.debug(`getVesselVisitUnits| ${err}`);
        });
    }    
  }


  // ############# Methods to handle Vessel Notes comments
  addVesselNoteDTOList(vnDto: VesselNoteDTO) {          
    // Check if exists
    var elementExists = false;
    this.vesselNotes.forEach(element => {
      if(element.noteId === vnDto.noteId) {
        elementExists = true;
        return;
      }
    });
    // Remove item
    if(elementExists) {
      this.vesselNotes = this.vesselNotes.filter(function( obj ) {
        return obj.noteId !== vnDto.noteId;
      });  
    }
    // Add item, only if active = true
    if(vnDto.active) {
      this.vesselNotes.push(vnDto);    
    }        
  }

  getVesselNotes() { 
    this.vesselNotes = [];
    this.restFulAPI.getVesselNotes(this.vesselVisitDTO.vesselName).subscribe(res => { 
      this.vesselNotes = res;
      this.sortVesselNotes(this.vesselNotes);         
    });
  }

  sortVesselNotes(notes: VesselNoteDTO[]) {
    notes.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());    
  }

  addVesselNote() {
    // Check if already exists an empty row
    var allowAdd = true;
    this.vesselNotes.forEach(function(element) {      
      if(element.noteId === "") {
        allowAdd = false;
      }
    });
    // Allow or not the edition
    if(allowAdd) {
      let vNote = new VesselNoteDTO();
      vNote.noteId = "";      
      vNote.textMessage = "";
      vNote.date = new Date();
      vNote.vesselName = "";
      vNote.active = true;  
      vNote.username = getCurrentUser();      
      this.vesselNotes.push(vNote);
      this.sortVesselNotes(this.vesselNotes);
    }
  }

  
  removeNote(){
    this.showConfirm = 'none';
    let vesselNote = this.confirmElement;
    if(vesselNote.noteId === "") {                   
      this.removeVesselNoteArray(vesselNote.noteId);  
      this.sortVesselNotes(this.vesselNotes);      
      this.enterReadMode(vesselNote, false);     
    } else { 
      vesselNote.active = false;      
      this.restFulAPI.deleteVesselNote(vesselNote).subscribe(data => {       
        this.removeVesselNoteArray(vesselNote.noteId);  
        this.sortVesselNotes(this.vesselNotes);           
      }); 
    }
  }

  removeVesselNote(vesselNote: VesselNoteDTO) {
    this.confirm("This note will be deleted.", vesselNote, this.removeNote);      
  }

  confirm(message: string, element: VesselNoteDTO, callback){
    this.showConfirm = 'block';
    this.confirmMessage = message;
    this.confirmCallback = callback;
    this.confirmElement = element;
  }

  editVesselNote(vNote: VesselNoteDTO) {
    this.enterEditMode(vNote);
  }

  cancelVesselNote(vNote: VesselNoteDTO) {
    this.enterReadMode(vNote, false);
  }

  removeVesselNoteArray(vNoteId: String) {
    // Remove the initial empty one
    this.vesselNotes = this.vesselNotes.filter(function( obj ) {      
      return obj.noteId !== vNoteId;
    });
  }

  saveVesselNote(vNote: VesselNoteDTO) {           
    if(vNote.noteId === "") {  
      this.getVesselNoteData(vNote.noteId, vNote); 
      // Save the new one
      this.saveNewVesselNote(vNote);
    } else {      
      this.getVesselNoteData(vNote.noteId, vNote); 
      this.modifyExistingVesselNote(vNote);
    }    
  }

  getVesselNoteData(currentId: String, newVNote: VesselNoteDTO) {    
    newVNote.date = new Date();
    newVNote.textMessage = (document.getElementById("vnInput " + currentId) as HTMLInputElement).value;
    newVNote.username = getCurrentUser(); 
    newVNote.active = true;      
    newVNote.vesselName = this.vesselVisitDTO.vesselName;    
  }

  saveNewVesselNote(vesselNote: VesselNoteDTO) {
    this.spinner.show();
    this.restFulAPI.addVesselNote(vesselNote).subscribe(data => {             
      this.sortVesselNotes(this.vesselNotes);
      vesselNote.noteId = "";
      this.removeVesselNoteArray(vesselNote.noteId);
      enableAllButtons();    
      this.spinner.hide();      
    });    
  }
  
  modifyExistingVesselNote(vesselNote: VesselNoteDTO) {
    this.spinner.show();
    this.restFulAPI.modifyVesselNote(vesselNote).subscribe(data => {             
      this.sortVesselNotes(this.vesselNotes);
      this.enterReadMode(vesselNote, false);    
      this.spinner.hide();       
    });     
  }

  enterReadMode(vesselNote: VesselNoteDTO, newVNote: boolean) {
    enableAllButtons();
    var noteId: String = "";
    if(!newVNote) {
      noteId = vesselNote.noteId;
    } else {
      noteId = "";
    } 
    document.getElementById("vnEditButtons " + vesselNote.noteId).style.display = "none";
    document.getElementById("vnReadButtons " + vesselNote.noteId).style.display = "block";
    document.getElementById("vnCommentsInput " + vesselNote.noteId).style.display = "none";
    document.getElementById("vnCommentsValue " + vesselNote.noteId).style.display = "block";          
    
  }

  enterEditMode(vesselNote: VesselNoteDTO) {
    disableAllButtons();
    document.getElementById("vnEditButtons " + vesselNote.noteId).style.display = "block";
    document.getElementById("vnReadButtons " + vesselNote.noteId).style.display = "none";
    document.getElementById("vnCommentsInput " + vesselNote.noteId).style.display = "block";
    document.getElementById("vnCommentsValue " + vesselNote.noteId).style.display = "none";    
    // Show editing buttons
    (document.getElementById("saveVesselNote " + vesselNote.noteId) as HTMLButtonElement).disabled = false;
    (document.getElementById("cancelVesselNote " + vesselNote.noteId) as HTMLButtonElement).disabled = false;
  }
}

