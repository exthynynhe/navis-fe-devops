import { Component, OnInit } from '@angular/core';
import { WebSocketService } from '../../../shared/services/websocket.service';
import { VesselVisitDTO } from '../../../model/VesselVisitDTO';
import { RestFulAPIService } from '../../../shared/services/restfulapi.service';
import { ActivatedRoute } from '@angular/router';
import { StageDTO } from '../../../model/StageDTO';
import * as _ from 'lodash';
import { FormControl } from '@angular/forms';
import { UnitsRecapDTO } from '../../../model/UnitsRecapDTO';
import { UnitCountDTO } from '../../../model/UnitCountDTO';
import { NgxSpinnerService } from 'ngx-spinner';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'e2e-index-workflow-details',
  templateUrl: './index-workflow-details.component.html',
  styleUrls: ['./index-workflow-details.component.scss']
})

export class IndexWorkflowDetailsComponent implements OnInit {

  public unitRecapDTO: UnitsRecapDTO;
  public vesselVisitDTO: VesselVisitDTO;    
  public selectedStage: StageDTO;
  public remainingStages: StageDTO[];
  public selectedTab = new FormControl(0);

  constructor(private route: ActivatedRoute, private restfulAPIService: RestFulAPIService, private webSocketService: WebSocketService, private spinner: NgxSpinnerService, private logger: NGXLogger) {          
    this.startUnitRecap();    
    this.webSocketService.vvDTOListObvbl$.subscribe(data => {      
      const newVV = <VesselVisitDTO>data;
      if (this.vesselVisitDTO && (newVV.vesselVisitName === this.vesselVisitDTO.vesselVisitName)) {
          this.updateVesselVisit(newVV);
          this.updateRemainingStages();          
        }    
    });
    this.webSocketService.unrcpDTOListObvbl$.subscribe(data => {
      const unitRecap = <UnitsRecapDTO>data;
      if (this.vesselVisitDTO && (unitRecap.vesselVisitName === this.vesselVisitDTO.vesselVisitName)) {
          this.updateUnitRecap(unitRecap);      
        }    
    });
  }

  ngOnInit() {
    try {  
      this.spinner.show();    
      this.route.paramMap
      .switchMap(params => this.restfulAPIService.getVesselVisit(params.get('id')))
      .subscribe(data => {          
          this.updateVesselVisit(<VesselVisitDTO>data);
          if (this.vesselVisitDTO.stages && this.vesselVisitDTO.stages.length > 0) {
            this.selectedStage = this.vesselVisitDTO.stages[0];           
          } 
          this.updateRemainingStages(); 
          this.unitRecapDTO.vesselVisitName = this.vesselVisitDTO.vesselVisitName; 
          this.spinner.hide();
      });            
    } catch (Error) {
      this.logger.debug('getVesselVisits> ' + Error.message());
      this.spinner.hide();
      return null;
    }
  }

  ngOnClose() {
    this.webSocketService.disconnect();
  }

  public startUnitRecap() {
    var unitCountDeepDTO: UnitCountDTO = {
      "total": 0,
      "nom40": 0,
      "nom20": 0,
      "nom45": 0,
      "full": undefined,
      "empty": undefined,
      "standard": undefined,
      "reefer": undefined,
      "oog": undefined,
      "dg": undefined
    }
    var unitCountIntDTO: UnitCountDTO = {
      "total": 0,
      "nom40": 0,
      "nom20": 0,
      "nom45": 0,
      "full": undefined,
      "empty": undefined,
      "standard": unitCountDeepDTO,
      "reefer": unitCountDeepDTO,
      "oog": unitCountDeepDTO,
      "dg": unitCountDeepDTO
    }
    var unitCountDTO: UnitCountDTO = {
      "total": 0,
      "nom40": 0,
      "nom20": 0,
      "nom45": 0,
      "full": unitCountIntDTO,
      "empty": unitCountIntDTO,
      "standard": undefined,
      "reefer": undefined,
      "oog": undefined,
      "dg": undefined
    }
    this.unitRecapDTO = {
      "vesselVisitName": "",
      "loadRecap": unitCountDTO,      
      "dschRecap": unitCountDTO,        
      "restowRecap": unitCountDTO
    }                    
  }

  public changeSelectedStage($event) {        
    this.selectedStage = $event as StageDTO;
    console.log(this.getSelectedStage());
    console.log(this.selectedStage);
    this.selectedTab.setValue(0);       
  }

  public changeSelectedTab($event) {   
    this.selectedTab.setValue($event as number);    
  }

  private updateUnitRecap(untRecap: UnitsRecapDTO) {
    this.unitRecapDTO = untRecap;
  }

  private updateVesselVisit(vv: VesselVisitDTO): void {
    this.vesselVisitDTO = vv;
    this.sortStagesByPosition();
    if(this.selectedStage !== undefined) {
      for(var i = 0; i < this.vesselVisitDTO.stages.length; i++) {
        if(this.vesselVisitDTO.stages[i].stageTemplateId === this.selectedStage.stageTemplateId) {          
          this.selectedStage = this.vesselVisitDTO.stages[i];
          break;
        }
      }
    } else if(this.selectedStage === undefined && this.vesselVisitDTO.stages && this.vesselVisitDTO.stages.length > 0) {
      this.selectedStage = this.vesselVisitDTO.stages[0];
    }  
  }

  private sortStagesByPosition (): void {
    this.vesselVisitDTO.stages = _.orderBy(this.vesselVisitDTO.stages, ['position'], ['asc']);
  }

  private updateRemainingStages() {
    this.remainingStages = [];
    for(var i=0; i < this.vesselVisitDTO.stages.length; i++) {
      // Stage with substeps
      if (this.vesselVisitDTO.stages[i].children && this.vesselVisitDTO.stages[i].children.length > 0) {
        for(var j=0; j<this.vesselVisitDTO.stages[i].children.length; j++) {
          if(!this.vesselVisitDTO.stages[i].children[j].completed) {
            this.remainingStages.push(this.vesselVisitDTO.stages[i].children[j]);
          }          
        }
      }
      // Stage without substeps
      else {
        if(!this.vesselVisitDTO.stages[i].completed) {
          this.remainingStages.push(this.vesselVisitDTO.stages[i]);
        }        
      }            
    }
  }

  getSelectedStage() {
    let flag = 0;
    this.vesselVisitDTO.stages.forEach((stage) => {
      if(this.selectedStage.stageTemplateId === stage.stageTemplateId) {
        //console.log(this.vesselVisitDTO.stages);
        flag = 1;
      }
    })
    if(flag === 1) return this.selectedStage;
    else if(this.vesselVisitDTO.stages.length > 0) this.selectedStage = this.vesselVisitDTO.stages[0];
    return this.selectedStage;
  }
}
