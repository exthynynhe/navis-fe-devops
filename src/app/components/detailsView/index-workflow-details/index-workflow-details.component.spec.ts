import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexWorkflowDetailsComponent } from './index-workflow-details.component';

describe('IndexWorkflowDetailsComponent', () => {
  let component: IndexWorkflowDetailsComponent;
  let fixture: ComponentFixture<IndexWorkflowDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexWorkflowDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexWorkflowDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
