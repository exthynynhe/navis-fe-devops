import { Component, OnInit, Input } from '@angular/core';
import { KpiDTO } from '../../../model/KpiDTO';
import { WebSocketService } from '../../../shared/services/websocket.service';
import { enableAllButtons, disableAllButtons, getCurrentUser, checkInputValue, getWeekdayNumber } from '../../../global/utils';
import { RestFulAPIService } from '../../../shared/services/restfulapi.service';
import { VesselVisitDTO } from '../../../model/VesselVisitDTO';
import { Globals } from 'src/app/global/globals';
import { AppSettingDTO } from 'src/app/model/AppSettingDTO';

@Component({
  selector: 'e2e-sla-metrics-details',
  templateUrl: './sla-metrics-details.component.html',
  styleUrls: ['./sla-metrics-details.component.scss']
})

export class SlaMetricsDetailsComponent implements OnInit {

  @Input() vesselVisitDTO: VesselVisitDTO;

  public p3;
  public amountItemsPagination: number;
  public kpiDtos: Array<KpiDTO> = [];
  public tempKpiDto: KpiDTO;
  public appSetting: AppSettingDTO;

  public weekDays = [
    { id: 0, value: "Sunday"},
    { id: 1, value: "Monday"},
    { id: 2, value: "Tuesday"},
    { id: 3, value: "Wednesday"},
    { id: 4, value: "Thursday"},
    { id: 5, value: "Friday"},
    { id: 6, value: "Saturday"}    
  ]  

  constructor(private restFulAPI: RestFulAPIService, private webSocketService: WebSocketService, private globals: Globals) {  
    if(this.globals.settingDTO === undefined) {
      this.globals.loadApplicationSettings();      
    }       
    this.appSetting = this.globals.settingDTO;
    this.amountItemsPagination = this.appSetting.amountItemsPagination;

    this.webSocketService.vvkpiDTPListObvbl$.subscribe(data => { 
      var newKpi : KpiDTO = data;
      if(newKpi.vesselVisitName === this.vesselVisitDTO.vesselVisitName) {
        this.addKpiDTOList(newKpi);
        this.sortKpiList(this.kpiDtos);
      }      
    })
  }

  ngOnInit() {
    this.getVesselVisitKpis();
  }

  setColourClass(className: string, tmpKpi: KpiDTO) {
    document.getElementById("vvkpiDifference " + tmpKpi.kpiId).className = className;     
  }

  calculateDifference(tmpKpi: KpiDTO) {
    var returnValue;
    if(tmpKpi.dataType==="STRING" && tmpKpi.actualValue !== undefined && tmpKpi.agreedValue !== undefined) {      
      returnValue = Number(tmpKpi.actualValue) - Number(tmpKpi.agreedValue); 
      if(tmpKpi.name === "CI") {
        returnValue = returnValue.toFixed(2);
      }          
    }
    if(tmpKpi.dataType==="DATE" && tmpKpi.actualValueDate !== undefined && tmpKpi.agreedValueDate) {
      var millisec = (new Date(tmpKpi.actualValueDate).getTime() - new Date(tmpKpi.agreedValueDate).getTime());
      returnValue =  (millisec / (1000 * 60 * 60)).toFixed(0);
    }
    if(returnValue === 0) {
      this.setColourClass('', tmpKpi);
    } else if((returnValue > 0 && tmpKpi.name !== "CI" && tmpKpi.name !== "DISCHMOVES" && tmpKpi.name !== "LOADMOVES") || (returnValue < 0 && tmpKpi.name === "CI")) {
      this.setColourClass('redDifference', tmpKpi);
    } else if((returnValue < 0 && tmpKpi.name !== "CI" && tmpKpi.name !== "DISCHMOVES" && tmpKpi.name !== "LOADMOVES") || (returnValue > 0 && tmpKpi.name === "CI")) {
      this.setColourClass('greenDifference', tmpKpi);
    }    
    return returnValue;
  }

  getVesselVisitKpis() { 
    this.kpiDtos = [];        
    this.restFulAPI.getVesselVisitKpis(this.vesselVisitDTO).subscribe(res => { 
      this.kpiDtos = res;
      this.sortKpiList(this.kpiDtos);         
    });
  }

  sortKpiList(kpiList: KpiDTO[]) {    
    kpiList.sort((a, b) => a.position - b.position);    
  }

  addKpiDTOList(vvkpiDto: KpiDTO) {          
    // Check if exists
    var elementExists = false;
    this.kpiDtos.forEach(element => {
      if(element.kpiId === vvkpiDto.kpiId) {
        elementExists = true;
        return;
      }
    });
    // Remove item
    if(elementExists) {
      this.kpiDtos = this.kpiDtos.filter(function( obj ) {
        return obj.kpiId !== vvkpiDto.kpiId;
      });  
    }
    // Add item
    this.kpiDtos.push(vvkpiDto);    
  }

  editKpi(currentKpi: KpiDTO) {
    this.enterEditMode(currentKpi);
  }

  saveKpi(currentKpi: KpiDTO) {
    currentKpi.date = new Date();
    currentKpi.comment = (document.getElementById("vvkpiInput " + currentKpi.kpiId) as HTMLInputElement).value;
    currentKpi.username = getCurrentUser();
    if(currentKpi.editable && currentKpi.dataType==="STRING") {  
      var agreedValueTemp = (document.getElementById("vvkpiInputValue " + currentKpi.kpiId) as HTMLInputElement).value;
      if(currentKpi.name!=="CI" && agreedValueTemp.includes('.')) {              
        agreedValueTemp = agreedValueTemp.substring(0, agreedValueTemp.indexOf('.')); 
        alert(agreedValueTemp);       
      }
      currentKpi.agreedValue = agreedValueTemp;
    }    
    if(currentKpi.editable && currentKpi.dataType==="DATE") {
      var currentDay = ((<HTMLSelectElement>document.getElementById("vvkpi weekday select " + currentKpi.kpiId)).value.trim());        
      currentKpi.agreedValueDate = new Date(currentKpi.agreedValueDate);
      var distance = (getWeekdayNumber(currentDay) - currentKpi.agreedValueDate.getDay()) % 7;    
      currentKpi.agreedValueDate.setDate(currentKpi.agreedValueDate.getDate() + distance);    
      currentKpi.agreedValueDate.setHours(Number((document.getElementById("vvkpiInputValue hour " + currentKpi.kpiId) as HTMLInputElement).value));
      currentKpi.agreedValueDate.setMinutes(Number((document.getElementById("vvkpiInputValue minute " + currentKpi.kpiId) as HTMLInputElement).value));                        
    }    
    this.restFulAPI.modifyVesselVisitKpi(this.vesselVisitDTO, currentKpi).subscribe(data => {             
      this.sortKpiList(this.kpiDtos);
      this.enterReadMode(currentKpi);          
    }); 
  }

  cancelKpi(currentKpi: KpiDTO) {
    this.enterReadMode(currentKpi);
  }

  enterReadMode(currentKpi: KpiDTO) {
    enableAllButtons();
    document.getElementById("vvkpiEditButtons " + currentKpi.kpiId).style.display = "none";
    document.getElementById("vvkpiReadButtons " + currentKpi.kpiId).style.display = "block";
    document.getElementById("vvkpiCommentsInput " + currentKpi.kpiId).style.display = "none";
    document.getElementById("vvkpiCommentsValue " + currentKpi.kpiId).style.display = "block";    
    document.getElementById("vvkpiAgreedValueValue " + currentKpi.kpiId).style.display = "block";
    document.getElementById("vvkpiAgreedValueInput " + currentKpi.kpiId).style.display = "none";    
  }

  enterEditMode(currentKpi: KpiDTO) {
    disableAllButtons();
    document.getElementById("vvkpiEditButtons " + currentKpi.kpiId).style.display = "block";
    document.getElementById("vvkpiReadButtons " + currentKpi.kpiId).style.display = "none";
    document.getElementById("vvkpiCommentsInput " + currentKpi.kpiId).style.display = "block";
    document.getElementById("vvkpiCommentsValue " + currentKpi.kpiId).style.display = "none";   
    if(currentKpi.editable) {
      document.getElementById("vvkpiAgreedValueValue " + currentKpi.kpiId).style.display = "none";
      document.getElementById("vvkpiAgreedValueInput " + currentKpi.kpiId).style.display = "block"; 
    }   
    // Show editing buttons
    (document.getElementById("saveKpiButton " + currentKpi.kpiId) as HTMLButtonElement).disabled = false;
    (document.getElementById("cancelKpiButton " + currentKpi.kpiId) as HTMLButtonElement).disabled = false;
  }  
  
  checkInputValue(input, minValue, maxValue){
    checkInputValue(input, minValue, maxValue);
  }
}
