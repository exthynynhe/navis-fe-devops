import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlaMetricsDetailsComponent } from './sla-metrics-details.component';

describe('SlaMetricsDetailsComponent', () => {
  let component: SlaMetricsDetailsComponent;
  let fixture: ComponentFixture<SlaMetricsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlaMetricsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlaMetricsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
