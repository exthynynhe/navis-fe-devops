import { AuthenticationService } from './../../../shared/services/authentication.service';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';
import { environment } from '../../../../environments/environment';
import { RestFulAPIService } from '../../../shared/services/restfulapi.service';
import { WebSocketService } from '../../../shared/services/websocket.service';
import { version } from '../../../global/utils';
import { Globals } from 'src/app/global/globals';
import { Observable } from 'rxjs';
//import { UserPreferenceDTO } from '../../app/model/UserPreferenceDTO';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {

  public alertMessage: string = "";
  public showAlert = 'none';
  public isProduction: boolean = environment.production;
  public isAlive: string = localStorage.getItem('webSocketAlive');
  public syncDisabled: boolean = false;
  public timeLeft: number = 120;
  public interval;
  public version;
 
  @Input() username: any;
  
  constructor(public router: Router, private authenticationService: AuthenticationService, private restFulAPI: RestFulAPIService, private webSocketService: WebSocketService, private globals: Globals) {
    this.username = this.authenticationService.getUserName();
    this.version = version;
  }

  alert(message: string){
    this.showAlert = 'block';
    this.alertMessage = message;
  }

  ngOnInit() {
    // Will not connect twice if it is already connected...
    this.webSocketService.connect(); 
  }

  savePreferences(){
    if(this.globals.preferencesDTO){
      this.restFulAPI.modifyUserPreferences(this.globals.preferencesDTO).subscribe( res => {
        if(res){
          
          this.alert("Your preferences have been saved!");
          return res;
        }
        else{
          return null;
        }
      });
    }
  }

  logoff() {
    this.webSocketService.disconnect();
    this.authenticationService.logout();
  }

  openUserManual(){
    window.open(environment.helpFiles, "_blank");
  }

  getWebSocketAlive(): any {
    return localStorage.getItem('webSocketAlive');
  }


  n4sync() {    
    if(!this.syncDisabled) {
        this.timeLeft = 120;
        this.syncDisabled = true;
        this.startTimer();
        this.restFulAPI.syncE2EwithN4().subscribe(res => {
          this.alert("Sync Completed!");
        },
          err => {
            this.alert('Error in N4 SYNC')
        });
          
    }            
  }

  userIsAdmin() {    
    return this.authenticationService.isAdmin();
  }

  userIsManager() {
    return this.authenticationService.isManager();
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {        
        this.timeLeft--;
      } else {
        console.log("eee");
        this.syncDisabled = false;
        clearInterval(this.interval);
      }
    },1000)
  }

  checkDisconnection() {
    if(localStorage.getItem('disconnectionAlerted') === "true") {
      return true;
    }
    else {
      return false;
    }
  }
}
