import { Component, OnInit, ViewChild, HostListener, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { WebSocketService } from '../../../shared/services/websocket.service';
import { VesselVisitDTO } from '../../../model/VesselVisitDTO';
import { SearchItem } from '../../../model/SearchItem';
import { RestFulAPIService } from '../../../shared/services/restfulapi.service';
import { colDescs, disableAllButtons, enableAllButtons, getColorPhase, getOperatorPicture, checkParamsInSession, saveParamInSession, readParamFromSession, readNumberParamFromSession, getVesselBackgroundClass, calculateTitleMoves, calculateSubtitleMoves, calculatePercentMoves, calculateLoadTitleMoves, calculateLoadSubtitleMoves, calculateLoadPercentMoves, calculateDschTitleMoves, calculateDschSubtitleMoves, calculateDschPercentMoves } from '../../../global/utils';
import { NoteDTO } from '../../../model/NoteDTO';
import { getCurrentUser } from '../../../global/utils';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { LabelType } from 'ng5-slider';
import { NgxSpinnerService } from 'ngx-spinner';
import { VesselNoteCountDTO } from 'src/app/model/VesselNoteCountDTO';
import { Globals } from '../../../global/globals';
import { UserSearchPresistanceService } from 'src/app/shared/services/user-search-presistance.service';
import { UserPreferenceDTO } from 'src/app/model/UserPreferenceDTO';
import { ColumnDescDTO } from 'src/app/model/ColumnDescDTO';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'e2e-vessel-visit-list',
  templateUrl: './vessel-visit-list.component.html',
  styleUrls: ['./vessel-visit-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})

export class VesselVisitListComponent implements OnInit {
  public searchTerms: Array<SearchItem> =  [];  
  public showBarges: Boolean = true;
  public showWorking: Boolean = true;
  public showDeepseas: Boolean = true;
  public showFeeders: Boolean = true;
  public dateRange: Date[];
  public minDateValue: number;
  public maxDateValue: number;
  public columns: Array<ColumnDescDTO> = [];    
  public vvListDTO: Array<VesselVisitDTO> = [];
  public vvListDTO_Original: Array<VesselVisitDTO> = [];
  public searchList: Array<VesselVisitDTO> = [];
  public terminalNotes: Array<NoteDTO> = [];  
  public showAdvancedSearch: Boolean; 
  public showColumnsVPanel: Boolean;  
  private typedKey: String = '';
  public vesselNotesCount: Array<VesselNoteCountDTO> = [];    
  public intervalHolder: any;
  public searchterm: any;
  public toggleChecked: boolean;
  public preferencesDTO: UserPreferenceDTO;
  public preferedLookAhead: String;
  public preferedLookBack: String;
  public showConfirm = 'none';
  public confirmMessage = "";
  public confirmCallback;
  public confirmElement;
  public showBorder = "";

  @ViewChild('vesselName') inpVesselName;
  @ViewChild('line') inpLine;
  @ViewChild('classification') inpClassification;
  @ViewChild('phase') inpPhase;
  @ViewChild('service') inpService;
  @ViewChild('facility') inpFacility;
  @ViewChild('startDate') inpStartDate;
  @ViewChild('endDate') inpEndDate;
  @ViewChild('instantSearch') inpInstantSearch;
  @ViewChild('advancedSearchPanel') advancedSearchPanel;    
  @ViewChild('advancedSearchButton') advancedSearchButton;  
  @ViewChild('columnsVisibilityPanel') columnsVisibilityPanel;
  @ViewChild('columnsVisibilityButton') columnsVisibilityButton;
  @ViewChild('toggleButton') toggleBtn;
  @ViewChild('timeSlider') timeSlider;
  @ViewChild('vesselVisitName') inpVesselVisitName;
  
  
  constructor(private restFulAPI: RestFulAPIService, private webSocketService: WebSocketService, private spinner: NgxSpinnerService, 
    private router: Router, private globals: Globals, private userSearchPersistance: UserSearchPresistanceService, private cdRef: ChangeDetectorRef, private logger: NGXLogger) {
    this.dateRange = this.createDateRange();
    
    if(globals.settingDTO === undefined) {
      globals.loadApplicationSettings();
    }
    if(globals.preferencesDTO === undefined){
      var currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if(currentUser) {
        var username = currentUser.username;
        globals.loadUserPreferences(username);
      }
    }        
    this.fillColumnsVisibility();
    this.initializeSearchItems();
    this.showAdvancedSearch = false;    
    this.showColumnsVPanel = false;
    this.webSocketService.vvDTOListObvbl$.subscribe(data => {
      this.addToVesselVisitDTOLists(data, !(this.getActiveSearchItems().length === 0));
      // Archive search disabled, filter vessel visits, disable unactive and sort
      if(this.getActiveSearchItems().length === 0) {        
        this.filterVesselVisits(true, true);       
      } else { 
        // Archive search enabled, only sort        
        this.sortVesselVisitsByETADESC(); 
      }
    });
    this.webSocketService.tnDTOListObvbl$.subscribe(data => {   
      this.addTerminalNoteDTOList(data);
      this.sortTerminalNotes(this.terminalNotes);
    });   
  }

  ngOnInit() {      
    this.getAllVesselVisits();    
    this.getTerminalNotes();   
    this.restFulAPI.getApplicationSettings().subscribe(res => {         
      this.globals.updateApplicationSettings(res);  
      if(this.globals.getAutoUpdateStarted() === false) {   
        // Auto - refresh dashboard    
        this.intervalHolder = setInterval(() => {
          // Refresh the list, if not archive search            
          if(this.router.url === '/dashboard-management' && this.globals.settingDTO !== undefined && 
            (this.globals.settingDTO.autoUpdateWithFilters === true || this.getActiveSearchItems().length === 0)) {            
              if(this.globals.settingDTO.autoUpdateWithFilters === true && this.getActiveSearchItems().length > 0) {
                // Clear filters and refresh dashboard
                this.resetClick();
              } else {
                // Refresh dashboard
                this.refreshSliderVesselVisits();
              }          
          }      
        }, 1000 * 60 * this.globals.settingDTO.autoUpdateFrequency);  
        this.globals.setAutoUpdateStarted(true);
      }      
    },
    err => {
        this.logger.debug(`getApplicationSettings| ${err}`);            
    });             
  }

  getSortOrder(prop){
    return function(a,b){
      if(Number(a[prop]) > Number(b[prop])){
        return 1;
      }
      else if(Number(a[prop]) < Number(b[prop])){
        return -1;
      }
      return 0;
    }
  }

  async fillColumnsVisibility() {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser) {
      var username = currentUser.username;
      await this.restFulAPI.getUserPreferences(username).subscribe(res => {
        this.globals.updateUserPreferences(res);
        if(this.globals.preferencesDTO){
          this.preferencesDTO = this.globals.preferencesDTO;
          if(this.preferencesDTO.preferences !== undefined && this.preferencesDTO.preferences !== null){
            var preferences = JSON.parse(this.preferencesDTO.preferences);
            this.showBarges = preferences.showBarges;
            this.showDeepseas = preferences.showDeepsea;
            this.showFeeders = preferences.showFeeders;
            this.preferedLookAhead = preferences.lookAhead;
            this.preferedLookBack = preferences.lookBack;
            this.showWorking = preferences.showWorkingVessels;  
            this.columns = preferences.colDescs;
            // If amount of columns saved in user preferences is lower than master, add those...          
            if(this.columns.length < colDescs.length) {
              colDescs.forEach(column => {
                let findColumn = this.columns.filter(x => x.columnDescId == column.columnDescId)[0];                            
                if(findColumn === undefined) {                
                  this.columns.push(column);
                }
              });  
              // Save user preferences with new columns...                        
              var temp = JSON.parse(this.globals.preferencesDTO.preferences);
              temp.colDescs = this.columns;
              this.globals.preferencesDTO.preferences = JSON.stringify(temp);
              this.restFulAPI.modifyUserPreferences(this.globals.preferencesDTO).subscribe( res => {
                if(res){
                  this.preferencesDTO = this.globals.preferencesDTO;                
                }              
              });                                    
            }           
          }
      } else {
          this.columns = colDescs;
          this.showBarges = true;
          this.showDeepseas = true;
          this.showFeeders = true;
          this.preferedLookAhead = "7";
          this.preferedLookBack = "5";
          this.showWorking = true;
        }
      
        this.refreshSliderVesselVisits();
      });
      this.columns.sort(this.getSortOrder("columnDescOrder"));
    }
  }

  initializeSearchItems() {
    this.searchTerms = [
      { "id":0,"name":"vesselName","chipName":"Vessel Name:","attributeName":"vesselName","value":"","comparisonChar":"~"},
      { "id":1,"name":"classification","chipName":"Classification:","attributeName":"classification","value":"","comparisonChar":":"},
      { "id":2,"name":"operator","chipName":"Operator:","attributeName":"operator","value":"","comparisonChar":"~"},
      { "id":3,"name":"phase","chipName":"Phase:","attributeName":"phase","value":"","comparisonChar":":"},
      { "id":4,"name":"service","chipName":"Service:","attributeName":"service","value":"","comparisonChar":"~"},
      { "id":5,"name":"facility","chipName":"Facility:","attributeName":"facility","value":"","comparisonChar":"~"},
      { "id":6,"name":"startDate","chipName":"Start Date:","attributeName":"eta","value":"","comparisonChar":">"},
      { "id":7,"name":"endDate","chipName":"End Date:","attributeName":"eta","value":"","comparisonChar":"<"},
      { "id":8,"name":"vesselVisitName","chipName":"Vessel Visit Id:","attributeName":"vesselVisitName","value":"","comparisonChar":"~"}
    ]
    this.userSearchPersistance.initialiseSearchItems(this.searchTerms);
  }

  refreshSliderVesselVisits() {
    this.dateRange = this.createDateRange();
 
    this.timeSlider.options = {
      stepsArray: this.dateRange.map((date: Date) => {
        return { value: date.getTime() };
      }),
      translate: (value: number, label: LabelType): string => {            
        return moment(new Date(value)).format('DD/MM/YYYY HH:mm');
      },    
      noSwitching: true,
      disabled: (this.getActiveSearchItems().length > 0)
    };  
    this.getAllVesselVisits();
  }

  @HostListener('document:click', ['$event.target'])
    onClick(targetElement) {
      // Hide advanced search panel when clicking outside
      if(this.advancedSearchPanel != undefined && this.advancedSearchButton != undefined) {        
        const clickedInside = this.advancedSearchPanel.nativeElement.contains(targetElement);
        const clickedOnButton = this.advancedSearchButton.nativeElement.contains(targetElement)
        if(!clickedInside) {
          if(!clickedOnButton) {
            this.showAdvancedSearch = false;
          }
        }
      } 
      // Hide advanced panel when clicking outside
      if(this.columnsVisibilityPanel != undefined && this.columnsVisibilityButton != undefined) {        
        const clickedInsideC = this.columnsVisibilityPanel.nativeElement.contains(targetElement);
        const clickedOnButtonC = this.columnsVisibilityButton.nativeElement.contains(targetElement)
        if(!clickedInsideC) {
          if(!clickedOnButtonC) {
            this.showColumnsVPanel = false;
          }          
        }
      }     
  }

  saveParamsInSession() {
    saveParamInSession("showBarges", this.showBarges, undefined);
    saveParamInSession("showWorkingVessels", this.showWorking, undefined);
    saveParamInSession("showDeepseas", this.showDeepseas, undefined);
    saveParamInSession("showFeeders", this.showFeeders, undefined);
    saveParamInSession("minDateValue", undefined, this.minDateValue);
    saveParamInSession("maxDateValue", undefined, this.maxDateValue); 
    for(let column of this.columns) {
      saveParamInSession("col" + column.columnDescId.toString(), column.columnDescShow, undefined);  
    }
    var temp = JSON.parse(this.globals.preferencesDTO.preferences);
    temp.showBarges = this.showBarges;
    temp.showFeeders = this.showFeeders;
    temp.showDeepsea = this.showDeepseas;
    temp.colDescs =  this.columns;
    temp.lookAhead = this.preferedLookAhead;
    temp.lookBack = this.preferedLookBack;
    temp.showWorkingVessels = this.showWorking;
    this.globals.preferencesDTO.preferences = JSON.stringify(temp);
  }

  getSliderOptions() {
      return  {
        stepsArray: this.dateRange.map((date: Date) => {
          return { value: date.getTime() };
        }),
        translate: (value: number, label: LabelType): string => {            
          return moment(new Date(value)).format('DD/MM/YYYY HH:mm');
        },    
        noSwitching: true,
        disabled: (this.getActiveSearchItems().length > 0)
      };
  }

  createDateRange(): Date[] {
    // -120 hours to +7 days
    const dates: Date[] = [];
    var tmpCurrentDate = new Date();

    var tmpStartDate = new Date();
    tmpStartDate.setHours(tmpCurrentDate.getHours()- (5)*24);

    var tmpEndDate = new Date();
    tmpEndDate.setHours(tmpCurrentDate.getHours() + (7)*24);  

    for (var d = tmpStartDate; d < tmpEndDate; d.setMinutes(d.getMinutes() + 1)) {      
      dates.push(new Date(d));
    }
    var date1 = new Date();
    var date2 = new Date();

    this.maxDateValue = date1.setHours(tmpCurrentDate.getHours() + (Number(this.preferedLookAhead))*24 + 24);
    this.minDateValue = date2.setHours(tmpCurrentDate.getHours() - (Number(this.preferedLookBack))*24 - 24);
    
    return dates;
  }

  drop(event: CdkDragDrop<string[]>) {    
    moveItemInArray(this.terminalNotes, event.previousIndex, event.currentIndex);
  }

  ngOnClose() {
  }
  
  private getVesselBackgroundClass(inputVVDto: VesselVisitDTO) {
    return getVesselBackgroundClass(inputVVDto);
  }

  private getVesselNotesCount(vesselName: String) :  number {
    let currentVessel = this.vesselNotesCount.find(i => i.vesselName === vesselName);
    if (currentVessel !== null && currentVessel !== undefined && currentVessel.notesCount > 0) {     
      return currentVessel.notesCount;
    }
    return 0;
  }

  private filterVesselVisitDTOListByActive() {  
    this.vvListDTO = this.vvListDTO.filter(vvDto => {
      return vvDto.active === true;
    });
    this.vvListDTO_Original = this.vvListDTO_Original.filter(vvDto => {
      return vvDto.active === true;
    });
  }

  private filterVesselVisits(byDates: boolean, byName: boolean) {
    var searchListOriginal = [];
    var searchListTemp = [];
    searchListOriginal = this.vvListDTO_Original;
    // Filter always using checks...
    if(!this.showWorking) {
      searchListOriginal = searchListOriginal.filter(function( vvDto ) {
          return vvDto.phase !== "WORKING";
      });
    }
    if(!this.showBarges) {
      searchListOriginal = searchListOriginal.filter(function( vvDto ) {
          return vvDto.classification !== "BARGE";
      });
    }
    if(!this.showDeepseas) {
      searchListOriginal = searchListOriginal.filter(function( vvDto ) {
          return vvDto.classification !== "DEEPSEA";
      });
    }
    if(!this.showFeeders) {
      searchListOriginal = searchListOriginal.filter(function( vvDto ) {
          return vvDto.classification !== "FEEDER";
      });
    }    
    // Filter by dates (SLIDER)
    if(byDates) {
      searchListTemp = [];
      searchListOriginal.filter(vvDto => { 
        // All Working Vessel Visits are included...
        if(vvDto.phase === 'WORKING') {
          searchListTemp.push(vvDto);                
        } else {
          var arrivalDate = new Date();
          if(vvDto.hasOwnProperty('ata')){
            arrivalDate = vvDto.ata;        
          } else {
            arrivalDate = vvDto.eta;        
          }    
          if (new Date(arrivalDate).getTime() <= this.maxDateValue && new Date(arrivalDate).getTime() >= this.minDateValue) {
            searchListTemp.push(vvDto);        
          }    
        }            
      });
      // Assign results to original to combine both filters
      searchListOriginal = searchListTemp;            
    }   
    // Filter by name (INSTANT SEARCH) 
    if(byName) {
      searchListTemp = [];
      const splitedSearchO = this.typedKey.trim().toUpperCase().split(',', 5);
      let splitedSearch;
      if (splitedSearchO.length > 1) {
        if (splitedSearchO[splitedSearchO.length - 1].slice().trim() === '') {
          splitedSearch = splitedSearchO.filter(e => e.trim() !== '')[0];
        } else {
          splitedSearch = splitedSearchO[splitedSearchO.length - 1].slice();
        }
      } else {
        splitedSearch = splitedSearchO;
      }    
      searchListOriginal.filter(vvDto => {
        if (this.isIncluded(splitedSearch, vvDto.vesselName) ||
          this.isIncluded(splitedSearch, vvDto.vesselVisitName) ||
          this.isIncluded(splitedSearch, vvDto.phase) ||
          this.isIncluded(splitedSearch, vvDto.classification) ||
          this.isIncluded(splitedSearch, vvDto.facility) ||
          this.isIncluded(splitedSearch, vvDto.service)) {
          if (!searchListTemp.includes(vvDto)) {
            searchListTemp.push(vvDto);
          }
        }
      });    
    }
    this.vvListDTO = searchListTemp;
    // Filter by Active
    this.filterVesselVisitDTOListByActive();
    // Sort Vessel Visits
    this.sortVesselVisitsByETADESC(); 

  }

  private addToVesselVisitDTOLists(vvDto: VesselVisitDTO, archiveSearch: boolean) {    
    // Check if exists
    var elementExists = false;
    this.vvListDTO_Original.forEach(element => {
      if(element.vesselVisitName === vvDto.vesselVisitName) {
        elementExists = true;
        return;
      }
    });
    // Remove item
    if(elementExists) {
      this.vvListDTO = this.vvListDTO.filter(function( obj ) {
        return obj.vesselVisitName !== vvDto.vesselVisitName;
      });
      this.vvListDTO_Original = this.vvListDTO_Original.filter(function( obj ) {
        return obj.vesselVisitName !== vvDto.vesselVisitName;
      });      
    }    
    if(archiveSearch) {
      // Add item if meets criteria
      this.logger.debug('Checking ARCHIVE SEARCH criteria');
      var addItem = this.checkVisitMeetsFilters(vvDto);                 
      if(addItem) {
        this.logger.debug('Meets ARcHIVE SEARCH criteria - Adding vessel visit');
        this.vvListDTO.push(vvDto);
        this.vvListDTO_Original.push(vvDto);  
      }
    } else {
      // Add item anyway
      this.vvListDTO.push(vvDto);
      this.vvListDTO_Original.push(vvDto);
    }    
  }

  private updateToVesselVisitDTOLists(vvDtoList: any) {
    this.vvListDTO = vvDtoList;
    this.vvListDTO_Original = vvDtoList;
  }

  private isIncluded(inSearchItem, inProperty) {
    return inProperty && inProperty.toUpperCase().includes(inSearchItem);
  }

  private sortArrayAtaEtaAscending(arrayToOrder): Array<VesselVisitDTO> {
    var ataArray = [];
    var etaArray = [];
    ataArray = arrayToOrder.filter(function( obj ) {
      return obj.phase === "WORKING";
    });
    etaArray = arrayToOrder.filter(function( obj ) {
      return obj.phase !== "WORKING";
    });  
    ataArray.sort(function(a, b){
      if(a.vesselVisitName.toLowerCase() < b.vesselVisitName.toLowerCase()) { return 1; }
      if(a.vesselVisitName.toLowerCase() > b.vesselVisitName.toLowerCase()) { return -1; }
      return 0;
    })
    ataArray.sort((a, b) => new Date(a.ata).getTime() - new Date(b.ata).getTime());     
    etaArray.sort(function(a, b){
      if(a.vesselVisitName.toLowerCase() > b.vesselVisitName.toLowerCase()) { return 1; }
      if(a.vesselVisitName.toLowerCase() < b.vesselVisitName.toLowerCase()) { return -1; }
      return 0;
    })
          
    etaArray.sort((a, b) => new Date(a.eta).getTime() - new Date(b.eta).getTime()); 
    return ataArray.concat(etaArray);    
  }

  private sortVesselVisitsByETADESC(): void {
    this.vvListDTO = this.sortArrayAtaEtaAscending(this.vvListDTO);        
    this.vvListDTO_Original = this.sortArrayAtaEtaAscending(this.vvListDTO_Original);            
  }

  resetSlider() {
    this.vvListDTO = this.vvListDTO_Original;
    this.restartMinMaxDate();
    this.saveParamsInSession();    
  }

  sliderChange() {    
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    this.vvListDTO = this.vvListDTO_Original;
    this.filterVesselVisits(true,true);
    var today = new Date();
    var maxdateval = new Date(this.maxDateValue);
    var mindateval = new Date(this.minDateValue);
    
    var utctoday = Date.UTC(today.getFullYear(), today.getMonth(), today.getDate());
    var utcmax = Date.UTC(maxdateval.getFullYear(), maxdateval.getMonth(), maxdateval.getDate());
    var utcmin = Date.UTC(mindateval.getFullYear(), mindateval.getMonth(), mindateval.getDate());

    var diffAhead = Math.floor((utcmax - utctoday)/_MS_PER_DAY) - 1;
    var diffBack = Math.floor((utctoday - utcmin)/_MS_PER_DAY) - 1;
    console.log(diffAhead, diffBack);
    this.preferedLookAhead = "" + Math.trunc(diffAhead);
    this.preferedLookBack = "" + Math.trunc(diffBack);
    //console.log(this.preferedLookAhead, this.preferedLookBack);
    var temp = JSON.parse(this.globals.preferencesDTO.preferences);
    temp.lookAhead = this.preferedLookAhead;
    temp.lookBack = this.preferedLookBack;
    this.globals.preferencesDTO.preferences = JSON.stringify(temp);
    this.saveParamsInSession();
  }  

  checkChange() {
    this.vvListDTO = this.vvListDTO_Original;
    this.filterVesselVisits(true,true);
    this.saveParamsInSession();
  }


  navigateToDetailsPage(vvDTO){
    if(vvDTO.totalStagesCount > 0){
      let url = ['/details-management', vvDTO.vesselVisitName];
      this.router.navigate(url);
    } else {
      let url = [];
      this.router.navigate(url);
    }

  }

  checkColsChange() {
    this.saveParamsInSession();
  }
  
  restartMinMaxDate() {
    this.dateRange = this.createDateRange();
    
  }

  changeSearchValue() {    
    this.showAdvancedSearch = !this.showAdvancedSearch;
  }

  search($event) {
    this.typedKey = $event.target.value;   
    this.filterVesselVisits(true, true);
  }

  // ################################ Advanced Search Methods
  checkVisitMeetsFilters(vvDTO: VesselVisitDTO) : boolean {
    var returnValue = true;
    // VesselName
    if(this.searchTerms[0].value !== '' && vvDTO.vesselName && !(vvDTO.vesselName.toUpperCase().includes(this.searchTerms[0].value.toUpperCase()))) {
      return false;
    }
    //VesselVisitId
    if(this.searchTerms[8].value !== '' && vvDTO.vesselVisitName && !(vvDTO.vesselNameId.toUpperCase().includes(this.searchTerms[8].value.toUpperCase()))) {
      return false;
    }
    // Classification
    if(this.searchTerms[1].value !== '' && vvDTO.classification && this.searchTerms[1].value.toUpperCase() !== vvDTO.classification.toUpperCase()) {
      return false;
    }
    // Operator
    if(this.searchTerms[2].value !== '' && vvDTO.operator && !(vvDTO.operator.toUpperCase().includes(this.searchTerms[2].value.toUpperCase()))) {
      return false;
    }
    // Phase
    if(this.searchTerms[3].value !== '' && vvDTO.phase && this.searchTerms[3].value.toUpperCase() !== vvDTO.phase.toUpperCase()) {
      return false;
    }
    // Service
    if(this.searchTerms[4].value !== '' && vvDTO.service && !(vvDTO.service.toUpperCase().includes(this.searchTerms[4].value.toUpperCase()))) {
      return false;
    }
    // Facility
    if(this.searchTerms[5].value !== '' && vvDTO.facility && !(vvDTO.facility.toUpperCase().includes(this.searchTerms[5].value.toUpperCase()))) {
      return false;
    }
    // StartDate
    if(this.searchTerms[6].value !== '' && vvDTO.eta && vvDTO.eta < new Date(this.searchTerms[6].value)) {
      return false;
    }
    // EndDate
    if(this.searchTerms[7].value !== '' && vvDTO.eta && vvDTO.eta > new Date(this.searchTerms[7].value)) {
      return false;
    }

    return true;
  } 

  enterSearchClick(event) {
    if(event.keyCode === 13) { 
      this.showAdvancedSearch = false;     
      this.searchClick();
    }
  }

  visibilityColsClick() {
    this.showAdvancedSearch = false;
    this.showColumnsVPanel = !this.showColumnsVPanel;
  }

  hideAdvancedSearch() {    
    this.showAdvancedSearch = false;
  }

  cancelClick() {    
    this.changeSearchValue();
    this.showAdvancedSearch = false;
  }

  clearInstantSearch() {
    this.inpEndDate.nativeElement.value = '';
  }

  clearInputs() {
    this.inpVesselName.nativeElement.value = '';
    this.inpLine.nativeElement.value = '';
    this.inpClassification.nativeElement.value = '';
    this.inpPhase.nativeElement.value = '';
    this.inpService.nativeElement.value = '';
    this.inpFacility.nativeElement.value = '';
    this.inpStartDate.nativeElement.value = '';
    this.inpEndDate.nativeElement.value = '';
    this.inpVesselVisitName.nativeElement.value = '';
  }

  clearActiveSearchTerms() {
    for(var i = 0; i < this.searchTerms.length; i++) {
      this.searchTerms[i].value = '';
    }
    this.userSearchPersistance.updateSearchItems(this.searchTerms);
  }

  resetClick() {
    this.clearActiveSearchTerms();
    if(this.showAdvancedSearch) {
      this.clearInputs();    
      this.clearInstantSearch();  
    }    
    this.restartMinMaxDate();
    this.getAllVesselVisits();
    this.showBarges = true;
    this.showDeepseas = true;
    this.showFeeders = true;
    this.saveParamsInSession();
    this.showAdvancedSearch = false;    
  }    

  getActiveSearchItems() {
    this.searchTerms = this.userSearchPersistance.getSearchItems();
    return this.searchTerms.filter(item => item.value !== '');
  }

  removeSearchTerm(searchTerm: SearchItem) {
    searchTerm.value = '';    
    if(this.searchTerms.filter(v => v.value !== '').length === 0) {
      // Update to original list
      this.refreshSliderVesselVisits();
    } else {
      // Search archive
      this.executeArchiveSearch();
    }
  }

  updateSearchTerms() {
    this.searchTerms[0].value = this.inpVesselName.nativeElement.value.trim();    
    this.searchTerms[1].value = this.inpClassification.nativeElement.value.trim();    
    this.searchTerms[2].value = this.inpLine.nativeElement.value.trim();    
    this.searchTerms[3].value = this.inpPhase.nativeElement.value.trim().toUpperCase();    
    this.searchTerms[4].value = this.inpService.nativeElement.value.trim();    
    this.searchTerms[5].value = this.inpFacility.nativeElement.value.trim();    
    this.searchTerms[6].value = this.inpStartDate.nativeElement.value.trim();    
    this.searchTerms[7].value = this.inpEndDate.nativeElement.value.trim();  
    this.searchTerms[8].value = this.inpVesselVisitName.nativeElement.value.trim();
    this.userSearchPersistance.updateSearchItems(this.searchTerms);  
  }

  executeArchiveSearch() {
    this.spinner.show();     
    this.restFulAPI.getVesselVisitsbyFilter(this.searchTerms).subscribe(res => {
      if(res === undefined || res === null) {
        res = new Array<VesselVisitDTO>();
      }
      this.vvListDTO = res;
      this.vvListDTO_Original = res;
      this.sortVesselVisitsByETADESC();
      this.logger.debug('Filter response!');
      this.spinner.hide();
      this.showAdvancedSearch = false;
    },
      err => {
        this.logger.debug(`getVesselVisitsbyFilter| ${err}`);
        this.spinner.hide();
      });
  }

  searchClick() {    
    this.updateSearchTerms();  
    if(this.searchTerms.filter(v => v.value !== '').length === 0) {
      // Update to original list
      this.refreshSliderVesselVisits();
      this.showAdvancedSearch = false;      
    }      
    else {
      this.executeArchiveSearch();
      this.showAdvancedSearch=false;        
    }    
  }

  // ################################ Get Methods
  getAllVesselVisits() { 
    this.spinner.show();    
    this.logger.debug("Updating vessel visits from REST API...")   
    this.restFulAPI.getVesselVisits()
    .subscribe(res => {      
      this.updateToVesselVisitDTOLists(res);
      this.filterVesselVisits(true, true);
      if(this.getActiveSearchItems().length !== 0){
        this.executeArchiveSearch();
      }            
      this.spinner.hide();    
    },
      err => {
        this.logger.debug(`getVesselVisits| ${err}`);
        this.spinner.hide();
      });
    
  }

  getColorPhase(phase) {
    return getColorPhase(phase);    
  }

  getOperatorPicture(operator) {
    return getOperatorPicture(operator);    
  }

  // ################################ Terminal Notes Methods

  addTerminalNoteDTOList(tnDto: NoteDTO) {     
    // Check if exists
    var elementExists = false;
    this.terminalNotes.forEach(element => {
      if(element.noteId === tnDto.noteId) {
        elementExists = true;
        return;
      }
    });
    // Remove item
    if(elementExists) {
      this.terminalNotes = this.terminalNotes.filter(function( obj ) {
        return obj.noteId !== tnDto.noteId;
      });  
    }    
    // Add item, only if active = true
    if(tnDto.active) {
      this.terminalNotes.push(tnDto);    
    }    
  }  

  getTerminalNotes() { 
    this.terminalNotes = [];
    this.restFulAPI.getTerminalNotes().subscribe(res => { 
      this.terminalNotes = res;
      this.sortTerminalNotes(this.terminalNotes);       
    });
  }

  sortTerminalNotes(notes: NoteDTO[]) {
    notes.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());    
  }

  addTerminalNote() {
    // Check if already exists an empty row
    var allowAdd = true;
    this.terminalNotes.forEach(function(element) {      
      if(element.noteId === "") {
        allowAdd = false;
      }
    });
    // Allow or not the edition
    if(allowAdd) {
      let terminalNote = new NoteDTO();
      terminalNote.noteId = "";      
      terminalNote.textMessage = "";
      terminalNote.date = new Date();
      terminalNote.vesselVisitName = "";
      terminalNote.username = getCurrentUser();
      terminalNote.active = true;    
      this.terminalNotes.push(terminalNote);
      this.sortTerminalNotes(this.terminalNotes);
      this.enterEditMode(terminalNote);
      // Move scroll to the top
      var myDiv = document.getElementById('tnList');    
      myDiv.scrollTop = 0;      
    }
  }

  removeNote(){
    this.showConfirm='none';
    let terminalNote = this.confirmElement;
    if(terminalNote.noteId === "") {                   
      this.removeTerminalNoteArray(terminalNote.noteId);  
      this.sortTerminalNotes(this.terminalNotes);      
      this.enterReadMode(terminalNote, false);     
    } else {
      terminalNote.active = false;
      this.restFulAPI.deleteTerminalNote(terminalNote).subscribe(data => {       
        this.removeTerminalNoteArray(terminalNote.noteId);          
        this.sortTerminalNotes(this.terminalNotes);                 
      }); 
    }  
  }

  removeTerminalNote(terminalNote: NoteDTO) {   
    this.confirm("You will delete this note for everyone.",terminalNote, this.removeNote);      
  }
  
  confirm(message: string, element: NoteDTO, callback){
    this.showConfirm = 'block';
    this.confirmMessage = message;
    this.confirmCallback = callback;
    this.confirmElement = element;
  }

  editTerminalNote(terminalNote: NoteDTO) {
    this.enterEditMode(terminalNote);
  }

  cancelTerminalNote(terminalNote: NoteDTO) {
    this.enterReadMode(terminalNote, false);
  }

  removeTerminalNoteArray(terminalNoteId: String) { 
    this.terminalNotes = this.terminalNotes.filter(function( obj ) {
      return obj.noteId !== terminalNoteId;
    }); 
    //this.terminalNotes.splice(this.terminalNotes.findIndex(item => item.noteId === terminalNoteId), 1)
  }

  saveTerminalNote(terminalNote: NoteDTO) {           
    if(terminalNote.noteId === "") {        
      this.getTerminalNoteData(terminalNote.noteId, terminalNote); 
      terminalNote.noteId = null;        
      this.saveNewTerminalNote(terminalNote);
    } else {      
      this.getTerminalNoteData(terminalNote.noteId, terminalNote); 
      this.modifyExistingTerminalNote(terminalNote);
    }    
  }

  getTerminalNoteData(currentId: String, newTerminalNote: NoteDTO) {    
    newTerminalNote.date = new Date();
    newTerminalNote.textMessage = (document.getElementById("tnInput " + currentId) as HTMLInputElement).value;
    newTerminalNote.username = getCurrentUser();
    newTerminalNote.active = true;
    newTerminalNote.vesselVisitName = "";    
  }

  saveNewTerminalNote(terminalNote: NoteDTO) {
    this.restFulAPI.addTerminalNote(terminalNote).subscribe(data => {     
      this.sortTerminalNotes(this.terminalNotes);
      terminalNote.noteId = "";
      this.removeTerminalNoteArray(terminalNote.noteId);  
      this.enableAllButtons();        
    });    
  }
  
  modifyExistingTerminalNote(terminalNote: NoteDTO) {
    this.restFulAPI.modifyTerminalNote(terminalNote).subscribe(data => {             
      this.sortTerminalNotes(this.terminalNotes);
      this.enterReadMode(terminalNote, false);           
    });     
  }

  enterReadMode(terminalNote: NoteDTO, newTerminalNote: boolean) {
    this.enableAllButtons();
    var noteId: String = "";
    if(!newTerminalNote) {
      noteId = terminalNote.noteId;               
    } else {
      noteId = "";
    }    
    document.getElementById("tnEditButtons " + noteId).style.display = "none";
    document.getElementById("tnReadButtons " + noteId).style.display = "block";
    document.getElementById("tnCommentsInput " + noteId).style.display = "none";
    document.getElementById("tnCommentsValue " + noteId).style.display = "block"; 
  }

  enterEditMode(terminalNote: NoteDTO) {
    this.disableAllButtons();
    if(document.getElementById("tnEditButtons " + terminalNote.noteId)!==null) {
      document.getElementById("tnEditButtons " + terminalNote.noteId).style.display = "block";
    }
    if(document.getElementById("tnReadButtons " + terminalNote.noteId)!==null) {
      document.getElementById("tnReadButtons " + terminalNote.noteId).style.display = "none";
    }
    if(document.getElementById("tnCommentsInput " + terminalNote.noteId)!==null) {
      document.getElementById("tnCommentsInput " + terminalNote.noteId).style.display = "block";
    }
    if(document.getElementById("tnCommentsValue " + terminalNote.noteId)!==null) {
      document.getElementById("tnCommentsValue " + terminalNote.noteId).style.display = "none";    
    }
    // Show editing buttons
    if(document.getElementById("saveTerminalNote " + terminalNote.noteId)!==null) {
      (document.getElementById("saveTerminalNote " + terminalNote.noteId) as HTMLButtonElement).disabled = false;
    }
    if(document.getElementById("cancelTerminalNote " + terminalNote.noteId)!==null) {
      (document.getElementById("cancelTerminalNote " + terminalNote.noteId) as HTMLButtonElement).disabled = false;
    }
  }

  disableAllButtons() {
    disableAllButtons();
  }

  enableAllButtons() {
    enableAllButtons();
  }

  // Completion moves chart
  calculateTitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateTitleMoves(vesselVisitDTO);
  }

  calculateSubtitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateSubtitleMoves(vesselVisitDTO);
  }

  calculatePercentMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculatePercentMoves(vesselVisitDTO);
  }

  // Load moves chart
  calculateLoadTitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateLoadTitleMoves(vesselVisitDTO);
  }

  calculateLoadSubtitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateLoadSubtitleMoves(vesselVisitDTO);
  }

  calculateLoadPercentMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateLoadPercentMoves(vesselVisitDTO);
  }

  // Discharge Moves chart
  calculateDschTitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateDschTitleMoves(vesselVisitDTO);
  }

  calculateDschSubtitleMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateDschSubtitleMoves(vesselVisitDTO);
  }

  calculateDschPercentMoves(vesselVisitDTO: VesselVisitDTO) {
    return calculateDschPercentMoves(vesselVisitDTO);
  }

  reorderColumns(event: CdkDragDrop<ColumnDescDTO>){
    var colsVisible: Array<ColumnDescDTO> = [];
    this.columns.forEach(ele => {
      if(ele.columnDescShow===true){
        colsVisible.push(ele);
      }
    });
    var previousPos = 0;
    var currentPos = 0;
    for(var i =0;i<this.columns.length;i++){
      if(this.columns[i].columnDescName === colsVisible[event.previousIndex].columnDescName){
        previousPos = i;
      }
      if(this.columns[i].columnDescName === colsVisible[event.currentIndex].columnDescName){
        currentPos=i;
      }
    }
    var order1 = this.columns[previousPos ].columnDescOrder;
    var order2 = this.columns[currentPos].columnDescOrder;
    this.columns[currentPos]["columnDescOrder"] = order1;
    this.columns[previousPos ]["columnDescOrder"] = order2;
    this.columns.sort(this.getSortOrder("columnDescOrder"));   
    var temp = JSON.parse(this.globals.preferencesDTO.preferences);
    temp.colDescs = this.columns;
    this.globals.preferencesDTO.preferences = JSON.stringify(temp);
  }
}
