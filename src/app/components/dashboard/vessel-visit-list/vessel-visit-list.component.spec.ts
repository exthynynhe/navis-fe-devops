import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VesselVisitListComponent } from './vessel-visit-list.component';

describe('VesselVisitListComponent', () => {
  let component: VesselVisitListComponent;
  let fixture: ComponentFixture<VesselVisitListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VesselVisitListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VesselVisitListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
