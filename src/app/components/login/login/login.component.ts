import { AuthenticationService } from '../../../shared/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WebSocketService } from '../../../shared/services/websocket.service';
import { LOGIN_TYPE } from '../../../global/utils';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'e2e-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public n4LoginMode: boolean = false;
  public model: any = {};
  public loading = false;
  public error = '';
  public browserAlert = false;
  public loginAlert = false;
  public connectionAlert = false;
  public n4Token: string = '';

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private webSocketService: WebSocketService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService) { 
    }

  ngOnInit() {
    this.authenticationService.logout();
    if(this.route.snapshot.queryParams['token']) {
        this.n4LoginMode = true;
        this.spinner.show();                    
        this.n4Token = this.route.snapshot.queryParams['token'];
        this.n4login();
    } else {
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        if(!is_chrome){
            this.browserAlert = true;            
        }
    }        
  }

  public n4login() {
    localStorage.clear();
    this.loading = true;
    this.authenticationService.n4login(this.n4Token)
        .subscribe(result => {
            if (result === LOGIN_TYPE.LOGGED) {                    
                // login successful
                localStorage.setItem('disconnectionAlerted', "false"); 
                localStorage.setItem('webSocketConnected', "false");
                this.webSocketService.connect();                    
                this.router.navigate(['dashboard-management']);                    
            } else if (result === LOGIN_TYPE.E_CONNECTION) {
                // this.error = 'Cannot reach Compass backend';
                this.connectionAlert = true;
                this.loading = false;                    
            }                
        }, error => {
            // TODO - Change message, receive different errors from backend
            this.error = 'Error accesing Compass Backend';
            this.loading = false;
            this.error = error;
    });
  }

  public login() {
    localStorage.clear();
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
        .subscribe(result => {
            if (result === LOGIN_TYPE.LOGGED) {                    
                // login successful
                localStorage.setItem('disconnectionAlerted', "false"); 
                localStorage.setItem('webSocketConnected', "false");
                this.webSocketService.connect();                    
                this.router.navigate(['dashboard-management']);                    
            } else if (result === LOGIN_TYPE.E_CREDENTIALS) {
                // login failed                                        
                // this.error = 'Username or password is incorrect';
                this.loginAlert = true;
                this.connectionAlert = false;
                this.browserAlert = false;
                this.loading = false;                    
            } else if (result === LOGIN_TYPE.E_CONNECTION) {
                // this.error = 'Cannot reach Compass backend';
                this.connectionAlert = true;
                this.loginAlert = false;
                this.browserAlert = false;
                this.loading = false;                    
            }                
        }, error => {
            // TODO - Change message, receive different errors from backend
            this.error = 'Username or password is incorrect';
            this.loading = false;
            this.error = error;
    });        
  }
}
