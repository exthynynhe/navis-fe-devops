import { Component, OnInit } from '@angular/core';
import { StageDTO } from '../../../../model/StageDTO';
import { StageEXT } from '../../../../model/StageEXT';
import { RestFulAPIService } from '../../../../shared/services/restfulapi.service';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { cleanCharacters, phases, disableAllButtons, enableAllButtons, workflowStages } from '../../../../global/utils';
import { v4 as uuid } from 'uuid';
import { NgxSpinnerService } from 'ngx-spinner';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'e2e-workflow-steps',
  templateUrl: './workflow-steps.component.html',
  styleUrls: ['./workflow-steps.component.scss']
})
export class WorkflowStepsComponent implements OnInit {

  public originalSubstages: Array<StageEXT> = [];
  public newSubstages: Array<StageEXT> = [];
  public stages: Array<StageEXT> = [] ;
  public phases;
  public dataSource;
  public removable: boolean = true;
  public selectable: boolean = true;  
  public separatorKeysCodes = [ENTER, COMMA];    
  public showAlert = 'none';
  public alertMessage: string = "";
  public showConfirm = 'none';
  public confirmMessage = "";
  public confirmCallback;
  public confirmElement;

  constructor(private restFulAPI: RestFulAPIService, private spinner: NgxSpinnerService, private logger: NGXLogger) {
    this.phases = phases;    
  }

  ngOnInit() {  
    this.getAllStages();    
  }

  alert(message: string){
    this.showAlert = 'block';
    this.alertMessage = message;
  }

  sortSteps() {  
    this.stages.sort(function(a,b) {
      if(a.name === "new") {
        return -1;
      }
      if(a.name === null || a.name === undefined || a.name.trim() === "")
        return -1;       
      if (a.name.toLowerCase() < b.name.toLowerCase())
        return -1;
      if (a.name.toLowerCase() > b.name.toLowerCase())
        return 1;
      return 0;
    }); 
    var newStages: Array<StageEXT> = this.stages.filter(function( obj ) {
      return obj.stageTemplateId === "new";
    });  
    var preStages: Array<StageEXT> = this.stages.filter(function( obj ) {
      return obj.category === workflowStages[0].code;
    });  
    var dschStages: Array<StageEXT> = this.stages.filter(function( obj ) {
      return obj.category === workflowStages[1].code;
    });  
    var loadStages: Array<StageEXT> = this.stages.filter(function( obj ) {
      return obj.category === workflowStages[2].code;
    });  
    var dprtStages: Array<StageEXT> = this.stages.filter(function( obj ) {
      return obj.category === workflowStages[3].code;
    });  
    var ovrlStages: Array<StageEXT> = this.stages.filter(function( obj ) {
      return obj.category === workflowStages[4].code;
    });  
    this.stages = newStages.concat(preStages).concat(dschStages).concat(loadStages).concat(dprtStages).concat(ovrlStages);         
  }

  checkInputValue(input, minValue, maxValue) {
    if (input.value.length > input.maxLength) {
      input.value = input.value.slice(0, input.maxLength);
    }
    if (input.value > maxValue) { 
      input.value = maxValue;
    }
    if (input.value < minValue) { 
      input.value = 0;
    }
  } 

  addSubstep(event, element: StageDTO): void {    
    let input = event.input;
    let value = event.value;
    let subStage = new StageDTO();   
    if ((value || '').trim()) { 
      // Use UUID only temporary to identify chips before saving... 
      subStage.stageTemplateId = uuid().toString();                        
      subStage.name = value.trim();
      subStage.position = 0;
      subStage.category = element.category;     
      element.children.push(subStage); 
    }
    if (input) {
      input.value = '';
    }    
  }

  removeSubstep(substep, element): void {    
    let index = element.children.indexOf(substep);
    if (index >= 0) {
      element.children.splice(index, 1);
    }
  }

  getAllStages() {
    this.spinner.show();
    this.stages = [];
    this.restFulAPI.getStages().subscribe(res => {   
      res.forEach(element=> {
        var stage = new StageEXT(element, false);
        this.stages.push(stage);
      });
      this.sortSteps();
      this.spinner.hide();
    },
      err => {
        this.logger.debug(`getVesselVisits| ${err}`);
        this.enableAllButtons();
        this.spinner.hide();
      });
  }

  addNewRow() {
    // Check if already exists an empty row
    var allowAdd = true;
    this.stages.forEach(function(element) {            
      if(element.stageTemplateId === "new") {
        allowAdd = false;
      }      
    });
    // Allow or not the edition
    if(allowAdd) {
      let myStage = new StageEXT();
      myStage.name = "";
      myStage.stageTemplateId = "new";
      myStage.builtInS = false;
      myStage.children = [];      
      myStage.position = 0;
      myStage.editable = true;
      this.stages.push(myStage);     
      this.sortSteps(); 
      // Move scroll to the top
      var myDiv = document.getElementById('tblContent');    
      myDiv.scrollTop = 0; 
    }
  }

  removeStage(){
    this.showConfirm = 'none';
    this.spinner.show();
    let element = this.confirmElement;
      this.restFulAPI.deleteStage(element).subscribe(data => {
        this.stages = this.stages.filter(function( obj ) {
          return obj.stageTemplateId !== element.stageTemplateId;
        });
        this.sortSteps();
        this.spinner.hide();          
      }, 
      err => {
        this.logger.debug(`removeRow | ${err}`);
        this.enableAllButtons();
        this.spinner.hide();
      });  
  }
  removeRow(element: StageEXT) {
    if(element.builtInS) {
      this.alert('Cannot remove a predefined row');
      return;
    } 
    if(element.stageTemplateId === "new") {
      var index = -1;
      for(var i=0; i<= this.stages.length -1; i++)
      {
        if(this.stages[i].stageTemplateId === "new") {
          index = this.stages.indexOf(this.stages[i]);
          break;
        }
      }
      if(index !== -1) {
        this.stages.splice(index,1);
        this.sortSteps();   
      } 
      return;
    }  
    this.confirm("If you delete a workflow step, it will be deleted from all vessel visits. Do you want to delete this workflow step? ", element, this.removeStage);
  }
  
     
 confirm(message: string, element: StageEXT, callback){
  this.showConfirm = 'block';
  this.confirmMessage = message;
  this.confirmCallback = callback;
  this.confirmElement = element;
}

  createNewStep(currentStage) {
    // Create object locally
    let newStage = new StageEXT();
    newStage.editable = false;
    newStage.builtInS = false;
    newStage.name = cleanCharacters(document.getElementById("name " + currentStage.stageTemplateId).textContent).substring(0,20);   
    newStage.position = 0;    
    newStage.category = (<HTMLSelectElement>document.getElementById("select " + currentStage.stageTemplateId)).value.trim();    
    //newStage.role = document.getElementById("role " + currentStage.stageTemplateId).textContent; 
    newStage.stageType = '';   
    var substages = [];          
    // Get substages from element          
    for(var i = 0; i <= currentStage.children.length - 1; i++)
    {
      let subStage = new StageDTO();
      subStage.stageTemplateId = "";            
      subStage.name = currentStage.children[i].name.substring(0,20);           
      subStage.position = 0;
      subStage.category = newStage.category;
      subStage.builtInS = newStage.builtInS;
      //subStage.role = newStage.role;
      subStage.stageType = '';
      subStage.children = [];
      substages.push(subStage);             
    }                    
    newStage.children = substages;
    var saveStage = true;
    // Check name does not exist or order not repeated, fields not empty...
    for(i=0; i<= this.stages.length -1; i++)
    {
      if(this.stages[i].name.toUpperCase() === newStage.name.toUpperCase()) {        
        this.alert("A step with name " + newStage.name + " already exists.");
        saveStage = false;
        break;
      }
    }
    if(saveStage)
    {            
      // Remove the initial empty one
      this.stages = this.stages.filter(function( obj ) {
        return obj.stageTemplateId !== "new";
      });
      this.spinner.show();         
      // Add the new one, update datasource and cancel editing       
      console.log(newStage); 
      this.restFulAPI.addStage(newStage).subscribe(data => {        
          this.stages.push(new StageEXT(data, false));
          console.log(this.stages);
          this.sortSteps();  
          this.spinner.hide();   
          this.cancelEditRow(data, true);               
      },
      err => {
        this.logger.debug(`createNewStep | ${err}`);
        this.enableAllButtons();
        this.spinner.hide();
      }   
      );
    } 
  }

  modifyExistingStep(currentStage: StageEXT) {
    var modifyObject = true;
    // Check name does not exist or order not repeated, fields not empty...
    for(var i=0; i<= this.stages.length -1; i++)
    {      
      if(!currentStage.builtInS && this.stages[i].stageTemplateId !== currentStage.stageTemplateId && this.stages[i].name.trim().toUpperCase().match(document.getElementById("name " + currentStage.stageTemplateId).textContent.trim().toUpperCase())) {
        this.alert("A step with name " + this.stages[i].name + " already exists.")
        modifyObject = false;
        break;
      }      
    }
    if(modifyObject) {
      var modifiedStage = new StageEXT();
      modifiedStage.editable = false;
      for(i=0; i<= this.stages.length -1; i++)
      {
        if(this.stages[i].stageTemplateId === currentStage.stageTemplateId) {          
          modifiedStage.stageTemplateId = currentStage.stageTemplateId;
          modifiedStage.builtInS = currentStage.builtInS;
          modifiedStage.name = cleanCharacters(document.getElementById("name " + currentStage.stageTemplateId).textContent).substring(0,20);
          modifiedStage.position = 0;
          modifiedStage.category = (<HTMLSelectElement>document.getElementById("select " + currentStage.stageTemplateId)).value.trim();
          //modifiedStage.role = document.getElementById("role " + currentStage.stageTemplateId).textContent.trim();
          modifiedStage.stageType = currentStage.stageType;
          modifiedStage.children = []
          var substages = [];          
          // Get substages from screen element 
          if(currentStage.children && currentStage.children !== undefined) {
            for(var x = 0; x <= currentStage.children.length - 1; x++)
            {              
              let subStage = new StageDTO();              
              // If it is not an UUID ID assigned temporary to the chips... get same ID from screen...
              if(currentStage.children[x].stageTemplateId !== undefined && currentStage.children[x].stageTemplateId.toString().length < 30) {
                subStage.stageTemplateId = currentStage.children[x].stageTemplateId;            
              }
              // If it is an UUID ID assigned temporary to the chip, put blank ID
              if(currentStage.children[x].stageTemplateId !== undefined && currentStage.children[x].stageTemplateId.toString().length > 30) {
                subStage.stageTemplateId = "";
              }             
              subStage.builtInS = currentStage.builtInS;
              if (document.getElementById("chip " + currentStage.stageTemplateId + " " + currentStage.children[x].stageTemplateId) !== undefined) {
                subStage.name = document.getElementById("chip " + currentStage.stageTemplateId + " " + currentStage.children[x].stageTemplateId).textContent.trim().substring(0,20);
              } else {
                subStage.name = currentStage.children[x].name.substring(0,20);
              }           
              subStage.position = 0;
              subStage.category = modifiedStage.category;
              subStage.builtInS = modifiedStage.builtInS;
              //subStage.role = modifiedStage.role;
              subStage.stageType = modifiedStage.stageType;
              subStage.children = [];
              substages.push(subStage);
            }                    
          }                
          modifiedStage.children = substages;           
          this.spinner.show();          
          // Modify the stage          
          this.restFulAPI.modifyStage(modifiedStage).subscribe(data => {            
            this.stages[i] = new StageEXT(data, false);
            console.log(this.stages)
            this.sortSteps();     
            this.spinner.hide();            
            this.cancelEditRow(data, false);              
          },
          err => {
            this.logger.debug(`modifyExistingStep | ${err}`);
            this.enableAllButtons();
            this.spinner.hide();
          });      
          break;          
        }
      }
    }
  }

  saveRow(currentStage) {        
    // Check name is not empty
    if(document.getElementById("name " + currentStage.stageTemplateId).textContent.trim() === "") {
      this.alert("Stage must contain a name");
      return;
    }
    // Check phase is not empty
    if((<HTMLSelectElement>document.getElementById("select " + currentStage.stageTemplateId)).value.trim() === "") {
      this.alert("Stage must contain the phase");
      return;
    }
    // Check substeps different names
    var childrenNames = [];
    if(currentStage.children !== undefined && currentStage.children !== null) {     
      // Get substages from element          
      for(var i = 0; i <= currentStage.children.length - 1; i++)
      {      
        var stageName;
        if (document.getElementById("chip " + currentStage.stageTemplateId + " " + currentStage.children[i].stageTemplateId) !== undefined) {
          stageName = document.getElementById("chip " + currentStage.stageTemplateId + " " + currentStage.children[i].stageTemplateId).textContent.trim();
        } else {
          stageName = currentStage.children[i].name;
        } 
        childrenNames.push(stageName);       
      }  
      var duplicates = false;
      var alreadySeen = [];
      childrenNames.forEach(function(str) {
        if (alreadySeen[str])
          duplicates = true;
        else
          alreadySeen[str] = true;
      }); 
      if(duplicates) {
        this.alert("Substages names must be unique");
        return;
      } 
    }        
    // New record
    if(currentStage.stageTemplateId === "new") {
      this.createNewStep(currentStage)
    } else {
      this.modifyExistingStep(currentStage)
    }   
  }

  cancelRow(element) {
    if(element.stageTemplateId === "new") {
      let index = this.stages.indexOf(element);
      if (index >= 0) {
        this.stages.splice(index, 1);
        this.enableAllButtons();
      }
    } else {;
      this.cancelEditRow(element, false);
      this.getAllStages();      
    }
  }

  cancelEditRow(element, newStage) {    
    this.enableAllButtons();
    if(!this.newSubstages) {
      var currentStageWFId = element.stageTemplateId;
      document.getElementById("main buttons " + currentStageWFId).style.display = "block";
      document.getElementById("edit buttons " + currentStageWFId).style.display = "none";
      document.getElementById("phase value " + currentStageWFId).style.display = "block";
      document.getElementById("phase select " + currentStageWFId).style.display = "none";
      // Disable editing row
      document.getElementById("name " + currentStageWFId).contentEditable = 'false';        
      // document.getElementById("role " + currentStageWFId).contentEditable = 'false';        
      // Disable editing chips and close buttons    
      if(!element.builtInS) {  
        // document.getElementById("chiplist " + element.stageTemplateId).contentEditable = 'false';          
        document.getElementById("substep " + currentStageWFId).className = '';     
        document.getElementById("input " + currentStageWFId).style.display = "none";
        element.children.forEach(substage => {
          document.getElementById("closebutton " + currentStageWFId + " " + substage.stageTemplateId).style.display = "none";
        });
        // Retrieve original substages array and disable again the chips
        element.children = Object.assign([], this.originalSubstages);
        element.editable = false;
      }    
    }         
  }

  editRow(element: StageEXT) {    
    // Disable rest of buttons
    this.disableAllButtons();
    document.getElementById("main buttons " + element.stageTemplateId).style.display = "none";
    document.getElementById("edit buttons " + element.stageTemplateId).style.display = "block";
    if(!element.builtInS) {
      document.getElementById("phase value " + element.stageTemplateId).style.display = "none";
      document.getElementById("phase select " + element.stageTemplateId).style.display = "block";
    }
    // Show editing buttons
    (document.getElementById("save " + element.stageTemplateId) as HTMLButtonElement).disabled = false;
    (document.getElementById("cancel " + element.stageTemplateId) as HTMLButtonElement).disabled = false;
    // Allow editing row
    if(!element.builtInS) {
      document.getElementById("name " + element.stageTemplateId).contentEditable = 'true';
      // document.getElementById("chiplist " + element.stageTemplateId).contentEditable = 'true';
    }   
    // document.getElementById("role " + element.stageTemplateId).contentEditable = 'true';
    if(!element.builtInS) {      
      document.getElementById("name " + element.stageTemplateId).className = 'cellContentEditable'; 
      document.getElementById("phase " + element.stageTemplateId).className = 'cellContentEditable';
      document.getElementById("substep " + element.stageTemplateId).className = 'cellContentEditable';      
      document.getElementById("input " + element.stageTemplateId).style.display = "block";                       
      element.editable = true;
    }
    // Keep original substages status
    this.originalSubstages = Object.assign([], element.children);
    this.newSubstages = Object.assign([], element.children);
  }

  disableAllButtons() {
    disableAllButtons();    
  }

  enableAllButtons() {
    enableAllButtons();    
  }

  getName(stageEXT: StageEXT): string {
    let returnValue: string = "";
    stageEXT.children.forEach((item, index) => {      
      returnValue += item.name;
      if (index != stageEXT.children.length -1) {
        returnValue += ", "
      }
    });
    return returnValue;
  }
}
