import { Component, OnInit, ViewChild } from '@angular/core';
import { StageEXT } from '../../../../model/StageEXT';
import { RestFulAPIService } from '../../../../shared/services/restfulapi.service';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { NgxSpinnerService } from 'ngx-spinner';
import { WorkflowConfigurationComponent } from '../workflow-configuration/workflow-configuration.component';
import { WorkflowListComponent } from '../workflow-list/workflow-list.component';
import { WorkflowStepsComponent } from '../workflow-steps/workflow-steps.component';

@Component({
  selector: 'e2e-index-workflow-settings',
  templateUrl: './index-workflow-settings.component.html',
  styleUrls: ['./index-workflow-settings.component.css']
})

export class IndexWorkflowSettingsComponent implements OnInit {

  @ViewChild(WorkflowConfigurationComponent) workflowConfigurationComponent: WorkflowConfigurationComponent;
  @ViewChild(WorkflowListComponent) workflowListComponent: WorkflowListComponent;
  @ViewChild(WorkflowStepsComponent) workflowStepsComponent: WorkflowStepsComponent;

  public workflowDisplay: Array<Object> = [];
  public originalSubstages: Array<StageEXT> = [];
  public newSubstages: Array<StageEXT> = [];
  public stages: Array<StageEXT> = [] ;
  public phases;
  public dataSource;
  public removable: boolean = true;
  public selectable: boolean = true;  
  public separatorKeysCodes = [ENTER, COMMA];    
  public displayLabel: String = "Workflow Settings";

  constructor(private restFulAPI: RestFulAPIService, private spinner: NgxSpinnerService) {       
  }

  ngOnInit() {      
    let element: HTMLElement = document.getElementById('workflow-settings-workflow-list-tab-btn') as HTMLElement;
    element.click();      
  }

  openTab(tabId: string) {    
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");  
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");    
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabId).style.display = "block";
    document.getElementById(tabId + '-btn').className += " active";
    // Refresh content of the tab
    switch(tabId) {
      case "workflow-settings-workflow-list-tab":
        this.workflowListComponent.ngOnInit();
        break;
      case "workflow-settings-workflow-stages-library-tab":
        this.workflowStepsComponent.ngOnInit();
        break;
      case "workflow-settings-workflow-configuration-tab": 
        this.workflowConfigurationComponent.ngOnInit();        
        break;
    }
  } 
}
