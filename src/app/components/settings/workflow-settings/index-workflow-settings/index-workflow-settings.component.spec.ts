import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexWorkflowSettingsComponent } from './index-workflow-settings.component';

describe('IndexWorkflowSettingsComponent', () => {
  let component: IndexWorkflowSettingsComponent;
  let fixture: ComponentFixture<IndexWorkflowSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexWorkflowSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexWorkflowSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
