import { Component, OnInit, SimpleChanges } from '@angular/core';
import { WorkflowTmpDTO } from '../../../../model/WorkflowTmpDTO'
import { RestFulAPIService } from 'src/app/shared/services/restfulapi.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { StageDTO } from 'src/app/model/StageDTO';
import { disableAllButtons, enableAllButtons, weekDays } from '../../../../global/utils';
import { FacilityDTO } from 'src/app/model/FacilityDTO';
import { CarrierServiceDTO } from 'src/app/model/CarrierServiceDTO';
import { LineOperatorDTO } from 'src/app/model/LineOperatorDTO';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'e2e-workflow-list',
  templateUrl: './workflow-list.component.html',
  styleUrls: ['./workflow-list.component.scss']
})

export class WorkflowListComponent implements OnInit {

  workflowList: Array<WorkflowTmpDTO>;
  selectedWorkflow: WorkflowTmpDTO;
  operatorOptions: Array<LineOperatorDTO> = [];
  facilityOptions: Array<FacilityDTO> = [];
  carrierOptions: Array<CarrierServiceDTO> = [];
  facilitySelectedList = [];
  carrierSelectedList = [];
  operatorSelectedList = [];
  id: string;
  name: string;  
  facSelected: number;
  carrierSelected: number;
  operatorSelected: number;
  workflowDisplay: Array<Object> = [];
  workflowName: String;
  active: Boolean;
  activeWorkflow: Boolean;
  editMode: boolean = false;
  backColor: boolean = false;  
  conflictedWorkflow = new Map();
  public showAlert = 'none';
  public alertMessage: string = "";
  public showConfirm = 'none';
  public confirmMessage = "";
  public confirmCallback;
  public confirmElement;

  constructor(private restfulApi: RestFulAPIService, private spinner: NgxSpinnerService, private logger: NGXLogger) {    
  }  
 
  ngOnChanges(changes: SimpleChanges) {
    changes.selectedWorkflow.currentValue.forEach((wf) => {
      if(wf.id === parseInt(this.id)) {
        if(wf.selected === true) {
          this.backColor = true;
        }
        else {
          this.backColor = false;
        }
      }
    })
  }

  changeSelected(id) {
      const rr = this.workflowDisplay.forEach(element => {
        if(element['id'] === id) {
          element['selected'] = true;
        }
        else {
          element['selected'] = false;
        }
      });
  }

  ngOnInit() {
    this.conflictedWorkflow.clear();
    this.loadData();
    console.log(this.workflowList);
  } 

  alert(message: string){
    this.showAlert = 'block';
    this.alertMessage = message;
  }

  loadData() {
    this.getWorkflows();
    this.getCarriers();
    this.getFacilities();
    this.getOperators();
  }

  sortWorkflows() {
    this.workflowList.sort(function(a,b) {
      if(a.workflowTmpId === -1) {
        return -1;
      }
      if(a.workflowTmpName === null || a.workflowTmpName === undefined || a.workflowTmpName.trim() === "")
        return -1;       
      if (a.workflowTmpName.toLowerCase() < b.workflowTmpName.toLowerCase())
        return -1;
      if (a.workflowTmpName.toLowerCase() > b.workflowTmpName.toLowerCase())
        return 1;
      return 0;
    });            
  }

  getWorkflows() {
    this.spinner.show();
    this.restfulApi.getWorkflowTemplates().subscribe(res => {
        this.workflowList = res;
        if(this.workflowList !== undefined && this.workflowList !== null && this.workflowList.length > 0){
          this.workflowList[0].selected = true;
          this.selectedWorkflow = this.workflowList[0];
        }   
        this.sortWorkflows();     
        this.spinner.hide();
    },
    err =>{
      this.logger.debug(`getWorkflowTemplates| ${err}`);
      this.spinner.hide();
    });   
  }

  getFacilities() {
    this.spinner.show();
    this.restfulApi.getFacilities().subscribe(res => {
      this.facilityOptions = res;
      this.spinner.hide();
    }),
    err => {
      this.logger.debug(`getFacilities | ${err}`);
      this.spinner.hide();
    };
  }

  getCarriers() {
    this.spinner.show();
    this.restfulApi.getCarrierServices().subscribe(res => {
      this.carrierOptions = res;
      this.spinner.hide();
    }),
    err => {
      this.logger.debug(`getFacilities | ${err}`);
      this.spinner.hide();
    };
  }

  getOperators() {
    this.spinner.show();
    this.restfulApi.getLineOperators().subscribe(res => {
      this.operatorOptions = res;
      this.spinner.hide();
    }),
    err => {
      this.logger.debug(`getFacilities | ${err}`);
      this.spinner.hide();
    };
  }

  addNewRow(){
    // Check if already exists an empty row
    var allowAdd = true;
    this.workflowList.forEach(function(element) {      
      if(element.workflowTmpId  === -1) {
        allowAdd = false;
      }
    });
    // Allow or not the edition
    if(allowAdd) {
      var newWorkflow: WorkflowTmpDTO = {
        workflowTmpId:-1,
        workflowTmpName: "",
        active: true,
        facilities:[], 
        carrierServices:[],
        lineOperators:[],
        stageTemplates:[],
        selected: true
      }
      this.workflowList.push(newWorkflow);
      this.sortWorkflows();      
      // Move scroll to the top
      var myDiv = document.getElementById('tblContent'); 
      myDiv.scrollTop = 0; 
      this.changeSelected(newWorkflow.workflowTmpId);
    }
  }  

  selectWorkflowRow(id: Number) {
    this.workflowList.forEach(element => {
      element.selected = false;
    });    
    this.selectedWorkflow = this.workflowList.find(workflow => workflow.workflowTmpId === id);               
    this.selectedWorkflow.selected = true;  
  }

  enterEditMode(element: WorkflowTmpDTO){
    disableAllButtons();
    // Enable editing buttons
    (document.getElementById("saveButton " + element.workflowTmpId) as HTMLButtonElement).disabled = false;
    (document.getElementById("cancelButton " + element.workflowTmpId) as HTMLButtonElement).disabled = false;
    document.getElementById("workflowName " + element.workflowTmpId).style.display = "none";
    document.getElementById("workflowName " + element.workflowTmpId + " edit").style.display = "block";
    document.getElementById("workflowActive " + element.workflowTmpId).style.display = "none";
    document.getElementById("workflowActive " + element.workflowTmpId + " edit").style.display = "block";
    document.getElementById("workflowFacilities " + element.workflowTmpId).style.display = "none";
    document.getElementById("workflowFacilities " + element.workflowTmpId + " edit").style.display = "block";
    document.getElementById("workflowCarriers " + element.workflowTmpId).style.display = "none";
    document.getElementById("workflowCarriers " + element.workflowTmpId + " edit").style.display = "block";
    document.getElementById("workflowOperators " + element.workflowTmpId).style.display = "none";
    document.getElementById("workflowOperators " + element.workflowTmpId + " edit").style.display = "block";
    document.getElementById("workflowButtons " + element.workflowTmpId).style.display = "none";
    document.getElementById("workflowButtons " + element.workflowTmpId + " edit").style.display = "block";
  }

  exitEditMode(workflowTmpId: number){
    document.getElementById("workflowName " + workflowTmpId).style.display = "block";
    document.getElementById("workflowName " + workflowTmpId + " edit").style.display = "none";
    document.getElementById("workflowActive " + workflowTmpId).style.display = "block";
    document.getElementById("workflowActive " + workflowTmpId + " edit").style.display = "none";
    document.getElementById("workflowFacilities " + workflowTmpId).style.display = "block";
    document.getElementById("workflowFacilities " + workflowTmpId + " edit").style.display = "none";
    document.getElementById("workflowCarriers " + workflowTmpId).style.display = "block";
    document.getElementById("workflowCarriers " + workflowTmpId + " edit").style.display = "none";
    document.getElementById("workflowOperators " + workflowTmpId).style.display = "block";
    document.getElementById("workflowOperators " + workflowTmpId + " edit").style.display = "none";
    document.getElementById("workflowButtons " + workflowTmpId).style.display = "block";
    document.getElementById("workflowButtons " + workflowTmpId + " edit").style.display = "none";
    enableAllButtons();
  }

  editWorkflow(element: WorkflowTmpDTO) {
    this.editMode = true;
    // Move from object to NgModel
    this.workflowName = element.workflowTmpName;
    this.activeWorkflow = element.active;
    this.carrierSelectedList = element.carrierServices;
    this.operatorSelectedList = element.lineOperators;
    this.facilitySelectedList = element.facilities;
    this.enterEditMode(element);    
  }

  cancelEditWorkflow(workflowTmpId: number) {
    this.conflictedWorkflow.clear();
    if(workflowTmpId === -1) {
      var newWorkflow: WorkflowTmpDTO = null;
      for(var i = 0; i < this.workflowList.length; i++) {
        if(this.workflowList[i].workflowTmpId == -1) {
          newWorkflow = this.workflowList[i];
        }
      }
      if(newWorkflow !== null) {
        let index = this.workflowList.indexOf(newWorkflow);
        if (index >= 0) {
          this.workflowList.splice(index, 1);       
        }
      }
      this.sortWorkflows();        
    } 
    this.editMode = false;
    this.exitEditMode(workflowTmpId);    
  }

  saveRow(element: WorkflowTmpDTO) {
    // New record
    this.workflowName = this.workflowName.trim();
    if(this.workflowName.trim() === '') {
      this.alert("A workflow template must have a name");
      return;
    }
    if(element.workflowTmpId === -1) {
      if(this.validateRowSubset() || this.validateRowSuperset()) {          
        setTimeout(() => {
          this.alert('Workflow Already exists!')
        }, 100)
      }
      else {
        this.createNewWorkflow(element);
      }            
    } else {
      if(this.validateRowSubset() || this.validateRowSuperset()) {
        setTimeout(() => {
          this.alert('Workflow Already exists!')
        }, 100)
      } 
      else {       
        this.modifyExistingWorkflow(element);      
      }      
    } 
  }

  createNewWorkflow(element: WorkflowTmpDTO) {
    // Create workflow locally
    var newWorkflow: WorkflowTmpDTO = {
      workflowTmpId: null, 
      workflowTmpName: this.workflowName,
      active: this.activeWorkflow,
      facilities: this.facilitySelectedList,
      carrierServices: this.carrierSelectedList,
      lineOperators: this.operatorSelectedList,
      stageTemplates: [],
      selected: null
    }          
    // Remove the initial empty one
    this.workflowList = this.workflowList.filter(function( obj ) {
      return obj.workflowTmpId !== -1;
    });
    this.spinner.show();
    // Add the new one, update datasource and cancel editing        
    this.restfulApi.addWorkflowTemplate(newWorkflow).subscribe(data => {        
        this.workflowList.push(new WorkflowTmpDTO(data));
        this.sortWorkflows();
        this.editMode = false;
        enableAllButtons();
        this.spinner.hide();         
      },
      err => {
        this.logger.debug(`createNewWorkflow | ${err}`);        
        this.spinner.hide();
      }      
    );          
  }

  modifyExistingWorkflow(element: WorkflowTmpDTO) {
    for(var i=0; i<= this.workflowList.length -1; i++)
    {
      if(this.workflowList[i].workflowTmpId === element.workflowTmpId) {   
        // Get values
        var modifiedWorkflow: WorkflowTmpDTO = {
          workflowTmpId: element.workflowTmpId, 
          workflowTmpName: this.workflowName,
          active: this.activeWorkflow,
          facilities: this.facilitySelectedList,
          carrierServices: this.carrierSelectedList,
          lineOperators: this.operatorSelectedList,
          stageTemplates: [],
          selected: null
        }        
        // Modify the workflow
        this.spinner.show();
        this.restfulApi.modifyWorkflowTemplates(modifiedWorkflow).subscribe(data => {
          this.workflowList[i] = new WorkflowTmpDTO(data);         
          this.sortWorkflows();         
          this.cancelEditWorkflow(modifiedWorkflow.workflowTmpId);       
          this.spinner.hide();                             
        },
        err => {
          this.logger.debug(`modifyExistingUser | ${err}`);
          enableAllButtons();  
          this.spinner.hide();
        });      
        break;          
      }
    }
    
  }

  cloneWorkflow(element: WorkflowTmpDTO) {
    this.spinner.show();
    this.restfulApi.cloneWorkflowTemplate(element.workflowTmpId).subscribe(res => {
      this.workflowList.unshift(res);
      console.log(res);
      const wf = {
        id: res.workflowTmpId,
        selected: false
      }
      this.workflowDisplay.push(wf);
      this.workflowDisplay = this.workflowDisplay.slice(0);
      this.changeSelected(res.workflowTmpId);
      this.sortWorkflows();
      this.spinner.hide();     
    },
    err => {
      this.logger.debug(`cloneWorkflow | ${err}`);
      this.spinner.hide();
    });    
  }

  removeWorkflow(){
    this.showConfirm = 'none';
    this.spinner.show();
    let element = this.confirmElement;
      element.active = false;    
      this.restfulApi.modifyWorkflowTemplates(element).subscribe(res => {
        this.workflowList = this.workflowList.filter(function( obj ) {
          return obj.workflowTmpId !== element.workflowTmpId;
        });
        var workflowTmpDTO: WorkflowTmpDTO = new WorkflowTmpDTO(<WorkflowTmpDTO>res);
        workflowTmpDTO.selected = true;
        this.workflowList.push(workflowTmpDTO);
        this.sortWorkflows();
        this.spinner.hide();  
      },
      err => {
        this.logger.debug(`deleteWorkflow | ${err}`);
        this.spinner.hide();
      });
  }

  deleteWorkflow(element: WorkflowTmpDTO) {
    if(element.workflowTmpId === -1) {
      this.cancelEditWorkflow(-1);
      return;
    }
    this.confirm('This workflow template will be marked as inactive. Future visits will not be associated with this workflow template. Do you want to delete this workflow template?', element, this.removeWorkflow);   
  }
       
 confirm(message: string, element: WorkflowTmpDTO, callback){
  this.showConfirm = 'block';
  this.confirmMessage = message;
  this.confirmCallback = callback;
  this.confirmElement = element;
}

  objectInArray(tempArray, tempObject) {
    let flag = false;
    tempArray.forEach(element => {
      if(JSON.stringify(element) === JSON.stringify(tempObject)) {
        flag = true;
      }
    });
    if(flag = true) return true;
    return false;
  }

  checkConflictFacility(id, facility) {
    if(this.conflictedWorkflow.get(id) === undefined) return false;
    else {
      let flag = 0;
      this.conflictedWorkflow.get(id).fac.forEach((f) => {
        if(f === facility) flag = 1;
      })
      if(flag === 1) return true;
      return false;
    }
  }

  checkConflictCarrier(id, carrier) {
    if(this.conflictedWorkflow.get(id) === undefined) return false;
    else {
      let flag = 0;
      this.conflictedWorkflow.get(id).car.forEach((f) => {
        if(f === carrier) flag = 1;
      })
      if(flag === 1) return true;
      return false;
    }
  }

  checkConflictOperator(id, op) {
    if(this.conflictedWorkflow.get(id) === undefined) return false;
    else {
      let flag = 0;
      this.conflictedWorkflow.get(id).op.forEach((f) => {
        if(f === op) flag = 1;
      })
      if(flag === 1) return true;
      return false;
    }
  }

  //Below are the validation functions and the validation utility functions.
  validateRowSubset() {
    //This will help us to validate the workflow. First lets filter by facilities. //We have to validate based on the facilitySelectedList and the workflowSelectedList.
    
    this.conflictedWorkflow.clear();
    if(this.activeWorkflow === false) return false; // Dont check if the workflow is inactive.
    //this.selectedWorkflow.workflowTmpName = this.selectedWorkflow.workflowTmpName.trim();
    let flg = 0;
    this.workflowList.forEach((wf) => {
      //We iterate through each workflow.
      if(wf != this.selectedWorkflow && wf.workflowTmpName.toUpperCase() === this.workflowName.toUpperCase() && wf.active === true) {
        //alert('Cannot have the same name!');
        flg = 1;
        this.conflictedWorkflow.set(wf.workflowTmpId, {
          name: wf.workflowTmpName, 
          fac: [],  
          car: [],
          op: []
        });
        console.log(this.conflictedWorkflow);
      }
    })
    //this.logger.debug(this.workflowList, this.workflowName);
    if(flg === 1) {
      return true;
    }
    let facilityConflict: Array<WorkflowTmpDTO> = [];
    let carrierConflict: Array<WorkflowTmpDTO> = [];
    let operatorConflict: Array<WorkflowTmpDTO> = [];
    //Facilities
    this.workflowList.forEach((wf) => {
      if(wf != this.selectedWorkflow && wf.active === true) {
        //Now we need to validate if there exists a row with the same facilities, workflow and carrier settings.
        let facilitiesExist: boolean;
        if(this.facilitySelectedList.length === 0 && wf.facilities.length === 0) facilityConflict.push(wf);
        if(this.facilitySelectedList.length > 0) facilitiesExist = true;
        else {
          facilitiesExist  = false;
        }
        this.facilitySelectedList.forEach((facility) => {
          let flag = false;
          wf.facilities.forEach((fc) => {
            if(facility.facilityId === fc.facilityId) {
              flag = true;
            }
          })
          facilitiesExist = facilitiesExist && flag;
        })
        if(facilitiesExist === true) {
          facilityConflict.push(wf)
        }
      }
    })    
    //Carrier Service.    
    facilityConflict.forEach((wf) => {
      if(wf != this.selectedWorkflow) {
        //Now we need to validate if there exists a row with the same facilities, workflow and carrier settings.
        let carrierExist: boolean;
        if(this.carrierSelectedList.length === 0 && wf.carrierServices.length === 0) carrierConflict.push(wf);
        if(this.carrierSelectedList.length > 0) carrierExist = true;
        else carrierExist  = false;
        this.carrierSelectedList.forEach((carrier) => {
          let flag = false;
          wf.carrierServices.forEach((cs) => {
            if(carrier.carrierServiceId === cs.carrierServiceId) {
              flag = true;
            }
          })
          carrierExist = carrierExist && flag;
        })
        if(carrierExist === true) {
          carrierConflict.push(wf)
        }
      }
    })
    //Line operators.
    carrierConflict.forEach((wf) => {
      if(wf != this.selectedWorkflow) {
        //Now we need to validate if there exists a row with the same facilities, workflow and carrier settings.
        let operatorExist: boolean;
        if(this.operatorSelectedList.length === 0 && wf.lineOperators.length === 0) operatorConflict.push(wf);
        if(this.operatorSelectedList.length > 0) operatorExist = true;
        else operatorExist  = false;
        this.operatorSelectedList.forEach((carrier) => {
          let flag = false;
          wf.lineOperators.forEach((cs) => {
            if(carrier.lineOperatorId === cs.lineOperatorId) {
              flag = true;
            }
          })
          operatorExist = operatorExist && flag;
        })
        if(operatorExist === true) {
          operatorConflict.push(wf)
        }
      }
    })
    //this.logger.debug(operatorConflict); //We are trying to see something here. It consists of the conflicting workflows.
    let myMap = new Map();
    operatorConflict.forEach((wf) => {
      myMap.set(wf.workflowTmpId, wf);
    }); 
    //Now we will store the conflcited workflows.
    operatorConflict.forEach((wf) => {
      let fac = []
      this.facilitySelectedList.forEach(element => {
          if(this.objectInArray(wf.facilities, element)) {
            fac.push(element.facilityId);
          }
        })
      let car = []
      this.carrierSelectedList.forEach(element => {
          if(this.objectInArray(wf.carrierServices, element)) {
            car.push(element.carrierServiceId);
          }
        })
      let op = []
      this.operatorSelectedList.forEach(element => {
          if(this.objectInArray(wf.lineOperators, element)) {
            op.push(element.lineOperatorId);
          }
        })
        this.conflictedWorkflow.set(wf.workflowTmpId, {
          fac, 
          car, 
          op
        })
      });
      
      operatorConflict.forEach((wf) => {
        if(wf.facilities.length === 0 && wf.carrierServices.length === 0 && wf.lineOperators.length === 0) {
          this.conflictedWorkflow.set(wf.workflowTmpId, {
            name: wf.workflowTmpName, 
            fac: [],  
            car: [],
            op: []
          });

          return true;
        }
      })
      if(operatorConflict.length > 0) return true;
      return false;
  }

  validateRowSuperset() {
    this.conflictedWorkflow.clear()
    if(this.activeWorkflow === false) return false;
    let facilityConflict: Array<WorkflowTmpDTO> = [];
    let carrierConflict: Array<WorkflowTmpDTO> = [];
    let operatorConflict: Array<WorkflowTmpDTO> = [];
    //Facilities
    this.workflowList.forEach((wf) => {
      if(wf != this.selectedWorkflow && wf.active === true) {
        //Now we need to validate if there exists a row with the same facilities, workflow and carrier settings.
        let facilitiesExist: boolean;
        if(this.facilitySelectedList.length === 0 && wf.facilities.length === 0) facilityConflict.push(wf);
        if(wf.facilities.length > 0) facilitiesExist = true;
        else {
          facilitiesExist  = false;
        }
        wf.facilities.forEach((facility) => {
          let flag = false;
          this.facilitySelectedList.forEach((fc) => {
            if(facility.facilityId === fc.facilityId) {
              flag = true;
            }
          })
          facilitiesExist = facilitiesExist && flag;
        })
        if(facilitiesExist === true) {
          facilityConflict.push(wf)
        }
      }
    })
    //this.logger.debug(facilityConflict);
    //Carrier Service.
    
    facilityConflict.forEach((wf) => {
      if(wf != this.selectedWorkflow) {
        //Now we need to validate if there exists a row with the same facilities, workflow and carrier settings.
        let carrierExist: boolean;
        if(this.carrierSelectedList.length === 0 && wf.carrierServices.length === 0) carrierConflict.push(wf);
        if(wf.carrierServices.length > 0) carrierExist = true;
        else carrierExist  = false;
        wf.carrierServices.forEach((carrier) => {
          let flag = false;
          this.carrierSelectedList.forEach((cs) => {
            if(carrier.carrierServiceId === cs.carrierServiceId) {
              flag = true;
            }
          })
          carrierExist = carrierExist && flag;
        })
        if(carrierExist === true) {
          carrierConflict.push(wf)
        }
      }
    })
    //this.logger.debug(carrierConflict);

    //Line operators.
    carrierConflict.forEach((wf) => {
      if(wf != this.selectedWorkflow) {
        //Now we need to validate if there exists a row with the same facilities, workflow and carrier settings.
        let operatorExist: boolean;
        if(this.operatorSelectedList.length === 0 && wf.lineOperators.length === 0) operatorConflict.push(wf);
        if(wf.lineOperators.length > 0) operatorExist = true;
        else operatorExist  = false;
        wf.lineOperators.forEach((carrier) => {
          let flag = false;
          this.operatorSelectedList.forEach((cs) => {
            if(carrier.lineOperatorId === cs.lineOperatorId) {
              flag = true;
            }
          })
          operatorExist = operatorExist && flag;
        })
        if(operatorExist === true) {
          operatorConflict.push(wf)
        }
      }
    })
    //this.logger.debug(operatorConflict); //We are trying to see something here. It consists of the conflicting workflows.
    let myMap = new Map();
    operatorConflict.forEach((wf) => {
      myMap.set(wf.workflowTmpId, wf);
    })
    //Now we will store the conflcited workflows.
    operatorConflict.forEach((wf) => {
      let fac = []
      this.facilitySelectedList.forEach(element => {
          if(this.objectInArray(wf.facilities, element)) {
            fac.push(element.facilityId);
          }
        })
      let car = []
      this.carrierSelectedList.forEach(element => {
          if(this.objectInArray(wf.carrierServices, element)) {
            car.push(element.carrierServiceId);
          }
        })
      let op = []
      this.operatorSelectedList.forEach(element => {
          if(this.objectInArray(wf.lineOperators, element)) {
            op.push(element.lineOperatorId);
          }
        })
        this.conflictedWorkflow.set(wf.workflowTmpId, {
          fac, 
          car, 
          op
        })
      });
      this.logger.debug(operatorConflict);
      if(operatorConflict.length > 0) return true;
      return false;
  }
}
  
