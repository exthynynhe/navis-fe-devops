import { Component, OnInit } from '@angular/core';
import { WorkflowTmpDTO } from 'src/app/model/WorkflowTmpDTO';
import { NgxSpinnerService } from 'ngx-spinner';
import { carriers, facilities, operators } from '../../../../global/utils';
import { RestFulAPIService } from '../../../../shared/services/restfulapi.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDropList } from '@angular/cdk/drag-drop';
import { StageEXT } from 'src/app/model/StageEXT';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'e2e-workflow-configuration',
  templateUrl: './workflow-configuration.component.html',
  styleUrls: ['./workflow-configuration.component.css']
})

export class WorkflowConfigurationComponent implements OnInit {
  
  public workflows: Array<WorkflowTmpDTO> = [];
  public operators: Array<any>;
  public carriers: Array<any>;
  public facilities: Array<any>;
  public stageLibrary: Array<StageEXT> = [];
  public selectedWorkflow: WorkflowTmpDTO;
  public alertMessage: string = "";
  public showAlert = 'none';

  constructor(private restFulAPI: RestFulAPIService, private spinner: NgxSpinnerService, private logger: NGXLogger) {
    this.carriers= carriers;
    this.facilities = facilities;
    this.operators = operators;
  }

  ngOnInit() {
    this.getWorkflows();
    this.getStageLibrary();              
  }

  alert(message: string){
    this.showAlert = 'block';
    this.alertMessage = message;
  }
  
  getSortOrder(prop){
    return function(a,b){
      if(a[prop] > b[prop]){
        return 1;        
      }
      else if(a[prop] < b[prop]){
        return -1;
      }
      return 0;
    }
  }

  sortWorkflows() {
    this.workflows.sort(function(a,b) {
      if(a.workflowTmpId === -1) {
        return -1;
      }
      if(a.workflowTmpName === null || a.workflowTmpName === undefined || a.workflowTmpName.trim() === "")
        return -1;       
      if (a.workflowTmpName.toLowerCase() < b.workflowTmpName.toLowerCase())
        return -1;
      if (a.workflowTmpName.toLowerCase() > b.workflowTmpName.toLowerCase())
        return 1;
      return 0;
    });            
  }
  getStageLibrary() {
    this.spinner.show();
    this.stageLibrary = [];
    this.restFulAPI.getStages().subscribe(res => {   
      res.forEach(element=> {
        var stage = new StageEXT(element, false);
        this.stageLibrary.push(stage);
      });

      //Now we filter our stage library.
      let randomArray = [];

      if(this.selectedWorkflow) {
        this.stageLibrary.forEach((stage) => {
          let flag = 0;
          this.selectedWorkflow.stageTemplates.forEach((wfStage) => {
            if(wfStage.stageTemplateId === stage.stageTemplateId) {
              flag = 1;
            }
          })
          if(flag === 0) {
            randomArray.push(stage);
          }
        })
      }
      this.stageLibrary = randomArray;
      this.sortSteps();    
      this.spinner.hide();
    },
      err => {
        this.logger.debug(`getStageLibrary | ${err}`);
        this.spinner.hide();
      });
  }

  sortSteps() {   
    this.stageLibrary.sort(function(a, b){
      return a.position-b.position;
    })    
    for(var i=0; i< this.stageLibrary.length; i++) {
      if(this.stageLibrary[i].children !== undefined && this.stageLibrary[i].children.length > 1) {
        this.stageLibrary[i].children.sort((a, b) => {
          if (a.name < b.name) return -1;
          else if (a.name > b.name) return 1;
          else return 0;
        });
      }
    }           
  }

  getWorkflowStageTemplates() {
    if(this.selectedWorkflow !== null && this.selectedWorkflow !== undefined) {
      return this.selectedWorkflow.stageTemplates;
    }
    else {
      return null;
    }
  }

  getSortedWorkflowStages(workflow){
    if(workflow !== null && workflow !== undefined) {
      workflow.stageTemplates.sort(this.getSortOrder("position"));    
      return workflow.stageTemplates;
    } else {
      return null;
    }    
  }

  
  getWorkflows() {
    this.spinner.show();
    this.workflows = [];
    this.restFulAPI.getWorkflowTemplates().subscribe(res => {   
      res.forEach(element=> {  
        var workflow = new WorkflowTmpDTO(element);     
        this.workflows.push(workflow);        
      });   
      if(this.workflows !== undefined && this.workflows !== null && this.workflows.length > 0){
        this.workflows[0].selected = true;
        this.selectedWorkflow = this.workflows[0];
      }            
      this.sortWorkflows();
      this.spinner.hide();
    },    
    err => {
      this.logger.debug(`getWorkflows | `, err);
      this.spinner.hide();
    });
  }

  dropToWorkflowSteps(event: CdkDragDrop<string[]>) {
    var currentWorkflow = this.selectedWorkflow;
    if (event.previousContainer === event.container) {
      //Reorder workflow steps
      var stepToDrop = currentWorkflow.stageTemplates[event.previousIndex];
      var modifiedTestWorkflow: WorkflowTmpDTO = JSON.parse(JSON.stringify(currentWorkflow));
      moveItemInArray(modifiedTestWorkflow.stageTemplates, event.previousIndex, event.currentIndex);
      let i =1;
      modifiedTestWorkflow.stageTemplates.forEach((ele) => {
        ele.position = i++;
      });
      this.spinner.show();
      this.restFulAPI.modifyExistingStage(modifiedTestWorkflow, stepToDrop).subscribe((element) => {
        if(element){
          moveItemInArray(currentWorkflow.stageTemplates, event.previousIndex, event.currentIndex);
          let i =1;
          currentWorkflow.stageTemplates.forEach((ele) => {
            ele.position = i++;
          });
          this.logger.debug("reordered stage", currentWorkflow, element);
          this.spinner.hide();
        }
        else{
          this.spinner.hide();
          this.alert("Cannot reorder stage");
        }
      });
    } else {
      var stepToDrop = this.stageLibrary[event.previousIndex];
      this.stageLibrary.splice(event.previousIndex, 1);
      var currentWorkflowStageTemplatesNames = [];
      currentWorkflow.stageTemplates.forEach((ele) => {
        currentWorkflowStageTemplatesNames.push(ele.name);
      })            
      if(currentWorkflowStageTemplatesNames.indexOf(stepToDrop.name)<0){
        var modifiedTestWorkflow: WorkflowTmpDTO = JSON.parse(JSON.stringify(currentWorkflow));
        modifiedTestWorkflow.stageTemplates.splice(event.currentIndex, 0, stepToDrop);
        let i =1;
        modifiedTestWorkflow.stageTemplates.forEach((ele) => {
          ele.position = i++;
        });        
        this.spinner.show();
        this.restFulAPI.addNewStage(modifiedTestWorkflow, stepToDrop).subscribe((element) => {
          if(element){
            //Insert in workflow steps
            currentWorkflow.stageTemplates.splice(event.currentIndex, 0, stepToDrop );
            i =1;
            currentWorkflow.stageTemplates.forEach((ele) => {
              ele.position = i++;
            });
            this.logger.debug(element, "stage added");
            this.spinner.hide();
          }
          else{
            this.spinner.hide();
            this.alert("Cannot add stage");
          }
        });        
      }
    }
  }

  dropToStepLibrary(event: CdkDragDrop<string[]>) {
    var currentWorkflow = this.selectedWorkflow;
    if (event.previousContainer !== event.container) {
      var stepToDrop = currentWorkflow.stageTemplates[event.previousIndex];
      var stageLibraryStageTemplatesNames = [];
      this.stageLibrary.forEach((ele) => {
        stageLibraryStageTemplatesNames.push(ele.name);
      })
      if(stageLibraryStageTemplatesNames.indexOf(stepToDrop.name) <0 ){
        //Insert in the stage library
        this.stageLibrary.push(stepToDrop);
      }
      var modifiedTestWorkflow: WorkflowTmpDTO = JSON.parse(JSON.stringify(currentWorkflow));
      modifiedTestWorkflow.stageTemplates.splice(event.previousIndex, 1);
      let i =1;
      modifiedTestWorkflow.stageTemplates.forEach((ele) => {
        ele.position = i++;
      });      
      //Post to API
      this.spinner.show();
      this.restFulAPI.removeExistingStage(modifiedTestWorkflow, stepToDrop).subscribe((element) => {
        if(element){
          //Remove from workflow steps
          currentWorkflow.stageTemplates.splice(event.previousIndex,1);
          let i =1;
          currentWorkflow.stageTemplates.forEach((ele) => {
            ele.position = i++;
          });
          this.spinner.hide();
          this.logger.debug("stage: ", element, "stage removed!");
        }
        else{
          this.spinner.hide();
          this.alert('Cannot be removed!');
        }
      })
      this.sortSteps();
    }
  }

  showSteps(workflow: WorkflowTmpDTO) {        
    this.workflows.forEach(element => {
      element.selected = false;
    });
    workflow.selected = true;   
    this.selectedWorkflow = workflow; 
    this.getStageLibrary();
  }
}
