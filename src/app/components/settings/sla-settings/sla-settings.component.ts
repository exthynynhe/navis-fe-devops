import { Component, OnInit } from '@angular/core';
import { SlaSettingDTO } from '../../../model/SlaSettingDTO';
import { RestFulAPIService } from '../../../shared/services/restfulapi.service';
import { checkInputValue, getWeekdayNumber, disableAllButtons, enableAllButtons, weekDays } from '../../../global/utils';
import { NgxSpinnerService } from 'ngx-spinner';
import { FacilityDTO } from 'src/app/model/FacilityDTO';
import { LineOperatorDTO } from 'src/app/model/LineOperatorDTO';
import { CarrierServiceDTO } from 'src/app/model/CarrierServiceDTO';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'e2e-sla-settings',
  templateUrl: './sla-settings.component.html',
  styleUrls: ['./sla-settings.component.scss']
})
export class SlaSettingsComponent implements OnInit {
    
  public slaSettings: Array<SlaSettingDTO> = [] ;
  public dataSource;
  public weekDays;
  public facilitySelectedList: Array<FacilityDTO> = [];
  public lineOperatorSelectedList: Array<LineOperatorDTO> = [];
  public serviceSelectedList: Array<CarrierServiceDTO> = [];
  public operatorOptions: Array<LineOperatorDTO> = [];
  public facilityOptions: Array<FacilityDTO> = [];
  public serviceOptions: Array<CarrierServiceDTO> = [];
  public showAlert = 'none';
  public alertMessage: string = "";
  public showConfirm = 'none';
  public confirmMessage = "";
  public confirmCallback;
  public confirmElement;
  
  constructor(private restFulAPI: RestFulAPIService, private spinner: NgxSpinnerService, private logger: NGXLogger) { 
    this.weekDays = weekDays;
  }

  ngOnInit() {    
    this.getSlaSettings();   
    this.getFacilities();
    this.getOperators();
    this.getCarrierServices();
  }

  sortSteps() {
    this.slaSettings.sort(function(a,b) {
      if(a.slaId === "new") {
        return -1;
      }
      if(a.service === null || a.service === undefined || a.service === "")
        return -1;      
      if (a.service.toLowerCase() < b.service.toLowerCase())
        return -1;
      if (a.service.toLowerCase() > b.service.toLowerCase())
        return 1;
      return 0;
    });            
  } 

  getSlaSettings() {
    this.spinner.show();
    this.slaSettings = [];
    this.restFulAPI.getSlaSettings().subscribe(res => {   
      res.forEach(element=> {        
        this.slaSettings.push(<SlaSettingDTO>element);
      });
      this.sortSteps();      
      this.spinner.hide();
    },
    err => {
      this.logger.debug(`getSlaSettings | ${err}`);
      this.enableAllButtons();
      this.spinner.hide();
    });
  }

  getOperators() {
    this.spinner.show();
    this.restFulAPI.getLineOperators().subscribe(res => {
      this.operatorOptions = res;
      this.spinner.hide();
    }),
    err => {
      this.logger.debug(`getOperators | ${err}`);
      this.spinner.hide();
    };
  }

  getFacilities(){
    this.spinner.show();
    this.restFulAPI.getFacilities().subscribe(res => {
      this.facilityOptions = res;
      this.spinner.hide();
    }),
    err => {
      this.logger.debug(`getFacilities | ${err}`);
      this.spinner.hide();
    };
  }

  getCarrierServices(){
    this.spinner.show();
    this.restFulAPI.getCarrierServices().subscribe(res => {
      this.serviceOptions = res;
      this.spinner.hide();
    }),
    err => {
      this.logger.debug(`getCarrierServices | ${err}`);
      this.spinner.hide();
    };
  }
  addNewRow() {
    // Check if already exists an empty row
    var allowAdd = true;
    this.slaSettings.forEach(function(element) {      
      if(element.slaId === "new") {
        allowAdd = false;
      }
    });
    // Allow or not the edition
    if(allowAdd) {
      let mySlaSetting = new SlaSettingDTO();
      mySlaSetting.slaId = "new";            
      this.slaSettings.push(mySlaSetting);
      this.sortSteps();      
      // Move scroll to the top
      var myDiv = document.getElementById('tblContent');    
      myDiv.scrollTop = 0; 
    }
  }

  removeSLA(){
    let element = this.confirmElement;
    this.spinner.show();
      this.restFulAPI.deleteSlaSetting(element).subscribe(data => {
        this.slaSettings = this.slaSettings.filter(function( obj ) {
          return obj.slaId !== element.slaId;
        });
        this.sortSteps();        
        this.spinner.hide();
        this.showConfirm = 'none';
      },
      err => {
        this.logger.debug(`removeRow | ${err}`);
        this.enableAllButtons();
        this.spinner.hide();
        this.showConfirm = 'none';
      });
  }

  removeRow(element: SlaSettingDTO) {
    if(element.slaId === "new") {
      var index = -1;
      for(var i=0; i<= this.slaSettings.length -1; i++)
      {
        if(this.slaSettings[i].slaId === "new") {
          index = this.slaSettings.indexOf(this.slaSettings[i]);
          break;
        }
      }
      if(index !== -1) {
        this.slaSettings.splice(index,1);
        this.sortSteps();   
      }       
      return;
    }
    this.confirm("This services’ SLA information will be deleted and users will no longer receive notifications for this service.", element, this.removeSLA);
  }
  
  confirm(message: string, element: SlaSettingDTO, callback){
    this.showConfirm = 'block';
    this.confirmMessage = message;
    this.confirmCallback = callback;
    this.confirmElement = element;
  }

  
  getSlaSettingData(rowId, slaSettingObject: SlaSettingDTO) {
    slaSettingObject.facilities = this.facilitySelectedList;
    slaSettingObject.lineOperators = this.lineOperatorSelectedList;
    slaSettingObject.carrierServices = this.serviceSelectedList;
    
    //For now adding service as the first element of selected services, needs to changed in future!
    slaSettingObject.service = this.serviceSelectedList[0].carrierServiceIdName;
    slaSettingObject.coprarLoadDeadline = Number((document.getElementById("coprarLoadDeadline input " + rowId) as HTMLInputElement).value);
    slaSettingObject.coprarDischDeadline = Number((document.getElementById("coprarDischDeadline input " + rowId) as HTMLInputElement).value);
    slaSettingObject.movinsDeadline = Number((document.getElementById("movinsDeadline input " + rowId) as HTMLInputElement).value);
    slaSettingObject.baplieDeadline = Number((document.getElementById("baplieDeadline input " + rowId) as HTMLInputElement).value);
    slaSettingObject.forecastDeadline = Number((document.getElementById("cargoCutoff input " + rowId) as HTMLInputElement).value);
    slaSettingObject.berthProdMin = Number((document.getElementById("berthProdMin input " + rowId) as HTMLInputElement).value);
    slaSettingObject.berthProdMax = Number((document.getElementById("berthProdMax input " + rowId) as HTMLInputElement).value);
    slaSettingObject.dischMoves = Number((document.getElementById("dischargeMoves input " + rowId) as HTMLInputElement).value);
    slaSettingObject.loadMoves = Number((document.getElementById("loadMoves input " + rowId) as HTMLInputElement).value);
    slaSettingObject.craneIntensity = Number((document.getElementById("craneIntensity input " + rowId) as HTMLInputElement).value);    
    slaSettingObject.eta = new Date();
    var currentDay = ((<HTMLSelectElement>document.getElementById("eta weekday select " + rowId)).value.trim());    
    var distance = (getWeekdayNumber(currentDay) + 7 - slaSettingObject.eta.getDay()) % 7;    
    slaSettingObject.eta.setDate(slaSettingObject.eta.getDate() + distance);    
    slaSettingObject.eta.setHours(Number((document.getElementById("eta hour input " + rowId) as HTMLInputElement).value));
    slaSettingObject.eta.setMinutes(Number((document.getElementById("eta minute input " + rowId) as HTMLInputElement).value));                        
    slaSettingObject.etd = new Date();
    var currentDay = ((<HTMLSelectElement>document.getElementById("etd weekday select " + rowId)).value.trim());    
    var distance = (getWeekdayNumber(currentDay) + 7 - slaSettingObject.etd.getDay()) % 7;    
    slaSettingObject.etd.setDate(slaSettingObject.etd.getDate() + distance);    
    slaSettingObject.etd.setHours(Number((document.getElementById("etd hour input " + rowId) as HTMLInputElement).value));
    slaSettingObject.etd.setMinutes(Number((document.getElementById("etd minute input " + rowId) as HTMLInputElement).value));                        
  }

  
  alert(message: string){
    this.showAlert = 'block';
    this.alertMessage = message;
  }


  createNewSlaSetting(currentSlaSetting: SlaSettingDTO) {
    // Create object locally
    let newSlaSetting = new SlaSettingDTO();
    newSlaSetting.facilities = this.facilitySelectedList;
    newSlaSetting.lineOperators = this.lineOperatorSelectedList;
    newSlaSetting.carrierServices = this.serviceSelectedList;
    //newSlaSetting.service = document.getElementById("slaSetting " + currentSlaSetting.slaId).textContent.trim();
    newSlaSetting.slaName = document.getElementById("slaName " + currentSlaSetting.slaId).textContent.trim();
    this.getSlaSettingData(currentSlaSetting.slaId, newSlaSetting);    
    var saveSlaSetting = true;
    // Check service name does not exist or order not repeated, fields not empty...
    for(var i=0; i<= this.slaSettings.length -1; i++)
    {
      if(this.slaSettings[i].service === newSlaSetting.service) {
        this.alert("A SLA Setting with same N4 Service already exists");
        saveSlaSetting = false;
        break;
      }
    }
    if(saveSlaSetting)
    {            
      // Remove the initial empty one
      this.slaSettings = this.slaSettings.filter(function( obj ) {
        return obj.slaId !== "new";
      });
      this.spinner.show();
      // Add the new one, update datasource and cancel editing        
      this.restFulAPI.addSlaSetting(newSlaSetting).subscribe(data => {        
          this.slaSettings.push(new SlaSettingDTO(data));
          this.sortSteps();               
          this.spinner.hide(); 
          this.cancelEditRow(data, true);                       
        },
        err => {
          this.logger.debug(`createNewSlaSetting | ${err}`);
          this.enableAllButtons();
          this.spinner.hide();
        }      
      );      
    } 
  }

  modifyExistingSlaSetting(currentSlaSetting: SlaSettingDTO) {
    var modifyObject = true;
    if(modifyObject) {
      var modifiedSlaSetting = new SlaSettingDTO();
      for(var i=0; i<= this.slaSettings.length -1; i++)
      {
        if(this.slaSettings[i].slaId === currentSlaSetting.slaId) {                    
          modifiedSlaSetting.slaId = currentSlaSetting.slaId; 
          //modifiedSlaSetting.service = document.getElementById("slaSetting " + currentSlaSetting.slaId).textContent.trim();
          modifiedSlaSetting.slaName = document.getElementById("slaName " + currentSlaSetting.slaId).textContent.trim();
          this.getSlaSettingData(currentSlaSetting.slaId, modifiedSlaSetting);
          // Modify the slaSetting
          this.spinner.show();
          this.restFulAPI.modifySlaSetting(modifiedSlaSetting).subscribe(data => {
            this.slaSettings[i] = new SlaSettingDTO(data);         
            this.sortSteps();                     
            this.spinner.hide();
            document.getElementById("mainTableSlaSettings").style.display = "block";
            this.cancelEditRow(data, false);              
          },
          err => {
            this.logger.debug(`modifyExistingSlaSetting | ${err}`);
            this.enableAllButtons();  
            this.spinner.hide();
          });      
          break;          
        }
      }
    }
  }

  saveRow(currentSlaSetting: SlaSettingDTO) {
    // Check SLA name is not empty
    if(document.getElementById("slaName " + currentSlaSetting.slaId).textContent.trim() === "") {
      this.showAlert = 'block';
      this.alert("SLA setting must contain a SLA name. ");
      return;
    }
    //Check if services is not empty
    if(this.serviceSelectedList==undefined || this.serviceSelectedList.length===0){
      this.alert("SLA setting must contain some Service. ");
      return;
    }
    //Check if facilities is not empty
    if(this.facilitySelectedList==undefined || this.facilitySelectedList.length===0){
      this.alert("SLA setting must contain some Facility. ");
      return;
    }
    //Check if Line Operator is not empty
    if(this.lineOperatorSelectedList==undefined || this.lineOperatorSelectedList.length===0){
      this.alert("SLA setting must contain some Line Operator. ");
      return;
    }
    // Check coprar load deadline is not empty
    if((document.getElementById("coprarLoadDeadline input " + currentSlaSetting.slaId) as HTMLInputElement).value === "") {
      this.alert("SLA setting must contain a COPRAR load deadline. ");
      return;
    }
    // Check coprar discharge deadline is not empty
    if((document.getElementById("coprarDischDeadline input " + currentSlaSetting.slaId) as HTMLInputElement).value === "") {
      this.alert("SLA setting must contain a COPRAR discharge deadline. ");
      return;
    }
    // Check baplie deadline is not empty
    if((document.getElementById("baplieDeadline input " + currentSlaSetting.slaId) as HTMLInputElement).value === "") {
      this.alert("SLA setting must contain a BAPLIE deadline. ");
      return;
    }
    // Check movins is not empty
    if((document.getElementById("movinsDeadline input " + currentSlaSetting.slaId) as HTMLInputElement).value === "") {
      this.alert("SLA setting must contain a MOVINS deadline. ");
      return;
    }
    // Check forecast is not empty
    if((document.getElementById("cargoCutoff input " + currentSlaSetting.slaId) as HTMLInputElement).value === "") {
      this.alert("SLA setting must contain a FORECAST deadline. ");
      return;
    }
    // Check ETA is not empty
    if((document.getElementById("eta hour input " + currentSlaSetting.slaId) as HTMLInputElement).value === "" ||
      (document.getElementById("eta minute input " + currentSlaSetting.slaId) as HTMLInputElement).value === "" || 
      (<HTMLSelectElement>document.getElementById("eta weekday select " + currentSlaSetting.slaId)).value.trim() === "") {
      this.alert("SLA setting must contain a ETA. ");
      return;
    }
    // Check ETD is not empty
    if((document.getElementById("etd hour input " + currentSlaSetting.slaId) as HTMLInputElement).value === "" ||
      (document.getElementById("etd minute input " + currentSlaSetting.slaId) as HTMLInputElement).value === "" || 
      (<HTMLSelectElement>document.getElementById("etd weekday select " + currentSlaSetting.slaId)).value.trim() === "") {
      this.alert("SLA setting must contain a ETD. ");
      return;
    }
    // New record
    if(currentSlaSetting.slaId === "new") {
      this.createNewSlaSetting(currentSlaSetting)
    } else {
      this.modifyExistingSlaSetting(currentSlaSetting)
    }   
  }

  cancelRow(element: SlaSettingDTO) {
    if(element.slaId === "new") {
      let index = this.slaSettings.indexOf(element);
      if (index >= 0) {
        this.slaSettings.splice(index, 1);
        this.sortSteps();        
        this.enableAllButtons();
      }
    } else {;
      this.cancelEditRow(element, false);
      this.getSlaSettings();      
    }
  }

  cancelEditRow(element: SlaSettingDTO, newSlaSetting) {          
    this.enableAllButtons();
    if(!newSlaSetting) {
      var currentSlaSettingId = element.slaId;
      document.getElementById("edit buttons " + currentSlaSettingId).style.display = "none";
      document.getElementById("main buttons " + currentSlaSettingId).style.display = "block";
      // Disable editing row
      document.getElementById("slaName " + currentSlaSettingId).contentEditable = 'false'; 
      //document.getElementById("slaSetting " + currentSlaSettingId).contentEditable = 'false'; 
      document.getElementById("coprarLoadDeadline " + currentSlaSettingId).className = 'cellContentReadable';     
      (document.getElementById("coprarLoadDeadline input " + currentSlaSettingId) as HTMLInputElement).readOnly = true;    
      document.getElementById("coprarDischDeadline " + currentSlaSettingId).className = 'cellContentReadable';     
      (document.getElementById("coprarDischDeadline input " + currentSlaSettingId) as HTMLInputElement).readOnly = true;    
      document.getElementById("baplieDeadline " + currentSlaSettingId).className = 'cellContentReadable'; 
      (document.getElementById("baplieDeadline input " + currentSlaSettingId) as HTMLInputElement).readOnly = true;  
      document.getElementById("movinsDeadline " + currentSlaSettingId).className = 'cellContentReadable'; 
      (document.getElementById("movinsDeadline input " + currentSlaSettingId) as HTMLInputElement).readOnly = true;
      document.getElementById("cargoCutoff " + currentSlaSettingId).className = 'cellContentReadable';  
      (document.getElementById("cargoCutoff input " + currentSlaSettingId) as HTMLInputElement).readOnly = true;
      document.getElementById("berthProductivity " + currentSlaSettingId).className = 'cellContentReadable'; 
      (document.getElementById("berthProdMin input " + currentSlaSettingId) as HTMLInputElement).readOnly = true;
      (document.getElementById("berthProdMax input " + currentSlaSettingId) as HTMLInputElement).readOnly = true;
      document.getElementById("dischargeMoves " + currentSlaSettingId).className = 'cellContentReadable'; 
      (document.getElementById("dischargeMoves input " + currentSlaSettingId) as HTMLInputElement).readOnly = true;
      document.getElementById("loadMoves " + currentSlaSettingId).className = 'cellContentReadable'; 
      (document.getElementById("loadMoves input " + currentSlaSettingId) as HTMLInputElement).readOnly = true;
      document.getElementById("craneIntensity " + currentSlaSettingId).className = 'cellContentReadable'; 
      (document.getElementById("craneIntensity input " + currentSlaSettingId) as HTMLInputElement).readOnly = true; 
      document.getElementById("eta " + currentSlaSettingId).className = 'cellContentReadable';  
      document.getElementById("eta editArea " + currentSlaSettingId).style.display = "none";
      document.getElementById("eta valueArea " + currentSlaSettingId).style.display = "block";
      document.getElementById("etd " + currentSlaSettingId).className = 'cellContentReadable';   
      document.getElementById("etd editArea " + currentSlaSettingId).style.display = "none";
      document.getElementById("etd valueArea " + currentSlaSettingId).style.display = "block";      
    }   
  }

  editRow(element: SlaSettingDTO) {
    this.lineOperatorSelectedList = element.lineOperators;
    this.facilitySelectedList = element.facilities;
    this.serviceSelectedList = element.carrierServices;

    // Disable rest of buttons
    this.disableAllButtons();
    document.getElementById("main buttons " + element.slaId).style.display = "none";
    document.getElementById("edit buttons " + element.slaId).style.display = "block";
    // Show editing buttons
    (document.getElementById("save " + element.slaId) as HTMLButtonElement).disabled = false;
    (document.getElementById("cancel " + element.slaId) as HTMLButtonElement).disabled = false;
    // Allow editing row
    // if(element.slaId === "new") {
    //   document.getElementById("slaSetting " + element.slaId).contentEditable = 'true';          
    //   document.getElementById("slaSetting " + element.slaId).className = 'cellContentEditable';          
    // }
    document.getElementById("slaName " + element.slaId).contentEditable = 'true';          
    document.getElementById("slaName " + element.slaId).className = 'cellContentEditable';   
    document.getElementById("slaFacilities " + element.slaId +" text").style.display = "none";
    document.getElementById("slaFacilities " + element.slaId + " edit").style.display = "block";
    document.getElementById("slaLineOperators " + element.slaId + " text").style.display = "none";
    document.getElementById("slaLineOperators " + element.slaId + " edit").style.display = "block";
    document.getElementById("slaServices " + element.slaId + " text").style.display = "none";
    document.getElementById("slaServices " + element.slaId + " edit").style.display = "block";
    document.getElementById("coprarLoadDeadline " + element.slaId).className = 'cellContentEditable'; 
    (document.getElementById("coprarLoadDeadline input " + element.slaId) as HTMLInputElement).readOnly = false;
    document.getElementById("coprarDischDeadline " + element.slaId).className = 'cellContentEditable'; 
    (document.getElementById("coprarDischDeadline input " + element.slaId) as HTMLInputElement).readOnly = false;
    document.getElementById("baplieDeadline " + element.slaId).className = 'cellContentEditable'; 
    (document.getElementById("baplieDeadline input " + element.slaId) as HTMLInputElement).readOnly = false;
    document.getElementById("movinsDeadline " + element.slaId).className = 'cellContentEditable'; 
    (document.getElementById("movinsDeadline input " + element.slaId) as HTMLInputElement).readOnly = false;
    document.getElementById("cargoCutoff " + element.slaId).className = 'cellContentEditable';  
    (document.getElementById("cargoCutoff input " + element.slaId) as HTMLInputElement).readOnly = false;
    document.getElementById("berthProductivity " + element.slaId).className = 'cellContentEditable'; 
    (document.getElementById("berthProdMin input " + element.slaId) as HTMLInputElement).readOnly = false;
    (document.getElementById("berthProdMax input " + element.slaId) as HTMLInputElement).readOnly = false;
    document.getElementById("dischargeMoves " + element.slaId).className = 'cellContentEditable'; 
    (document.getElementById("dischargeMoves input " + element.slaId) as HTMLInputElement).readOnly = false;
    document.getElementById("loadMoves " + element.slaId).className = 'cellContentEditable'; 
    (document.getElementById("loadMoves input " + element.slaId) as HTMLInputElement).readOnly = false;
    document.getElementById("craneIntensity " + element.slaId).className = 'cellContentEditable'; 
    (document.getElementById("craneIntensity input " + element.slaId) as HTMLInputElement).readOnly = false;
    document.getElementById("eta " + element.slaId).className = 'cellContentEditable';  
    document.getElementById("eta editArea " + element.slaId).style.display = "block";
    document.getElementById("eta valueArea " + element.slaId).style.display = "none";
    document.getElementById("etd " + element.slaId).className = 'cellContentEditable';         
    document.getElementById("etd editArea " + element.slaId).style.display = "block";
    document.getElementById("etd valueArea " + element.slaId).style.display = "none";    
  }

  disableAllButtons() {
    disableAllButtons();
  }

  enableAllButtons() {
    enableAllButtons();
  }

  checkInputValue(input, minValue, maxValue){
    checkInputValue(input, minValue, maxValue);
  }
}
