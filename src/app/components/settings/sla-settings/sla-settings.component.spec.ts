import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlaSettingsComponent } from './sla-settings.component';

describe('SlaSettingsComponent', () => {
  let component: SlaSettingsComponent;
  let fixture: ComponentFixture<SlaSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlaSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlaSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
