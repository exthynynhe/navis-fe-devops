import { Component, OnInit } from '@angular/core';
import { AppSettingDTO } from 'src/app/model/AppSettingDTO';
import { RestFulAPIService } from '../../../shared/services/restfulapi.service';
import { Globals } from '../../../global/globals';
import { NgxSpinnerService } from 'ngx-spinner';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'e2e-application-settings',
  templateUrl: './application-settings.component.html',
  styleUrls: ['./application-settings.component.scss']
})

export class ApplicationSettingsComponent implements OnInit {

  public settingDTO: AppSettingDTO = new AppSettingDTO();

  constructor(private restFulAPI: RestFulAPIService, private spinner: NgxSpinnerService, private globals: Globals, private logger: NGXLogger) { }

  ngOnInit() {
    this.initializeSettings(); 
    let element: HTMLElement = document.getElementById('app-settings-setup-tab-btn') as HTMLElement;
    element.click();   
  }  

  initializeSettings() {
    this.spinner.show();    
    this.restFulAPI.getApplicationSettings().subscribe(res => {         
      this.settingDTO = res;      
      this.spinner.hide();    
    },
    err => {
      this.logger.debug(`getApplicationSettings| ${err}`);
      this.spinner.hide();    
    });
  }

  openTab(evt, tabId) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(tabId).style.display = "block";
    document.getElementById(tabId + '-btn').className += " active";
  } 

  changeAutoUpdateWithFiltersValue(event) {
    this.settingDTO.autoUpdateWithFilters = event;    
    this.saveSettings();
  }

  changeResetAllDschValue(event) {
    this.settingDTO.resetAllDsch = event;    
    this.saveSettings();
  }

  changeMultipleStepsCompleteValue(event) {
    this.settingDTO.multipleStepsComplete = event;    
    this.saveSettings();
  }

  saveSettings() {        
    this.settingDTO.autoUpdateFrequency = parseInt((document.getElementById("app-settings-input-update-dashboard") as HTMLInputElement).value);    
    this.settingDTO.amountItemsPagination = parseInt((document.getElementById("app-settings-input-update-dashboard-pagination") as HTMLInputElement).value);    
    this.restFulAPI.modifyApplicationSettings(this.settingDTO).subscribe(data => {                      
      this.settingDTO = data;
      this.globals.updateApplicationSettings(this.settingDTO);                     
    },
    err => {
      this.logger.debug(`modify application settings | ${err}`);
    });
  }
}
