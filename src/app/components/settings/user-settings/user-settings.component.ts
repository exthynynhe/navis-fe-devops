import { Component, OnInit, Input } from '@angular/core';
import { UserDTO } from '../../../model/UserDTO';
import { RestFulAPIService } from '../../../shared/services/restfulapi.service';
import { disableAllButtons, enableAllButtons, userRoles, notificationProfiles } from '../../../global/utils';
import { NgxSpinnerService } from 'ngx-spinner';
import { NGXLogger } from 'ngx-logger';
import { UserPreferenceDTO } from 'src/app/model/UserPreferenceDTO';
import { colDescs } from '../../../global/utils'
import { element } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'e2e-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {
  
  public checkboxEvent;
  public users: Array<UserDTO> = [];
  public notificationProfiles;
  public userRoles;
  public userPreferenceDTO: UserPreferenceDTO;
  public showAlert = 'none';
  public alertMessage = "";
  public showConfirm = 'none';
  public confirmMessage = "";
  public confirmCallback;
  public confirmElement;
  
  constructor(private restFulAPI: RestFulAPIService, private spinner: NgxSpinnerService, private logger: NGXLogger) {
    this.notificationProfiles = notificationProfiles;
    this.userRoles = userRoles;
  }

  ngOnInit() {
    this.getUsers();    
  }

  alert(message: string){
    this.showAlert = 'block';
    this.alertMessage = message;
  }

  sortUsers() {
    this.users.sort(function(a,b) {
      if(a.userId === "new") {
        return -1;
      }
      if(a.username === null || a.username === undefined || a.username.trim() === "")
        return -1;       
      if (a.username.toLowerCase() < b.username.toLowerCase())
        return -1;
      if (a.username.toLowerCase() > b.username.toLowerCase())
        return 1;
      return 0;
    });            
  } 

  getNotificationValue(currentUser: UserDTO) : String {
    for(var i=0; i< notificationProfiles.length; i++) {
      if(notificationProfiles[i].value === currentUser.notificationProfileId) {
        return notificationProfiles[i].text;
      }
    }
    return '';    
  }

  getRoleValue(currentUser: UserDTO) : String {    
    for(var i=0; i< userRoles.length; i++) {
      if(userRoles[i].value === currentUser.roleId) {
        return userRoles[i].text;
      }
    }
    return '';  
  }

  getUsers() {
    this.spinner.show();
    this.users = [];
    this.restFulAPI.getUsers().subscribe(res => {   
      res.forEach(element=> {        
        this.users.push(<UserDTO>element);
      });
      this.sortUsers();    
      this.spinner.hide();
    },    
    err => {
      this.logger.debug(`getUsers | ${err}`);
      this.enableAllButtons();
      this.spinner.hide();
    });
  }

  addNewRow() {
    // Check if already exists an empty row
    var allowAdd = true;
    this.users.forEach(function(element) {      
      if(element.userId  === "new") {
        allowAdd = false;
      }
    });
    // Allow or not the edition
    if(allowAdd) {
      let myUser = new UserDTO();
      myUser.userId = "new";            
      myUser.active = true;
      this.users.push(myUser);
      this.sortUsers();      
      // Move scroll to the top
      var myDiv = document.getElementById('tblContent'); 
      myDiv.scrollTop = 0; 
    }
  }

  removeUser(){
    this.showConfirm='none';
    this.spinner.show();
    this.confirmElement.active = false;
    this.confirmElement.password = undefined;
    this.confirmElement.repeatPassword = undefined;
    let element = this.confirmElement;
    this.restFulAPI.deleteUser(element).subscribe(data => {
      this.users = this.users.filter(function( obj ) {
        return obj.userId !== element.userId;
      });
      this.users.push(new UserDTO(<UserDTO>data));
      this.sortUsers();        
      this.spinner.hide();
    },
    err => {
      this.logger.debug(`removeRow | ${err}`);
      this.enableAllButtons();
      this.spinner.hide();
      
    });
  }

  removeRow(element: UserDTO) {    
    // Remove an empty element
    if(element.userId === "new") {
      var index = -1;
      for(var i=0; i<= this.users.length -1; i++)
      {
        if(this.users[i].userId === "new") {
          index = this.users.indexOf(this.users[i]);
          break;
        }
      }
      if(index !== -1) {
        this.users.splice(index,1);
        this.sortUsers();   
      }       
      return;
    }
    this.confirm("This user will be deleted and will no longer have access to Compass.", element, this.removeUser);
  }
  
      
  confirm(message: string, element: UserDTO, callback){
    this.showConfirm = 'block';
    this.confirmMessage = message;
    this.confirmCallback = callback;
    this.confirmElement = element;
  }

  unCheckUser(){
    let event = this.checkboxEvent;
    let userObject = this.confirmElement;
    for(var i = 0; i < this.users.length; i++) {
      if(this.users[i].userId === userObject.userId) {
        this.users[i].active = event.target.checked;
      }
    }
    this.showConfirm = 'none';
    return;
  }

  checkChange(event, userObject: UserDTO) {   
    this.checkboxEvent = event; 
    if(!this.checkboxEvent.target.checked) {
        this.confirm("This user will be deleted and will no longer have access to Compass.",userObject, this.unCheckUser); 
    }
  }

  getUserData(rowId, userObject: UserDTO) {    
    userObject.username = (document.getElementById("username input " + rowId) as HTMLInputElement).value;
    userObject.password = (document.getElementById("password input " + rowId) as HTMLInputElement).value;    
    userObject.roleId = Number((<HTMLSelectElement>document.getElementById("role select field " + rowId)).value.trim());
    userObject.notificationProfileId = Number((<HTMLSelectElement>document.getElementById("profile select field " + rowId)).value.trim());
  }

  createNewUser(currentUser: UserDTO) {
    // Create object locally
    let newUser = new UserDTO(); 
    newUser.active = true;    
    this.getUserData(currentUser.userId, newUser);
    newUser.active = (document.getElementById("active check new") as HTMLInputElement).checked;
    var saveUser = true;
    // Check username does not exist
    for(var i=0; i<= this.users.length -1; i++)
    {
      if(this.users[i].username === newUser.username) {
        this.alert("A user with same name already exists");
        saveUser = false;
        break;
      }
    }
    if(saveUser)
    {            
      // Remove the initial empty one
      this.users = this.users.filter(function( obj ) {
        return obj.userId !== "new";
      });
      this.spinner.show();
      // Add the new one, update datasource and cancel editing        
      this.restFulAPI.addUser(newUser).subscribe(data => {        
          this.users.push(new UserDTO(data));
          this.sortUsers();                
          this.spinner.hide(); 
          this.cancelEditRow(data, true);                       
        },
        err => {
          this.logger.debug(`createNewUser | ${err}`);
          this.enableAllButtons();
          this.spinner.hide();
        }      
      );  
      
      this.userPreferenceDTO = new UserPreferenceDTO();
      this.userPreferenceDTO.username = newUser.username;
      this.userPreferenceDTO.userPreferenceId = "";
      var userPreferenceObj = {};
      userPreferenceObj['colDescs'] = colDescs;
      userPreferenceObj['showBarges'] = true;
      userPreferenceObj['showDeepsea'] = true;
      userPreferenceObj['showFeeders'] = true;
      userPreferenceObj['lookAhead'] = "7";
      userPreferenceObj['lookBack'] = "5";
      userPreferenceObj['showWorkingVessels'] = true;
      this.userPreferenceDTO.preferences = JSON.stringify(userPreferenceObj);
      this.restFulAPI.modifyUserPreferences(this.userPreferenceDTO).subscribe( res => {
        if(!res){
          this.alert('There is problem in internet connection, please try again');
        }
        
      });
    } 
  }

  modifyExistingUser(currentUser: UserDTO) {
    var modifyObject = true;
    // Check name does not exist or order not repeated, fields not empty...
    for(var i=0; i<= this.users.length -1; i++)
    {      
      if(this.users[i].userId !== currentUser.userId && this.users[i].username.trim().toUpperCase().match((document.getElementById("username input " + currentUser.userId) as HTMLInputElement).value.trim().toUpperCase())) {        
        this.alert("A user with same name already exists");
        modifyObject = false;
        break;
      }     
    }
    if(modifyObject) {
      var modifiedUser = new UserDTO();
      for(i=0; i<= this.users.length -1; i++)
      {
        if(this.users[i].userId === currentUser.userId) {                    
          modifiedUser.userId = currentUser.userId;
          modifiedUser.active = currentUser.active;      
          this.getUserData(currentUser.userId, modifiedUser);
          modifiedUser.active = (document.getElementById("active check " + modifiedUser.userId) as HTMLInputElement).checked;
          // If password is empty... send null values in password to not modify it...
          if ((document.getElementById("password input " + currentUser.userId) as HTMLInputElement).value === '') {
            modifiedUser.password = undefined;
            modifiedUser.repeatPassword = undefined;
          }
          // Modify the user
          this.spinner.show();
          this.restFulAPI.modifyUser(modifiedUser).subscribe(data => {
            this.users[i] = new UserDTO(data);         
            this.sortUsers();               
            this.spinner.hide();
            document.getElementById("mainTableUsers").style.display = "block";
            this.cancelEditRow(data, false);              
          },
          err => {
            this.logger.debug(`modifyExistingUser | ${err}`);
            this.enableAllButtons();  
            this.spinner.hide();
          });      
          break;          
        }
      }
    }
  }

  saveRow(currentUser: UserDTO) {
    if ((document.getElementById("username input " + currentUser.userId) as HTMLInputElement).value === '') {
      this.alert("A user must have a name");
      return;
    }
    if (currentUser.userId === 'new' && (document.getElementById("password input " + currentUser.userId) as HTMLInputElement).value === '') {
      this.alert("A user must contain a password");
      return;
    }
    if (currentUser.userId === 'new' && (document.getElementById("repeatPassword input " + currentUser.userId) as HTMLInputElement).value === '') {
      this.alert("A user must contain a password");
      return;
    }
    if ((<HTMLSelectElement>document.getElementById("profile select field " + currentUser.userId)).value.trim() === '') {
      this.alert("A user must contain a profile");
      return;
    }
    if ((<HTMLSelectElement>document.getElementById("role select field " + currentUser.userId)).value.trim() === '') {
      this.alert("A user must contain a role");
      return;
    }
    if ((document.getElementById("password input " + currentUser.userId) as HTMLInputElement).value !== (document.getElementById("repeatPassword input " + currentUser.userId) as HTMLInputElement).value) {
      this.alert("Passwords do not match");
      return;
    }
    // New record
    if(currentUser.userId === "new") {
      this.createNewUser(currentUser)
    } else {
      this.modifyExistingUser(currentUser)
    } 
  }

  cancelRow(element: UserDTO) {
    if(element.userId === "new") {
      let index = this.users.indexOf(element);
      if (index >= 0) {
        this.users.splice(index, 1);
        this.sortUsers();        
        this.enableAllButtons();
      }
    } else {
      this.cancelEditRow(element, false);
      this.getUsers();      
    }
  }

  cancelEditRow(element: UserDTO, newUSer) {          
    this.enableAllButtons();    
    if(!newUSer) {
      var currentUserId = element.userId;
      document.getElementById("edit buttons " + currentUserId).style.display = "none";
      document.getElementById("main buttons " + currentUserId).style.display = "block";
      // Disable editing row
      document.getElementById("username " + currentUserId).className = 'cellContentReadable';     
      (document.getElementById("username input " + currentUserId) as HTMLInputElement).readOnly = true;    
      document.getElementById("password " + currentUserId).className = 'cellContentReadable'; 
      (document.getElementById("password input " + currentUserId) as HTMLInputElement).readOnly = true;
      document.getElementById("repeatPassword " + currentUserId).className = 'cellContentReadable'; 
      (document.getElementById("repeatPassword input " + currentUserId) as HTMLInputElement).readOnly = true;
      document.getElementById("notificationProfile " + currentUserId).className = 'cellContentReadable';
      document.getElementById("profile select " + currentUserId).style.display = "none";
      document.getElementById("profile value " + currentUserId).style.display = "block";
      document.getElementById("role " + currentUserId).className = 'cellContentReadable';  
      document.getElementById("role select " + currentUserId).style.display = "none";
      document.getElementById("role value " + currentUserId).style.display = "block"; 
      document.getElementById("active " + element.userId).className = 'cellContentReadable'; 
      element.editEnabled = false;      
    }       
  }

  editRow(element: UserDTO) {
    // Disable rest of buttons
    this.disableAllButtons();
    document.getElementById("main buttons " + element.userId).style.display = "none";
    document.getElementById("edit buttons " + element.userId).style.display = "block";
    // Show editing buttons
    (document.getElementById("save " + element.userId) as HTMLButtonElement).disabled = false;
    (document.getElementById("cancel " + element.userId) as HTMLButtonElement).disabled = false;
    // Allow editing row   
    document.getElementById("username " + element.userId).className = 'cellContentEditable'; 
    (document.getElementById("username input " + element.userId) as HTMLInputElement).readOnly = false;
    document.getElementById("password " + element.userId).className = 'cellContentEditable'; 
    (document.getElementById("password input " + element.userId) as HTMLInputElement).readOnly = false;
    (document.getElementById("password input " + element.userId) as HTMLInputElement).value = "";
    document.getElementById("repeatPassword " + element.userId).className = 'cellContentEditable'; 
    (document.getElementById("repeatPassword input " + element.userId) as HTMLInputElement).readOnly = false;
    (document.getElementById("repeatPassword input " + element.userId) as HTMLInputElement).value = "";
    document.getElementById("notificationProfile " + element.userId).className = 'cellContentEditable';  
    document.getElementById("profile select " + element.userId).style.display = "block";
    document.getElementById("profile value " + element.userId).style.display = "none";
    document.getElementById("role " + element.userId).className = 'cellContentEditable';  
    document.getElementById("role select " + element.userId).style.display = "block";
    document.getElementById("role value " + element.userId).style.display = "none"; 
    document.getElementById("active " + element.userId).className = 'cellContentEditable'; 
    element.editEnabled = true;   
  }

  disableAllButtons() {
    disableAllButtons();
  }

  enableAllButtons() {
    enableAllButtons();
  }
}