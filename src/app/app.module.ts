import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { UiSwitchModule } from 'ngx-ui-switch';
import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routing';
import { AuthenticationService } from './shared/services/authentication.service';
import { VesselVisitListComponent } from './components/dashboard/vessel-visit-list/vessel-visit-list.component';
import { IndexWorkflowDetailsComponent } from './components/detailsView/index-workflow-details/index-workflow-details.component';
import { SlaMetricsDetailsComponent } from './components/detailsView/sla-metrics-details/sla-metrics-details.component';
import { StagesWorkflowDetailsComponent } from './components/detailsView/stages-workflow-details/stages-workflow-details.component';
import { StatusMovesWorkflowDetailsComponent } from './components/detailsView/status-moves-workflow-details/status-moves-workflow-details.component';
import { SubstagesWorkflowDetailsComponent } from './components/detailsView/substages-workflow-details/substages-workflow-details.component';
import { VesselVisitWorkflowDetailsComponent } from './components/detailsView/vessel-visit-workflow-details/vessel-visit-workflow-details.component';
import { LoginComponent } from './components/login/login/login.component';
import { Interceptor } from './shared/security/interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { MaterialModule } from './material.module';
import { MatChipsModule } from '@angular/material/chips';
import { NavbarComponent } from './components/navigation/navbar/navbar.component';
import { NotificationsComponent } from './components/notifications/notifications/notifications.component';
import { ApplicationSettingsComponent } from './components/settings/application-settings/application-settings.component';
import { NotificationsSettingsComponent } from './components/settings/notifications-settings/notifications-settings.component';
import { SlaSettingsComponent } from './components/settings/sla-settings/sla-settings.component';
import { UserSettingsComponent } from './components/settings/user-settings/user-settings.component';
import { PushNotificationsService } from './shared/services/push-notification.service';
import { AuthGuard } from './shared/security/authguard';
import { AdminGuard } from './shared/security/adminguard';
import { HttpErrorHandler } from './shared/services/http-error-handler.service';
import { MessageService } from './shared/services/message.service';
import { Globals } from './global/globals';
import { WebSocketService } from './shared/services/websocket.service';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { Ng5SliderModule } from 'ng5-slider';
import { NgxPaginationModule } from 'ngx-pagination';
import { WorkflowListComponent } from './components/settings/workflow-settings/workflow-list/workflow-list.component';
import { WorkflowConfigurationComponent } from './components/settings/workflow-settings/workflow-configuration/workflow-configuration.component';
import { WorkflowStepsComponent } from './components/settings/workflow-settings/workflow-steps/workflow-steps.component';
import { IndexWorkflowSettingsComponent } from './components/settings/workflow-settings/index-workflow-settings/index-workflow-settings.component';
import { UserSearchPresistanceService } from './shared/services/user-search-presistance.service';
import { NGXLogger } from 'ngx-logger';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { environment } from '../environments/environment';
import {ErrorPageComponent} from '../app/components/notifications/error-page/error-page.component'
import { GoogleAnalyticsService } from './shared/services/analytics.service';

@NgModule({
  declarations: [
    AppComponent,
    VesselVisitListComponent,
    IndexWorkflowDetailsComponent,
    SlaMetricsDetailsComponent,
    StagesWorkflowDetailsComponent,
    StatusMovesWorkflowDetailsComponent,
    SubstagesWorkflowDetailsComponent,
    VesselVisitWorkflowDetailsComponent,
    LoginComponent,
    NavbarComponent,
    NotificationsComponent,
    NotificationsSettingsComponent,
    SlaSettingsComponent,
    UserSettingsComponent,    
    WorkflowListComponent,
    WorkflowConfigurationComponent,
    WorkflowStepsComponent,
    ApplicationSettingsComponent,  
    IndexWorkflowSettingsComponent, 
    ErrorPageComponent
  ],
  imports: [
    NgxPaginationModule,
    DragDropModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    MaterialModule,
    Ng5SliderModule,
    MatChipsModule,
    RouterModule.forRoot(appRoutes),
    AngularFontAwesomeModule,
    NgCircleProgressModule.forRoot({
      animationDuration: 0
    }),
    NgxSpinnerModule,
    LoggerModule.forRoot({
      level: environment.debug ? NgxLoggerLevel.LOG : NgxLoggerLevel.OFF}),
    UiSwitchModule,
    NgSelectModule    
  ],
 
  providers: [
    Globals,
    PushNotificationsService,
    AuthenticationService,
    AuthGuard,
    AdminGuard,
    HttpErrorHandler,
    MessageService,
    WebSocketService,
    GoogleAnalyticsService,
    UserSearchPresistanceService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },     
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },       
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(protected _googleAnalyticsService: GoogleAnalyticsService) { } //this keeps the service online the whole time 
}
