import { LoginComponent } from './components/login/login/login.component';
import { Routes } from '@angular/router';
import { AuthGuard } from './shared/security/authguard';
import { AdminGuard } from './shared/security/adminguard';
import { NotificationsComponent } from './components/notifications/notifications/notifications.component';
import { VesselVisitListComponent } from './components/dashboard/vessel-visit-list/vessel-visit-list.component';
import { IndexWorkflowSettingsComponent } from './components/settings/workflow-settings/index-workflow-settings/index-workflow-settings.component';
import { SlaSettingsComponent } from './components/settings/sla-settings/sla-settings.component';
import { NotificationsSettingsComponent } from './components/settings/notifications-settings/notifications-settings.component';
import { ApplicationSettingsComponent } from './components/settings/application-settings/application-settings.component';
import { IndexWorkflowDetailsComponent } from './components/detailsView/index-workflow-details/index-workflow-details.component';
import { UserSettingsComponent } from './components/settings/user-settings/user-settings.component';
import { ErrorPageComponent } from './components/notifications/error-page/error-page.component';

export const appRoutes: Routes = [
  {
    path: '',
    component: LoginComponent
  },  
  {
    path: 'login',
    component: LoginComponent
  },  
  {
    path: 'error-page',  
    component: ErrorPageComponent
  },
  // Settings MENU
  {
    path: '',
    canActivate: [AdminGuard],
    children: [
      { path: 'index-workflow-settings', canActivate: [AdminGuard], component: IndexWorkflowSettingsComponent },
      { path: 'sla-settings', canActivate: [AdminGuard], component: SlaSettingsComponent },
      { path: 'notifications-settings', canActivate: [AdminGuard], component: NotificationsSettingsComponent },
      { path: 'user-settings', canActivate: [AdminGuard], component: UserSettingsComponent },
      { path: 'application-settings', canActivate: [AdminGuard], component: ApplicationSettingsComponent }
    ]
  },
  // Management MENU
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      { path: 'dashboard-management', canActivate: [AuthGuard], component: VesselVisitListComponent },
      { path: 'details-management/:id', canActivate: [AuthGuard], component: IndexWorkflowDetailsComponent },
      { path: 'notifications-management', canActivate: [AuthGuard], component: NotificationsComponent }
    ]
  }
];
