import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './shared/services/authentication.service';
import { Router } from '../../node_modules/@angular/router';
import { Observable } from '../../node_modules/rxjs';
import { GoogleAnalyticsService } from './shared/services/analytics.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'e2e-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  public showContentView: boolean;
  public loading: boolean;
  public isAuthorized: boolean;
  public sessionId: string;
  public errorCode: number;

  private isLoggedIn$: Observable<boolean>;
  private userLoggedIn: string;

  constructor(private auth: AuthenticationService, public router: Router, private googleAnalyticsService: GoogleAnalyticsService) {

    this.loading = true;
    this.isAuthorized = false;
    this.auth.loggedInObvbl$.subscribe(
      (data: boolean) => {
        this.showContentView = data;
        this.isAuthorized = data;
        if (this.isAuthorized) {
          this.loading = false;
        }
      });
      this.auth.usernameObvbl$.subscribe(data => {
        this.userLoggedIn = data;
      });
      this.appendGaTrackingCode()

  }

  private appendGaTrackingCode() {
    try {
      const script = document.createElement('script');
      script.innerHTML = `
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', '` + environment.gaUserId + `', 'auto');
      `;
      document.head.appendChild(script);
    } catch (ex) {
     console.error('Error appending google analytics');
     console.error(ex);
    }
  }

  public ngOnInit(): void {
  }
}
