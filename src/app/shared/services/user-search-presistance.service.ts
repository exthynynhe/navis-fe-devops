import { Injectable } from '@angular/core';
import { SearchItem } from 'src/app/model/SearchItem';

@Injectable({
  providedIn: 'root'
})
export class UserSearchPresistanceService {
  public advancedSearchItems: Array<SearchItem> =  [];
  constructor() { }

  initialiseSearchItems(items: Array<SearchItem>){
    if(this.advancedSearchItems.length == 0){
    this.advancedSearchItems = [...items];
    }
  }
  updateSearchItems(items: Array<SearchItem>){
    this.advancedSearchItems = [...items];
  }
  getSearchItems(){
    return this.advancedSearchItems;
  }

  clearSearchItems(){
    this.advancedSearchItems = [];
  }
}
