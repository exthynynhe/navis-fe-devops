import { VesselVisitDTO } from '../../model/VesselVisitDTO';
import { StageDTO } from '../../model/StageDTO';
import { SearchItem } from '../../model/SearchItem';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpErrorHandler, HandleError } from './http-error-handler.service';
import { Observable } from 'rxjs/Observable';

// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { SlaSettingDTO } from '../../model/SlaSettingDTO';
import { NoteDTO } from '../../model/NoteDTO';
import { VesselNoteDTO } from '../../model/VesselNoteDTO';
import { PlanningNoteDTO } from '../../model/PlanningNoteDTO';
import { KpiDTO } from '../../model/KpiDTO';
import { UnitDTO } from '../../model/UnitDTO';
import { UserDTO } from '../../model/UserDTO';
import { AppSettingDTO } from '../../model/AppSettingDTO';
import { CarrierServiceDTO } from '../../model/CarrierServiceDTO';
import { LineOperatorDTO } from '../../model/LineOperatorDTO';
import { FacilityDTO } from '../../model/FacilityDTO';
import { WorkflowTmpDTO } from '../../model/WorkflowTmpDTO';
import { UserPreferenceDTO } from 'src/app/model/UserPreferenceDTO';
import { NGXLogger } from "ngx-logger";

@Injectable({
  providedIn: 'root'
})
export class RestFulAPIService {

  private restFulAPIurl = environment.apiEndpoint + 'api';
  
  private handleError: HandleError;

  constructor(private logger: NGXLogger, private httpClient: HttpClient,
    httpErrorHandler: HttpErrorHandler) { 
      this.handleError = httpErrorHandler.createHandleError('CompassService');
    }

  // ######################## SYNC ######################## 
  syncE2EwithN4() : Observable<Array<VesselVisitDTO>> {
    try {             
      return this.httpClient.get<Array<VesselVisitDTO>>(`${this.restFulAPIurl}/UQ`);      
    } catch (Error) {
      this.logger.debug('getVesselUnits > ' + Error.message());            
    }
  }

  // ######################## UNITS ######################## 
  getVesselVisitUnits(vesselVisit: VesselVisitDTO): Observable<Array<UnitDTO>> {
    try {                 
      return this.httpClient.get<Array<UnitDTO>>(`${this.restFulAPIurl}/VesselVisit/${vesselVisit.vesselVisitName}/Units`);      
    } catch (Error) {
      this.logger.debug('getVesselUnits > ' + Error.message());
      return null;
    }
  }

  // ######################## USERS ########################
  getUsers(): Observable<Array<UserDTO>> {
    try {            
      return this.httpClient.get<Array<UserDTO>>(`${this.restFulAPIurl}/Users`);      
    } catch (Error) {
      this.logger.debug('getUsers > ' + Error.message());
      return null;
    }
  }

  addUser(newUser: UserDTO):Observable<UserDTO> {
    return this.httpClient.post<UserDTO>(`${this.restFulAPIurl}/User`, newUser)
      .pipe(
        catchError(this.handleError('addUser', newUser))
      );    
  }

  modifyUser(user: UserDTO):Observable<UserDTO> {
    return this.httpClient.put<UserDTO>(`${this.restFulAPIurl}/User`, user)
      .pipe(
        catchError(this.handleError('modifyUser', user))
      );
  }
  
  deleteUser (user: UserDTO): Observable<{}> {    
    return this.httpClient.put<UserDTO>(`${this.restFulAPIurl}/User`, user)
      .pipe(
        catchError(this.handleError('deleteUser'))
      );
  }

  // ######################## APPLICATION SETTINGS ########################
  getApplicationSettings(): Observable<AppSettingDTO> {
    try {   
      return this.httpClient.get<AppSettingDTO>(`${this.restFulAPIurl}/ApplicationSetting`);      
    } catch (Error) {
      this.logger.debug('getApplicationSettings > ' + Error.message());
      return null;
    }
  }

  modifyApplicationSettings(applicationSettings: AppSettingDTO):Observable<AppSettingDTO> {
    return this.httpClient.put<AppSettingDTO>(`${this.restFulAPIurl}/ApplicationSetting`, applicationSettings)
      .pipe(
        catchError(this.handleError('modifyApplicationSettings', applicationSettings))
      );
  } 
  
   // ######################## USER PREFERENCES ########################
  getUserPreferences(username: string): Observable<UserPreferenceDTO>{
    try{
      return this.httpClient.get<UserPreferenceDTO>(`${this.restFulAPIurl}/UserPreferences/UserPreference/${username}`);
    }
    catch(Error){
      this.logger.debug('getUserPreferences() | '+ Error.message());
      return null;
    }
  }

  modifyUserPreferences(userPreferences: UserPreferenceDTO): Observable<UserPreferenceDTO>{      
      return this.httpClient.put<UserPreferenceDTO>(`${this.restFulAPIurl}/UserPreferences/UserPreference`, userPreferences)
      .pipe(
        catchError(this.handleError('modifyUserPreferences', userPreferences))
      );
  }
  // ######################## SLA - VESSEL VISIT KPIS ########################
  getVesselVisitKpis(vesselVisit: VesselVisitDTO): Observable<Array<KpiDTO>> {
    try {   
      return this.httpClient.get<Array<KpiDTO>>(`${this.restFulAPIurl}/VesselVisit/${vesselVisit.vesselVisitName}/Kpis`);      
    } catch (Error) {
      this.logger.debug('getTerminalNotes > ' + Error.message());
      return null;
    }
  }

  modifyVesselVisitKpi(vesselVisit: VesselVisitDTO, kpiDto: KpiDTO):Observable<KpiDTO> {
    return this.httpClient.put<KpiDTO>(`${this.restFulAPIurl}/VesselVisit/${vesselVisit.vesselVisitName}/Kpi`, kpiDto)
      .pipe(
        catchError(this.handleError('modifyTerminalNote', kpiDto))
      );
  } 

  // ######################## TERMINAL NOTES ########################
  getTerminalNotes(): Observable<Array<NoteDTO>> {
    try {          
      return this.httpClient.get<Array<NoteDTO>>(`${this.restFulAPIurl}/TerminalNotes`);      
    } catch (Error) {
      this.logger.debug('getTerminalNotes > ' + Error.message());
      return null;
    }
  }

  addTerminalNote(terminalNote: NoteDTO):Observable<NoteDTO> {
    return this.httpClient.post<NoteDTO>(`${this.restFulAPIurl}/TerminalNote`, terminalNote)
      .pipe(
        catchError(this.handleError('addTerminalNote', terminalNote))
      );    
  }

  deleteTerminalNote(terminalNote: NoteDTO): Observable<{}> {    
    return this.httpClient.put<NoteDTO>(`${this.restFulAPIurl}/TerminalNote`, terminalNote)
      .pipe(
        catchError(this.handleError('deleteTerminalNote', terminalNote))
      );
  }

  modifyTerminalNote(terminalNote: NoteDTO):Observable<NoteDTO> {
    return this.httpClient.put<NoteDTO>(`${this.restFulAPIurl}/TerminalNote`, terminalNote)
      .pipe(
        catchError(this.handleError('modifyTerminalNote', terminalNote))
      );
  } 

// ######################## VESSEL NOTES ########################
getVesselNotes(vesselName: String): Observable<Array<VesselNoteDTO>> {
  try {          
    return this.httpClient.get<Array<VesselNoteDTO>>(`${this.restFulAPIurl}/VesselNotes/${vesselName}`);      
  } catch (Error) {
    this.logger.debug('getVesselNotes > ' + Error.message());
    return null;
  }
}

addVesselNote(vesselNote: VesselNoteDTO):Observable<VesselNoteDTO> {
  return this.httpClient.post<VesselNoteDTO>(`${this.restFulAPIurl}/VesselNote`, vesselNote)
    .pipe(
      catchError(this.handleError('addVesselNote', vesselNote))
    );    
}

deleteVesselNote(vesselNote: VesselNoteDTO): Observable<{}> {        
  return this.httpClient.put<VesselNoteDTO>(`${this.restFulAPIurl}/VesselNote`, vesselNote)
    .pipe(
      catchError(this.handleError('modifyVesselNote', vesselNote))
    );
}

modifyVesselNote(vesselNote: VesselNoteDTO):Observable<VesselNoteDTO> {
  return this.httpClient.put<VesselNoteDTO>(`${this.restFulAPIurl}/VesselNote`, vesselNote)
    .pipe(
      catchError(this.handleError('modifyVesselNote', vesselNote))
    );
} 


  // ######################## VESSEL VISIT NOTES ########################
  getVesselVisitNotes(vesselVisitName: String): Observable<Array<PlanningNoteDTO>> {
    try {
      return this.httpClient.get<Array<PlanningNoteDTO>>(`${this.restFulAPIurl}/VesselVisitNotes/${vesselVisitName}`);      
    } catch (Error) {
      this.logger.debug('getVesselVisitNotes > ' + Error.message());
      return null;
    }
  }

  addVesselVisitNote(vvNote: PlanningNoteDTO):Observable<PlanningNoteDTO> {
    return this.httpClient.post<PlanningNoteDTO>(`${this.restFulAPIurl}/VesselVisitNote`, vvNote)
      .pipe(
        catchError(this.handleError('addVesselVisitNote', vvNote))
      );    
  }

  modifyVesselVisitNote(vvNote: PlanningNoteDTO):Observable<PlanningNoteDTO> {
    return this.httpClient.put<PlanningNoteDTO>(`${this.restFulAPIurl}/VesselVisitNote`, vvNote)
      .pipe(
        catchError(this.handleError('modifyVesselVisitNote', vvNote))
      );
  } 


  // ######################## STAGES ########################
  getStages(): Observable<Array<StageDTO>> {
    try {    
      return this.httpClient.get<Array<StageDTO>>(`${this.restFulAPIurl}/StagesTemplate`);      
    } catch (Error) {
      this.logger.debug('getStages> ' + Error.message());
      return null;
    }
  }

  addStage(stage: StageDTO):Observable<StageDTO> {
    return this.httpClient.post<StageDTO>(`${this.restFulAPIurl}/StagesTemplate/Stage`, stage)
    .pipe(
      catchError(this.handleError('addStage', stage))
    );    
  }

  deleteStage (stage: StageDTO): Observable<{}> {    
    return this.httpClient.delete(`${this.restFulAPIurl}/StagesTemplate/Stage/${stage.stageTemplateId}`)
      .pipe(
        catchError(this.handleError('deleteStage'))
      );
  }

  deleteChildrenStage (stage: StageDTO, childrenStage: StageDTO): Observable<{}> {  
    return this.httpClient.delete(`${this.restFulAPIurl}/StagesTemplate/Stage/${stage.stageTemplateId}/ChildStage/${childrenStage.stageTemplateId}`)
      .pipe(
        catchError(this.handleError('deleteChildrenStage'))
      );
  }

  modifyStage(stage: StageDTO):Observable<StageDTO> {
    return this.httpClient.put<StageDTO>(`${this.restFulAPIurl}/StagesTemplate/Stage`, stage)
    .pipe(
      catchError(this.handleError('modifyStage', stage))
    );
  }

  // ######################## VESSEL VISIT STAGES ########################
  modifyVesselVisitStage(stage: StageDTO, vesselVisit: VesselVisitDTO):Observable<StageDTO> {
    return this.httpClient.post<StageDTO>(`${this.restFulAPIurl}/${vesselVisit.vesselVisitId}/Stage`, stage)
    .pipe(
      catchError(this.handleError('modifyVesselVisitStage', stage))
    );
  }

  modifyVVStage(stage: StageDTO, vesselVisit: VesselVisitDTO):Observable<StageDTO> {    
    this.logger.debug(JSON.stringify(stage));
    this.logger.debug(JSON.stringify(vesselVisit));
    this.logger.debug(`${this.restFulAPIurl}/VesselVisit/${vesselVisit.vesselVisitName}/Stage`);
    return this.httpClient.put<StageDTO>(`${this.restFulAPIurl}/VesselVisit/${vesselVisit.vesselVisitName}/Stage`, stage)
    .pipe(
      catchError(this.handleError('modifyVVStage', stage))
    );
  }

  modifyVVStageChild(childStage: StageDTO, stage: StageDTO, vesselVisit: VesselVisitDTO):Observable<StageDTO> {    
    return this.httpClient.put<StageDTO>(`${this.restFulAPIurl}/VesselVisit/${vesselVisit.vesselVisitName}/Stage/${stage.name}/ChildStage`, childStage)
    .pipe(
      catchError(this.handleError('modifyVVStageChild', stage))
    );
  }

  closeVVStages(childStages: Array<StageDTO>, stage: StageDTO, vesselVisit: VesselVisitDTO):Observable<Array<StageDTO>> {
    return this.httpClient.put<Array<StageDTO>>(`${this.restFulAPIurl}/VesselVisit/${vesselVisit.vesselVisitName}/Stage/${stage.name}/ChildStages`, childStages)
    .pipe(
      catchError(this.handleError('closeVVStages', childStages))
    );
  }

  // ######################## SLA SETTINGS ########################
  getSlaSettings(): Observable<Array<SlaSettingDTO>> {
    try {          
      return this.httpClient.get<Array<SlaSettingDTO>>(`${this.restFulAPIurl}/Slas`);      
    } catch (Error) {
      this.logger.debug('getSlaSettings > ' + Error.message());
      return null;
    }
  }

  addSlaSetting(slaSetting: SlaSettingDTO):Observable<SlaSettingDTO> {
    this.logger.debug(JSON.stringify(slaSetting));
    this.logger.debug(`${this.restFulAPIurl}/Sla`);
    return this.httpClient.post<SlaSettingDTO>(`${this.restFulAPIurl}/Sla`, slaSetting)
    .pipe(
      catchError(this.handleError('addSlaSetting', slaSetting))
    );    
  }

  deleteSlaSetting(slaSetting: SlaSettingDTO): Observable<{}> {    
    return this.httpClient.delete(`${this.restFulAPIurl}/Sla/${slaSetting.slaId}`)
      .pipe(
        catchError(this.handleError('deleteSlaSetting'))
      );
  }

  modifySlaSetting(slaSetting: SlaSettingDTO):Observable<SlaSettingDTO> {
    this.logger.debug(JSON.stringify(slaSetting));
    this.logger.debug(`${this.restFulAPIurl}/Sla`);
    return this.httpClient.put<SlaSettingDTO>(`${this.restFulAPIurl}/Sla`, slaSetting)
    .pipe(
      catchError(this.handleError('modifySlaSetting', slaSetting))
    );
  }

  // ######################## VESSEL VISITS ########################
  getVesselVisits(): Observable<Array<VesselVisitDTO>> {
    try {         
      return this.httpClient.get<Array<VesselVisitDTO>>(`${this.restFulAPIurl}/VesselVisitsSlim`);
    } catch (Error) {
      this.logger.debug('getVesselVisits> ' + Error.message());
      return null;
    }
  }

  getVesselVisit(id: any): Observable<VesselVisitDTO> {
    try {
      const vv =  this.httpClient.get<VesselVisitDTO>(`${this.restFulAPIurl}/VesselVisit/${id}`);
      let aa;
      vv.map(m => aa = m);
      return vv;
    } catch (Error) {
      this.logger.debug('getVesselVisit> ' + Error.message());
      return null;
    }
  }

  getVesselVisitsbyFilter(searchItems: Array<SearchItem>): Observable<Array<VesselVisitDTO>> {
    try {
      var finalQuery = '';
      for(var i=0; i<searchItems.length; i++) {
        if(searchItems[i].value !== '') {
          finalQuery += searchItems[i].attributeName + searchItems[i].comparisonChar + searchItems[i].value.toUpperCase();
          finalQuery += ',';
        }
      }
      if(finalQuery === '') {
        return;
      }      
      finalQuery = finalQuery.substring(0, finalQuery.length - 1);
      this.logger.debug('Archive search: ' + finalQuery);       
      this.logger.debug(`RestFulAPIService-> ${this.restFulAPIurl}/VesselVisits`);
      return this.httpClient.get<Array<VesselVisitDTO>>(`${this.restFulAPIurl}/VesselVisit?search=${finalQuery}`);
    } catch (Error) {
      this.logger.debug('getVesselVisits> ' + Error.message());
      return null;
    }
  }

  // ###################################### WORKFLOW LIST #######################################
  getWorkflowTemplates(): Observable<Array<WorkflowTmpDTO>> {
    try {        
      return this.httpClient.get<Array<WorkflowTmpDTO>>(`${this.restFulAPIurl}/WorkflowTemplates`);
    } catch (Error) {
      this.logger.debug('getWorkflowTmps> ' + Error.message());
      return null;
    }
  }

  addWorkflowTemplate(workflowTemplate: WorkflowTmpDTO):Observable<WorkflowTmpDTO> {    
    return this.httpClient.post<WorkflowTmpDTO>(`${this.restFulAPIurl}/WorkflowTemplates/WorkflowTemplate`, workflowTemplate)
    .pipe(
      catchError(this.handleError('addWorkflowTemplate>>', workflowTemplate))
    );    
  }

  cloneWorkflowTemplate(workflowTmpId: number):Observable<WorkflowTmpDTO> {    
    return this.httpClient.post<WorkflowTmpDTO>(`${this.restFulAPIurl}/WorkflowTemplates/WorkflowTemplate/${workflowTmpId}`, null)
    .pipe(
      catchError(this.handleError('addWorkflowTemplate>>', null))
    ); 
  }

  modifyWorkflowTemplates(workflowTemplates: WorkflowTmpDTO):Observable<WorkflowTmpDTO> {
    return this.httpClient.put<WorkflowTmpDTO>(`${this.restFulAPIurl}/WorkflowTemplates/WorkflowTemplate`, workflowTemplates)
    .pipe(
      catchError(this.handleError('modifyWorkflowTemplates', workflowTemplates))
    );
  }

  // ###################################### CARRIER - SERVICES - LINE OPERATORS #######################################
  getFacilities(): Observable<Array<FacilityDTO>> {
    try {         
      return this.httpClient.get<Array<FacilityDTO>>(`${this.restFulAPIurl}/Facilities`);
    } catch (Error) {
      this.logger.debug('getFacilities> ' + Error.message());
      return null;
    }
  }

  getCarrierServices(): Observable<Array<CarrierServiceDTO>> {
    try {         
      return this.httpClient.get<Array<CarrierServiceDTO>>(`${this.restFulAPIurl}/CarrierServices`);
    } catch (Error) {
      this.logger.debug('getCarrierServices> ' + Error.message());
      return null;
    }
  }

  getLineOperators(): Observable<Array<LineOperatorDTO>> {
    try {         
      return this.httpClient.get<Array<LineOperatorDTO>>(`${this.restFulAPIurl}/LineOperators`);
    } catch (Error) {
      this.logger.debug('getLineOperators> ' + Error.message());
      return null;
    }
  }

  
  // ###################################### WORKFLOW - STAGES #######################################
  removeExistingStage(workflow: WorkflowTmpDTO, stage: StageDTO): Observable<WorkflowTmpDTO>{
    try {      
      return this.httpClient.request<WorkflowTmpDTO>('DELETE', `${this.restFulAPIurl}/WorkflowTemplates/WorkflowTemplate/${workflow.workflowTmpId}/StageTemplate/${stage.stageTemplateId}`, {body: workflow});      
    } catch (Error) {
      this.logger.debug('removeExistingStage > ' + Error.message());
      return null;
    }
  }

  modifyExistingStage(workflow: WorkflowTmpDTO, stage: StageDTO): Observable<WorkflowTmpDTO>{
    try {      
      return this.httpClient.put<WorkflowTmpDTO>(`${this.restFulAPIurl}/WorkflowTemplates/WorkflowTemplate/${workflow.workflowTmpId}/StageTemplate/${stage.stageTemplateId}`, workflow);      
    } catch (Error) {
      this.logger.debug('modifyExistingStage > ' + Error.message());
      return null;
    }
  }

  addNewStage(workflow: WorkflowTmpDTO, stage: StageDTO): Observable<WorkflowTmpDTO>{
    try {      
      return this.httpClient.post<WorkflowTmpDTO>(`${this.restFulAPIurl}/WorkflowTemplates/WorkflowTemplate/${workflow.workflowTmpId}/StageTemplate/${stage.stageTemplateId}`, workflow);      
    } catch (Error) {
      this.logger.debug('addNewStage > ' + Error.message());
      return null;
    }
  }
}