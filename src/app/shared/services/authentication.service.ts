import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { of, throwError } from "rxjs";

import { JwtHelperService } from '@auth0/angular-jwt';
import * as jwt_decode from 'jwt-decode';
import { Subject } from '../../../../node_modules/rxjs';
import { HttpErrorHandler, HandleError } from './http-error-handler.service';
import { LOGIN_TYPE } from '../../global/utils';
import { environment } from '../../../environments/environment';
import { UserSearchPresistanceService } from './user-search-presistance.service';

@Injectable()
export class AuthenticationService {
    private authUrl = environment.apiEndpoint + 'auth';
    private n4AutUrl = environment.apiEndpoint + 'tcs';
    private notifyUser = new Subject<any>();
    private notifyLoggedIn = new Subject<boolean>();
    private handleError: HandleError;

    usernameObvbl$ =  this.notifyUser.asObservable();
    loggedInObvbl$ = this.notifyLoggedIn.asObservable();
    private loggedUser: string;    

    // private loggedIn = new BehaviorSubject<boolean>(false); // {1}
    constructor(private http: HttpClient, private router: Router, httpErrorHandler: HttpErrorHandler, private userSearchPersistance: UserSearchPresistanceService) {
        this.handleError = httpErrorHandler.createHandleError('CompassAuthentication');
    }    

    public isAuthenticated(): boolean {
        const token = localStorage.getItem('token');
        const jwtHelper: JwtHelperService = new JwtHelperService();
        // Check whether the token is expired and return
        // true or false
        return !jwtHelper.isTokenExpired(token);
      }
      
    public isAdmin(): boolean {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));

        if(currentUser) {
            var token = currentUser.token; // your token
            const decoded = jwt_decode(token); 

            return decoded.roles === "ADMIN";
        }
    }

    public isManager(): boolean {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser) {
            var token = currentUser.token; // your token
            const decoded = jwt_decode(token); 
            return decoded.roles === "MANAGER";
        }
    }

    public getUserNotificationProfile(): string {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser) {
            var token = currentUser.token; // your token
            const decoded = jwt_decode(token);                        
            return decoded.notificationProfile;
        }
    }

    n4login(n4token: string): Observable<LOGIN_TYPE> {
        return this.http.post(this.n4AutUrl, JSON.stringify({token: n4token})).pipe(
            map((response: Response) => {              
                var token = null;
                if(response) {
                    token = response['token'];
                }
                if (token) {                    
                    const decoded = jwt_decode(token);
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    // localStorage.setItem('currentUser', decoded.sub);
                    localStorage.setItem('token', token);                    
                    // localStorage.setItem('roles', decoded.roles);
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: decoded.sub, token: token }));
                    // return true to indicate successful login
                    this.loggedUser = decoded.sub;
                    this.notifyUser.next(decoded.sub);
                    this.notifyLoggedIn.next(true);
                    return LOGIN_TYPE.LOGGED;
                } else {
                    // return false to indicate failed login
                    this.notifyLoggedIn.next(false);                    
                    return LOGIN_TYPE.E_CREDENTIALS;
                }
            }),
            catchError(this.handleError('CompassLoginError', LOGIN_TYPE.E_CONNECTION))
        );
    }

    login(username: string, password: string): Observable<LOGIN_TYPE> {        
        return this.http.post(this.authUrl, JSON.stringify({username: username, password: password})).pipe(
            map((response: Response) => {
                console.log("here", response);          
                var token = null;
                if(response) {
                    token = response['token'];
                }
                if (token) {                    
                    const decoded = jwt_decode(token);
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    // localStorage.setItem('currentUser', decoded.sub);
                    localStorage.setItem('token', token);                    
                    // localStorage.setItem('roles', decoded.roles);
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));
                    // return true to indicate successful login
                    this.loggedUser = username;
     
                    this.notifyUser.next(username);
                    this.notifyLoggedIn.next(true);
                    return LOGIN_TYPE.LOGGED;
                } else {
                    // return false to indicate failed login
                    this.notifyLoggedIn.next(false);                    
                    return LOGIN_TYPE.E_CREDENTIALS;
                }
            }),
            catchError(err => {
                if (err.status === 400) {
                    return of(LOGIN_TYPE.E_CREDENTIALS);
                }

                else{
                    return of(LOGIN_TYPE.E_CONNECTION);
                }
            })
        );
    }

    getToken(): String {
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser) {
            const token = currentUser && currentUser.token;
            return token ? token : '';
        }
    }
    isLoggedIn() {
        const token: String = this.getToken();
        const auth: boolean = token && token.length > 0;
        this.notifyLoggedIn.next(true);
        return auth;
    }

    getUserName() {
    const token: String = this.getToken();
    const auth: boolean = token && token.length > 0;
    if (auth) {
        const decoded = jwt_decode(token);
        this.notifyUser.next(decoded.sub);
    }
    }

    logout(): Observable<boolean> {
        // clear token remove user from local storage to log user out
        this.userSearchPersistance.clearSearchItems();
        localStorage.removeItem('currentUser');
        localStorage.removeItem('paramsInSession');
        this.router.navigate(['login']);
        this.notifyLoggedIn.next(false);        
        return of(true);
    }
}
