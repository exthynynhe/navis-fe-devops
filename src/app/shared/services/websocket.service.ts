import { Injectable } from '@angular/core';
import { VesselVisitDTO } from '../../model/VesselVisitDTO';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../../environments/environment';
import * as Stomp from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import { NotificationDTO } from '../../model/NotificationDTO';
import { PushNotificationsService } from './push-notification.service';
import { NoteDTO } from '../../model/NoteDTO';
import { VesselNoteDTO } from '../../model/VesselNoteDTO';
import { PlanningNoteDTO } from '../../model/PlanningNoteDTO';
import { KpiDTO } from '../../model/KpiDTO';
import { AuthenticationService } from './../../shared/services/authentication.service';
import { UnitsRecapDTO } from '../../model/UnitsRecapDTO';

@Injectable({
    providedIn: 'root'
})

export class WebSocketService {

    private notifyVesselVisitKpi = new Subject<KpiDTO>();
    private notifyVesselVisitList = new Subject<VesselVisitDTO>();
    private notifyVesselVisitNoteList = new Subject<PlanningNoteDTO>();
    private notifyVesselNoteList = new Subject<VesselNoteDTO>();
    private notifyTerminalNoteList = new Subject<NoteDTO>();
    private notifyUnitsRecap = new Subject<UnitsRecapDTO>();
    private stompClient;
    private socket;

    disabled = true;
    name: string;    
    vvkpiDTPListObvbl$ = this.notifyVesselVisitKpi.asObservable();
    vvDTOListObvbl$ = this.notifyVesselVisitList.asObservable();
    vnDTOListObvbl$ = this.notifyVesselNoteList.asObservable();
    vvnDTOListObvbl$ = this.notifyVesselVisitNoteList.asObservable();
    tnDTOListObvbl$ = this.notifyTerminalNoteList.asObservable();
    unrcpDTOListObvbl$ = this.notifyUnitsRecap.asObservable();

    constructor(private _notificationService: PushNotificationsService, private authenticationService: AuthenticationService) { }

    setConnected(connected: boolean) {
        this.disabled = !connected;
        if (connected) {
        }
    }

    connect() {                
        var conn = localStorage.getItem('webSocketConnected');        
        if(this.stompClient === undefined || (conn !== null && conn !== "true")) {
            this.connectWebSocket();
        }
    }

    connectWebSocket() {
        this.socket = new SockJS(environment.apiEndpoint + 'websocket');
        this.stompClient = Stomp.over(this.socket);
        if(!environment.debug){
        this.stompClient.debug = null;
        }
        this.stompClient.heartbeat.outgoing = 4000;
        this.stompClient.heartbeat.incoming = 4000;
        this.stompClient.reconnect_delay = 4000;
        const _this = this;
        this.stompClient.connect({}, (frame) => {     
            _this.setConnected(true);
            localStorage.setItem('disconnectionAlerted', 'false');
            if(environment.debug){
            console.log('###### INFO > WebSocket (re)connected to Compass backend');
            }
            // Save status in local storage
            localStorage.setItem('webSocketAlive', "C");            
            // Vessel Visits              
            _this.stompClient.subscribe('/topic/', function (msg) {
                _this.processIncomingMessage(JSON.parse(msg.body), msg.headers.messageType);
            });          
        }, function(message) {
            _this.disconnect();
            if(environment.debug){
            console.log("###### FATAL > WebSocket disconnected from Compass backend");
            console.log(message);
            }
            setTimeout(() => {
                _this.connectWebSocket();
                if(environment.debug){
                console.log("Trying to reconnect...");
                }
            }, 5000);
        });
        this._notificationService.requestPermission();
        localStorage.setItem('webSocketConnected', "true");
        if(environment.debug){
        console.log('###### INFO > Connecting websockets... ');  
        console.log(environment.apiEndpoint);
        }
        this.socket.onclose = function() {
            if(environment.debug){
            console.log('Closing the WebSocket');
            }
            _this.disconnect();
            _this.connectWebSocket();
        }                                        
    }

    disconnect() {
        if (this.stompClient != null) {
            this.stompClient.disconnect();
        }
        this.setConnected(false);
        localStorage.removeItem('webSocketConnected');        
        if(environment.debug){
        console.log('Disconnecting!');
        }
        localStorage.removeItem('webSocketAlive'); 
    }

    sendMessage() {
        this.stompClient.send('/gkz/hello',{},JSON.stringify({ 'name': this.name }));
    }
    
    processIncomingMessage(message, messageType: String) {
        switch(messageType) {
            case "VesselVisitDTO":                
                this.processIncomingVesselVisit(message);
                break;                
            case "NotificationDTO":                              
                this.processIncomingNotification(message);
                break;
            case "NoteDTO":
                this.processIncomingTerminalNote(message);     
                break;
            case "PlanningNoteDTO":                
                this.processIncomingVesselVisitNote(message);
                break;
            case "KpiDTO":                
                this.processIncomingVesselVisitKpi(message);
                break;
            case "UnitsRecapFullDTO":
                this.processIncomingUnitsRecap(message);
                break;
            case "VesselNoteDTO":
                this.processIncomingVesselNote(message);
                break;
            case "JmsDisconection":
                this.processIncomingDisconnection(message);             
                break;
            case "Boolean":
                this.processIncomingDisconnection(message);
                break;
        }        
    }

    processIncomingVesselVisit(message) {
        const vesselVisitJSON = JSON.stringify(message);
        const vesselVisitDTO = <VesselVisitDTO>JSON.parse(vesselVisitJSON);
        this.notifyVesselVisitList.next(vesselVisitDTO); 
    }

    processIncomingDisconnection(message) {
        const disconnected = JSON.stringify(message);
        const isDisconnected = <Boolean>JSON.parse(disconnected);
        const disconnectionAlerted = localStorage.getItem('disconnectionAlerted');
        if(isDisconnected && disconnectionAlerted === "false") {
            let data: Array < any >= [];
            data.push({
                'title': "COMPASS - DISCONNECTION",
                'alertContent': "Compass is not connected. You will not get notifications and vessel visit updates. Contact your IT department.",
                'icon': "assets/images/notification/BrokenConnectionIcon@2x.png",
                'link': "index.html#/dashboard-management"
            });         
            if(this._notificationService.permission !== 'granted') {
                this._notificationService.requestPermission();                
            }   
            this._notificationService.generateNotification(data);
            localStorage.setItem('disconnectionAlerted', "true");
        }
        if(!isDisconnected) {
            localStorage.setItem('disconnectionAlerted', "false"); 
        }
    }

    processIncomingNotification(notification) {             
        const notificationJSON = JSON.stringify(notification);
        const notificationDTO = <NotificationDTO>JSON.parse(notificationJSON);
        var notificationRole = this.authenticationService.getUserNotificationProfile();    
        if((notificationRole !== "NONE" && notificationDTO.notificationProfiles.indexOf(notificationRole) > -1) || notificationRole === "ALL") {
            var notificationIcon = "";            
            var notificationTitle = "COMPASS - ";
            var link = "";
            var message = "";
            switch(notificationDTO.notificationType) {
                case "EDI":
                    notificationTitle += notificationDTO.vesselVisitName;
                    notificationIcon = "assets/images/notification/EDINotificationIcon@2x.png";
                    message = notificationDTO.deadlineName + ' expired ';
                    link += "index.html#/dashboard-management";
                    // link += "/index.html#/details-management/" + notificationDTO.vesselVisitName;
                    break;
                case "TERMINAL":
                    notificationTitle += "TERMINAL NOTE";
                    notificationIcon = "assets/images/notification/TerminalNotificationIcon@2x.png";
                    message = notificationDTO.message;
                    link += "index.html#/dashboard-management";
                    break;
                case "DESKTOP":                
                default:
                    notificationTitle += "NOTIFICATION";
                    notificationIcon = "assets/images/notification/DesktopNotificationIcon@2x.png";
                    message = notificationDTO.deadlineName + ' expired ';
                    link += "index.html#/dashboard-management";
                    break;
            }
            let data: Array < any >= [];
            data.push({
                'title': notificationTitle,
                'alertContent': message,
                'icon': notificationIcon,
                'link': link
            });         
            if(this._notificationService.permission !== 'granted') {
                this._notificationService.requestPermission();
            }               
            this._notificationService.generateNotification(data);
        }    
    }

    processIncomingTerminalNote(message) {       
        const terminalNoteJSON = JSON.stringify(message);
        const terminalNoteDTO = <NoteDTO>JSON.parse(terminalNoteJSON);
        if(terminalNoteDTO.textMessage.startsWith("Warn:")){
            alert(terminalNoteDTO.textMessage.toString().split(':')[1]+terminalNoteDTO.vesselVisitName.toString()+'\n'+"Check the Facility, Carrier Service and Line Operator selections in Workflow Settings > Workflow Templates")
        } 
        else{
            this.notifyTerminalNoteList.next(terminalNoteDTO);
        }         
    }
    
    processIncomingVesselVisitNote(message) {
        const vesselVisitNoteJSON = JSON.stringify(message);
        const vesselVisitNoteDTO = <PlanningNoteDTO>JSON.parse(vesselVisitNoteJSON);        
        this.notifyVesselVisitNoteList.next(vesselVisitNoteDTO); 
    }

    processIncomingVesselNote(message) {
        const vesselNoteJSON = JSON.stringify(message);
        const vesselNoteDTO = <VesselNoteDTO>JSON.parse(vesselNoteJSON);        
        this.notifyVesselNoteList.next(vesselNoteDTO); 
    }

    processIncomingVesselVisitKpi(message) {
        const vesselVisitKpiJSON = JSON.stringify(message);
        const vesselVisitKpiDTO = <KpiDTO>JSON.parse(vesselVisitKpiJSON);        
        this.notifyVesselVisitKpi.next(vesselVisitKpiDTO); 
    }

    processIncomingUnitsRecap(message) {
        const unitsRecapJSON = JSON.stringify(message);
        const unitsRecapDTO = <UnitsRecapDTO>JSON.parse(unitsRecapJSON);        
        this.notifyUnitsRecap.next(unitsRecapDTO);         
    }
  
    OnDestroy() {
        if(environment.debug){
        console.log('Disconecting...');
        }
        this.disconnect();
    }
}