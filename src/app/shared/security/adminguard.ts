
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AdminGuard implements CanActivate {

    constructor(private router: Router, private authService: AuthenticationService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {        
        if (!this.authService.isLoggedIn()) {
            // not logged in so redirect to login page with the return url and return false
            this.router.navigate(['/error-page']);
            return false;
        }
        if (!this.authService.isAdmin() && !this.authService.isManager()) {
            // not admin so navigate to main page with the return url and return false
            this.router.navigate(['/error-page']);
            return false;
        }        
        return true;
    }
}