import { AuthenticationService } from '../services/authentication.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/do';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

// (err: any) => {        
//   if (err instanceof HttpErrorResponse) {
//     if (err.status === 401) {
//       this.router.navigate(['/error-page']);
//     }
//   }
// }

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(private auth: AuthenticationService, private router: Router) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {        
    request = request.clone({
      setHeaders: {
        'Content-Type' : 'application/json; charset=utf-8',
        'Accept'       : 'application/json',
        'Authorization': `Bearer ${this.auth.getToken()}`
      },
      withCredentials: true
    });    
    return next.handle(request).pipe(
      retry(1),
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';
        if(error.error instanceof ErrorEvent){
          errorMessage = `Error: ${error.error.message}`;
        } else {
          errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        window.alert(errorMessage);
        return throwError(errorMessage);
      })
    )
  }
}
