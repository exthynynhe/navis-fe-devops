export const environment = {
  production: true,
  // ## DPW TEST
  // helpFiles: 'https://10.54.219.143:5008/dist/usermanual/index.htm',
  // apiEndpoint: 'https://10.54.219.143:5008/backend-0.0.1-SNAPSHOT/',
  // websiteUrl: 'https://10.54.219.143:5008/dist/',
  // ## DPW
  // helpFiles: 'https://10.54.229.43:5008/dist/usermanual/index.htm',
  // apiEndpoint: 'https://10.54.229.43:5008/backend-0.0.1-SNAPSHOT/',
  // websiteUrl: 'https://10.54.229.43:5008/dist/',
  // ## ATOM DEMO
  // helpFiles: 'https://10.49.70.26:5000/dist/usermanual/index.htm',
  // apiEndpoint: 'https://10.49.70.26:5000/backend-0.0.1-SNAPSHOT/',
  // websiteUrl: 'https://10.49.70.26:5000/dist/',
  // ## CHENAI TEST
  // helpFiles: 'https://10.43.81.40:5000/dist/usermanual/index.htm',
  // apiEndpoint: 'https://10.43.81.40:5000/backend-0.0.1-SNAPSHOT/',
  // websiteUrl: 'https://10.43.81.40:5000/dist/',    
  // ## ATOM PROD
  // helpFiles: 'https://10.49.70.57:5000/dist/usermanual/index.htm',
  // apiEndpoint: 'https://10.49.70.57:5000/backend-0.0.1-SNAPSHOT/',
  // websiteUrl: 'https://10.49.70.57:5000/dist/', 
  // ## ATOM OTHER ENVIRONMENT
  // helpFiles: 'https://10.49.70.50:5000/dist/usermanual/index.htm',
  // apiEndpoint: 'https://10.49.70.50:5000/backend-0.0.1-SNAPSHOT/',
  // websiteUrl: 'https://10.49.70.50:5000/dist/',    
  // ## YILPORTS TEST ATOM
  helpFiles: 'https://10.49.70.44:5000/dist/usermanual/index.htm',
  apiEndpoint: 'https://10.49.70.44:5000/backend-0.0.1-SNAPSHOT/',
  websiteUrl: 'https://10.49.70.44:5000/dist/',
  // ## YILPORTS
  // helpFiles: 'https://www.navislabs.com/usermanual/index.htm',
  // apiEndpoint: 'https://backend.navislabs.com/',    
  // websiteUrl: 'https://www.navislabs.com',   
  // ## AWS STAGING
  // helpFiles: 'https://www.testyilport.navislabs.com/usermanual/index.htm',
  // apiEndpoint: 'https://backendtstyil.navislabs.com/',    
  // websiteUrl: 'https://www.staging.navislabs.com',
  gaUserId: 'UA-134356194-2',
  debug: true
};
