// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false, 
  helpFiles: 'https://10.49.70.44:5000/dist/usermanual/index.htm',
  apiEndpoint: 'https://10.49.70.44:5000/backend-0.0.1-SNAPSHOT/',
  websiteUrl: 'https://10.49.70.44:5000/dist/',
  gaUserId: 'UA-134356194-1',
  debug: true
};