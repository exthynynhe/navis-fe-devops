remote_state {
  backend = "s3"
  config = {
    bucket         = "henry-tf-demo"
    key            = "${path_relative_to_include()}/devops/terraform.tfstate"
    region         = "eu-west-1"
    encrypt        = true
    dynamodb_table = "henry-tf-lock-table"
  }
}
