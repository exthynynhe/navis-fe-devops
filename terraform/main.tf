terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

provider "aws" {
  region = "eu-west-1"
}

module "codebuild" {
    source      =   "./modules/codebuild"

    env         =   "${var.env}"
    app_pwd     =   "${var.app_pwd}"
    user        =   "${var.user}"
    repo_url    =   "${var.repo_url}"
}

module "codepipeline" {
    source          =   "./modules/codepipeline"

    env             =   "${var.env}"
    artifact_bucket_arn     =   module.codebuild.artifact_bucket_arn
    deploy_bucket           =   "${var.deploy_bucket}"
}