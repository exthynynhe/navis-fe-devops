terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

provider "aws" {
  region = "eu-west-1"
}

data "aws_iam_policy_document" "instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

data "template_file" "codebuild_policy" {
    template = "${file("${path.module}/codebuild_role_policy.json.tpl")}"
    vars = {
        artifact_bucket = "${aws_s3_bucket.artifact_bucket.arn}"
    }
}
resource "aws_iam_role" "codebuild_role" {
    name    =   "CodeBuildCompassRole-${var.env}"
    assume_role_policy =   "${data.aws_iam_policy_document.instance_assume_role_policy.json}"
}

resource "aws_iam_role_policy" "codebuild_role_policy" {
  role      =   "${aws_iam_role.codebuild_role.name}"
  policy    =   "${data.template_file.codebuild_policy.rendered}"
}

resource "aws_s3_bucket" "artifact_bucket" {
    bucket  = "compass-fe-artifacts-${var.env}"
    acl     = "private"
    versioning {
        enabled = true
    }
}

resource "aws_codebuild_source_credential" "bitbucket_creds" {
    auth_type   =   "BASIC_AUTH"
    server_type =   "BITBUCKET"
    token       =   "${var.app_pwd}"
    user_name   =   "${var.user}"
}

resource "aws_codebuild_project" "compass-fe" {
    name    =   "compass-fe-build-${var.env}"
    description =   "Pull source code form Bitbucket, build app and push artifacts to S3"
    build_timeout   =   "15"
    service_role    =   "${aws_iam_role.codebuild_role.arn}"

    artifacts {
        type    =   "S3"
        name    =   "/"
        encryption_disabled =   true
        location    =   "${aws_s3_bucket.artifact_bucket.bucket}"
        namespace_type  =   "NONE"
        packaging       =   "NONE"
    }

    environment {
        compute_type    =   "BUILD_GENERAL1_SMALL"
        image           =   "aws/codebuild/standard:2.0"
        type            =   "LINUX_CONTAINER"

    }

    source {
        type            = "BITBUCKET"
        location        = "${var.repo_url}"
        git_clone_depth = 1
    }

    logs_config {
        cloudwatch_logs {
            group_name = "codebuild-compass-fe-${var.env}"
            stream_name = "build-logs"
        }
    }
}