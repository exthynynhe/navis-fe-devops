{
  "Version": "2012-10-17",
  "Statement": [
      {
      "Effect": "Allow",
      "Resource": [
        "*"
      ],
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "${artifact_bucket}",
        "${artifact_bucket}/*"
      ]
    }
  ]
}