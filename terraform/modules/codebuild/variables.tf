variable "env" {
  type = string
}
variable "app_pwd" {
    type = string
}

variable "user" {
  type = string
}

variable "repo_url" {
  type = string
}