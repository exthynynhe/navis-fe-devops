terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

provider "aws" {
  region = "eu-west-1"
}

data "aws_iam_policy_document" "instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codepipeline_role" {
    name    =   "CodePipelineCompassRole-${var.env}"
    assume_role_policy =   "${data.aws_iam_policy_document.instance_assume_role_policy.json}"
}

data "template_file" "codebuild_policy" {
    template = "${file("${path.module}/codepipeline_role_policy.json.tpl")}"
    vars = {
        artifact_bucket = "${var.artifact_bucket_arn}"
        deploy_bucket   = "${var.deploy_bucket}"
    }
}

resource "aws_iam_role_policy" "codepipeline_role_policy" {
    role    =   "${aws_iam_role.codepipeline_role.name}"    
    policy  =   "${data.template_file.codebuild_policy.rendered}"
}

resource "aws_codepipeline" "compass_pipeline" {
    name    =   "compass-fe-pipeline-${var.env}"
    role_arn    =   "${aws_iam_role.codepipeline_role.arn}"

    artifact_store {
        location    =   split(":::","${var.artifact_bucket_arn}")[1]
        type        =   "S3"
    }

    stage {
        name = "Source"

        action {
            name        =   "Source"
            category    =   "Source"
            owner       =   "AWS"
            provider    =   "S3"
            version     =   "1"
            output_artifacts    =   ["source_output"]

            configuration = {
                S3Bucket    =   split(":::","${var.artifact_bucket_arn}")[1]
                S3ObjectKey =   "dist/compass.zip"
                PollForSourceChanges    =   "True"
            }
        }
    }

    stage {
        name = "Deploy"

        action {
            name        =   "Deploy"
            category    =   "Deploy"
            owner       =   "AWS"
            provider    =   "S3"
            input_artifacts = ["source_output"]
            version     =   "1"

            configuration = {
                BucketName  =   split(":::","${var.deploy_bucket}")[1]
                Extract     =   true
            }
        }
    }
}