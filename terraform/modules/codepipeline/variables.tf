variable "env" {
  type = string
}

variable "artifact_bucket_arn" {
  type = string
}

variable "deploy_bucket" {
  type = string
}
