{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect":"Allow",
      "Action": [
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:GetBucketVersioning",
        "s3:PutObject"
      ],
      "Resource": [
        "${artifact_bucket}",
        "${artifact_bucket}/*",
        "${deploy_bucket}",
        "${deploy_bucket}/*"
      ]
    }
  ]
}