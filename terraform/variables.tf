variable "env" {
  type = string
}

variable "app_pwd" {
    type = string
}

variable "user" {
  type = string
}

variable "repo_url" {
  type = string
}

variable "deploy_bucket" {
  type = string
  description = "ARN of the S3 website bucket created in infra template"
}
